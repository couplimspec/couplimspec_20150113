
function [coa,hvp,hco]=acpimdsograph(vp,co,vl,im,scale)
% graph of pricnipal component analysis for dso data
%
%% input 
%   vp : vecteur propres au format div
%   co : composantes pricnipales au format dso
%   vl : valeurs propres au format div : valeur, interie, interie cumulée

%% output
%   hvp : handle des figures vecteurs propres
%   hco : handle des figures composantes  principales

%% initialisation 

if (isempty(scale))
    scale=0;
end;

% fermeture des fenetre acp existentes

close all

% for i=1:4
%     h=findobj('name',strcat('Analyse en Composantes Principales : Composante %d',i));
%     close(h);
% end;
% h=findobj('name','Analyse en Composantes Principales : Composantes 1-2-3');
% close(h);
% h=findobj('name','Analyse en Composantes Principales : Composantes 2-3-4');
% close(h);
% h=findobj('name','Analyse en Composantes Principales : Composantes 1 et 2');
% close(h);
% h=findobj('name','Analyse en Composantes Principales : Composantes 3 et 4');
% close(h);
% h=findobj('name','Analyse en Composantes Principales : Profils des variables');
% close(h);
% h=findobj('name','Analyse en Composantes Principales : cercle des corrélations : Composantes 1 et 2');
% close(h);
% h=findobj('name','Analyse en Composantes Principales : cercle des corrélations : Composantes 3 et 4');
% close(h);

%% nom générique
nom=strrep(co.name,'.cp','');

% cartes des composantes 1 et 2
hco(1)=figure('numbertitle','off','name','Analyse en Composantes Principales : Composantes 1 et 2');
figure(hco(1))
if co.imagemode
    plot(co.data(:,1),co.data(:,2),'.b')
else
    plot(co.data(:,1),co.data(:,2),'.w')
    text(co.data(:,1),co.data(:,2),co.label{1},'color','k','fontsize',7)
end;
xlabel(sprintf('composante principale 1 (%5.2f %%)',vl.d(1,2)))
ylabel(sprintf('composante principale 2 (%5.2f %%)',vl.d(2,2)))
title(nom)


if (size(co.data,2)>=4)
    % cartes des composantes 3 et 4
    hco(2)=figure('numbertitle','off','name','Analyse en Composantes Principales : Composantes 3 et 4');
    figure(hco(2))
    if co.imagemode
        plot(co.data(:,3),co.data(:,4),'.b')
    else
        plot(co.data(:,3),co.data(:,4),'.w')
        text(co.data(:,3),co.data(:,4),co.label{1},'color','k','fontsize',7)
    end;
    xlabel(sprintf('composante principale 3 (%5.2f %%)',vl.d(3,2)))
    ylabel(sprintf('composante principale 4 (%5.2f %%)',vl.d(4,2)))
    title(nom)  
end;
        
if co.imagemode
    for i=1:min(4,size(co.data,2))
        %représentation des composantes comme des images
        nbf=2+i;
        hco(nbf)=figure('numbertitle','off','name',sprintf('Analyse en Composantes Principales : Composante %d',i)); %#ok<*AGROW>
        figure(hco(nbf))
        coa(:,:,i)=ajusterimage(co.imagedata(:,:,i));
        imshow(co.imagedata(:,:,i),[]);
        title(sprintf('%s : CP %d',nom,i)); 
    end;
    if size(coa,2)>=3
        nbf=nbf+1;
        co123=coa(:,:,1:3);
        hco(nbf)=figure('numbertitle','off','name','Analyse en Composantes Principales : Composantes 1-2-3');
        imshow(co123);
        title(sprintf('%s : CP 1-2-3',nom)); 
    end;
    if size(coa,2)>=4
        nbf=nbf+1;
        co234=coa(:,:,2:4);
        hco(nbf)=figure('numbertitle','off','name','Analyse en Composantes Principales : Composantes 2-3-4');
        imshow(co234);
        title(sprintf('%s : CP 2-3-4',nom)); 
    end;
end;
    
  
% profils des variables
hvp(1)=figure('numbertitle','off','name','Analyse en Composantes Principales : Profils des variables');
figure(hvp(1))
% si les variables sont des nombres
if sum(size(str2num(vp.i)))>0
    axx=str2num(vp.i);
else
    axx=1:size(vp.i,1);
end;
for i=1:min(4,size(vp.d,2))
   subplot(2,2,i)
   plot(axx,vp.d(:,i),'k')
   if strcmp(co.userdata.unit,'cm-1')
        set(gca,'xdir','reverse')
   end;
   hold on
   plot([min(axx) max(axx)],[0 0],':k')
   xlabel(' ')
   ylabel('unité arbitraire')
   minx=min(axx);
   maxx=max(axx);
   miny=min(vp.d(:,i))-0.03*abs(min(vp.d(:,i)));
   maxy=max(vp.d(:,i))+0.03*abs(max(vp.d(:,i)));
   axis([minx maxx miny maxy]);
   title(sprintf('%s : profil %d',nom,i),'fontsize',7)
   hold off
end;

% les variables sont des chaines de caractères : cercle des corrélations
if scale
    hvp(1)=figure('numbertitle','off','name','Analyse en Composantes Principales : cercle des corrélations : Composantes 1 et 2');
    figure(hvp(1))
    subplot(1,1,1)
    plot(sqrt(vl.d(1,1))*vp.d(:,1),sqrt(vl.d(2,1))*vp.d(:,2),'.w')
    text(sqrt(vl.d(1,1))*vp.d(:,1),sqrt(vp.d(2,1))*vp.d(:,2),vp.i,'fontsize',7)
    xlabel(sprintf('composante principale 1 (%5.2f %%)',vp.d(1,2)))
    ylabel(sprintf('composante principale 2 (%5.2f %%)',vp.d(2,2)))
    hold on
    cercle;
    plot([-1 1],[0 0],':k')
    plot([0 0 ],[-1 1],':k')
    title(nom)
    hold off

    if (size(co.data,2)>=4)
        hvp(2)=figure('numbertitle','off','name','Analyse en Composantes Principales : cercle des corrélations : Composantes 3 et 4');
        figure(hvp(2))
        plot(sqrt(vl.d(3,1))*vp.d(:,3),sqrt(vl.d(4,1))*vp.d(:,4),'.w')
        text(sqrt(vl.d(3,1))*vp.d(:,3),sqrt(vl.d(4,1))*vp.d(:,4),don.v,'fontsize',7)
        xlabel(sprintf('composante principale 3 (%5.2f %%)',vl.d(3,2)))
        ylabel(sprintf('composante principale 4 (%5.2f %%)',vl.d(4,2)))
        hold on
        cercle;
        plot([-1 1],[0 0],':k')
        plot([0 0 ],[-1 1],':k')
        title(nom)
        hold off
     end;
else
     cc=corrcoef([double(im.data),co.data]);
     cc=cc(1:size(im.data,2),(size(im.data,2)+1):(size(im.data,2)+size(co.data,2)));
     h=figure('numbertitle','off','name','Analyse en Composantes Principales : cercle des corrélations : Composantes 1 et 2');
     figure(h)
     subplot(1,1,1)
     plot(cc(:,1),cc(:,2),'.w')
     text(cc(:,1),cc(:,2),im.label{2},'fontsize',7)
     xlabel(sprintf('composante principale 1 (%5.2f %%)',vl.d(1,2)))
     ylabel(sprintf('composante principale 2 (%5.2f %%)',vl.d(2,2)))
     hold on
     cercle;
     plot([-1 1],[0 0],':k')
     plot([0 0 ],[-1 1],':k')
     title(nom)
     hold off


     if (size(co.data,2)>=4)
         h=figure('numbertitle','off','name','Analyse en Composantes Principales : cercle des corrélations : Composantes 3 et 4');
         figure(h)
         subplot(1,1,1)
         plot(cc(:,3),cc(:,4),'.w')
         text(cc(:,3),cc(:,4),im.label{2},'fontsize',7)
         xlabel(sprintf('composante principale 3 (%5.2f %%)',vl.d(3,2)))
         ylabel(sprintf('composante principale 4 (%5.2f %%)',vl.d(4,2)))
         hold on
         cercle;
         plot([-1 1],[0 0],':k')
         plot([0 0 ],[-1 1],':k')
         title(nom)
         hold off
     end;
end;
    


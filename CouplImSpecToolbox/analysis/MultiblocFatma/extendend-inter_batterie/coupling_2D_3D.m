function [dso] = coupling_2D_3D (varargin) %#ok<STOUT>

 %% description
    % coupling and linking two 2 way datatables
    
%% input

   % no inputs
   % two files: -txt file
   %            -raw file
   %            -dso file

    
%% output
    
    %dso: datasetobject of the datatable reduced

%% principe
   % coupling two datasets having the same number of ways (2 dimensions)

%% use
   % [dso]=coupling_2D_2D()
   % [dso]=coupling_2D_2D(file1,file2)
    
%% Comments
    % writen for F.Allouche Phd Thesis
    
    
%% Author
    % F.Allouche
    
%% date
    % 22/04/2011    

%% context variables 
         

%% input

existdso=0;
if nargin ==0
     % input datatable1
     [nom1,rep1]=uigetfile({'*.dso.mat;*.txt;*.raw;'},'name of the two way datatable','*.dso.mat;*.txt;*.raw;');
     cd(rep1)
     setappdata(1i,'rep1',rep1);
     setappdata(1i,'nom1',nom1);
     point=strfind(nom1,'.');
     point=point(length(point));
     ext=nom1((point+1):(length(nom1)));
        if strcmp(ext,'mat')
            point=strfind(nom1,'.');
            point=point(length(point)-1);
            ext=nom1((point+1):(length(nom1)));
            if strcmp(ext,'dso.mat')
            load(nom1);
            dtab1=dso;
            lo1=dtab1.axisscale{2};
            existdso=existdso+1;
            clear dso;
            else
               error('format .dso.mat or .raw or .txt expected')
            end
            if length(size(dtab1.data))~=2
                error('please enter a two way datatable');
            else 
                dtab1=dtab1.data;
            end;
            
        elseif strcmp(ext,'raw')
            [dtab1,lo1]=lire_raw(nom1);
            
            if length(size(dtab1))==3
                dtab1=mean(dtab1,3);
            end;
        elseif strcmp(ext,'txt')
            dtab1=lire(nom1); 
            lo1=str2double(dtab1.v);
            dtab1=dtab1.d;
        else
            error('format .dso.mat or .raw or .txt attendu')
        end;
        
    %input datatable 2   
     [nom2,rep2]=uigetfile({'*.dso.mat;*.txt;*.raw;'},'name of the three way datatable','*.dso.mat;*.txt;*.raw;');
     cd(rep2)
     setappdata(1i,'rep2',rep2);
     setappdata(1i,'nom2',nom2);
     point=strfind(nom2,'.');
     point=point(length(point));
     ext=nom2((point+1):(length(nom2)));
        if strcmp(ext,'mat')
            point=strfind(nom2,'.');
            point=point(length(point)-1);
            ext=nom2((point+1):(length(nom2)));
            if strcmp(ext,'dso.mat')
            load(nom2);
            dtab2=dso;
            lo2=dtab2.axisscale{2};
            clear dso;
            existdso=existdso+1;
            if length(size(dtab2.data))~=3
                error('please enter a three way datable');
            else 
                dtab2=dtab2.data;
            end;
            clear dso;
            else
               error('format .dso.mat or .raw or .txt expected')
            end
        elseif strcmp(ext,'raw')
            [dtab2,lo2]=lire_raw(nom2);
            
        elseif strcmp(ext,'txt')
            error('Please enter a three way datatable')
        else
            error('format .dso.mat or .raw or .txt expected')
        end;
        ok=0;
    while ~ok
        prompt = {'Enter the dimension '};
        dlg_title = 'dimension';
        num_lines = 1;

        answer = inputdlg(prompt,dlg_title,num_lines);
        dimension=str2double(cell2mat (answer)); %#ok<ST2NM>
        if strcmp(class(dimension),'double')
            ok=1;
        else 
            ok=0;
        end
    end
        
end;


if nargin==2
    
    dtab1=varargin{1};
    dtab2=varargin{2};
    dimension=varargin{3};
    
    if strcmp(class(dtab1),'dataset')
            
            dtab1=dtab1.data;
            if length(size(dtab1.data))~=2
                dtab1=mean(dtab2.data,3);
            end;
   
    elseif strcmp(class(dtab1),'double')
            if length(size(dtab1))==3
                dtab1=mean(dtab1,3);
            end;
    else
            error('format .dso.mat or .raw or .txt expected')
    end;
    
    
    if strcmp(class(dtab2),'dataset')
            
            dtab2=dtab2.data;
            if length(size(dtab2.data))==3
                dtab2=mean(dtab2.data,3);
            end;
   
    elseif strcmp(class(dtab2),'double')
            if ength(size(dtab2.data))==3
                dtab2=mean(dtab2,3);
            end;
    else
            error('format .dso.mat or .raw or .txt expected')
    end;
    
end;

if nargin >2
     error('use: coupling2D_2D or coupling2D_2D(file1,file2)');
end;

%% treatement
dtab1=dtab1-ones(size(dtab1,1),1)*mean(dtab1);

%couplage



[U,V,Z]= deflation_tucker(dtab1,dtab2,dimension);


% calcul des variances et des variances cumul�es
% variance totale du tableau infrarouge
inertiaX=trace((1/size(dtab1,1))*(dtab1'*dtab1));
varX=zeros(dimension,1);
CX=dtab1*U;
varX=(1/size(dtab1,1))*diag(CX'*CX);
pvarX=(1/inertiaX)*varX;
pvarX=fix(100*pvarX);

% variance totale du tableau fluorescence
varY=zeros(dimension,1);
[kx,ky,kz]=size(dtab2);

for k=1:kz
     dtab2(:,:,k)=dtab2(:,:,k)-ones(size(dtab2,1),1)*mean(dtab2(:,:,k));
     CY(:,:,k)=dtab2(:,:,k)*V;
     inertiaY= trace((1/size(dtab1,1))*(dtab2(:,:,k)'*dtab2(:,:,k)));  
     varYK(:,:,k)=(1/size(dtab1,1))*diag(CY(:,:,k)'*CY(:,:,k));
end
varY=sum(varYK,3);
% pourcentage de variance des profils 
pvarY=varY/inertiaY;
pvarY=fix(100*pvarY);

% calcul des pourcentages de covariance
Z=zeros(kx,ky);
for k=1:kz
     ak = (1/size(dtab1,1))*(U(:,dimension)'*dtab1'*dtab2(:,:,k)*V(:,dimension));
     Z= Z+ak*dtab2(:,:,k);     
end
   [~,D,Q]=svd(dtab1'*Z);
   pcov= diag(D).*diag(D)/sum(diag(D).*diag(D));
 
 

 %%Save

if (existdso==2)
 
rep1=getappdata(1i,'rep1');
cd(rep1)
nom1=getappdata(1i,'nom1');
load(nom1);
figure (1);
plot(str2num(dso.label{2}),U(:,1:dimension))
xlabel([ dso.title{2} ' (' dso.userdata.unit, ')'],'fontsize',16)
if strcmp(dso.userdata.unit,'cm-1')
    set(gca,'xdir','reverse')
end;
switch dso.userdata.method
    case 'INFRARED'
        ylabel('infrared absorbance','fontsize',16);
    case 'RAMAN'
        ylabel('Raman intensity','fontsize',16);
    case 'FLUORESCENCE'
        ylabel('fluorescence intensity','fontsize',16);
    otherwise
        ylabel('')
end;
title(dso.name,'fontsize',16)
set(gca,'fontsize',14)
im1=dso;
clear('dso');
rep2=getappdata(1i,'rep2');
cd(rep2)
nom2=getappdata(1i,'nom2');
load(nom2);
figure (2);
plot(str2num(dso.label{2}),V(:,1:dimension))
xlabel([ dso.title{2} ' (' dso.userdata.unit, ')'],'fontsize',16)
if strcmp(dso.userdata.unit,'cm-1')
    set(gca,'xdir','reverse')
end;
switch dso.userdata.method
    case 'INFRARED'
        ylabel('infrared absorbance','fontsize',16);
    case 'RAMAN'
        ylabel('Raman intensity','fontsize',16);
    case 'FLUORESCENCE'
        ylabel('fluorescence intensity','fontsize',16);
    otherwise
        ylabel('')
end;
title(dso.name,'fontsize',16)
set(gca,'fontsize',14)

% for ii=1:kz
%   dtab2(:,:,ii)=dtab2(:,:,ii)-ones(size(dtab2(:,:,ii),1),1)*mean(dtab2(:,:,ii));  
% end
 
Comp_F=zeros(size(dtab2,1),5,25);
for jj=1:size(dtab2,3)
    Comp_F(:,:,jj)=dtab2(:,:,jj)*V;
end
F1=U*pinv(diag(sqrt(diag(U'*U))));
compIR=dtab1*F1;

[nom2,rep2]=uigetfile({'*.dso.mat;*.txt;*.raw;'},'name of the two way datatable','*.dso.mat;*.txt;*.raw;');
cd(rep2)
load(nom2);
im1=dso;
clear dso
[nom2,rep2]=uigetfile({'*.dso.mat;*.txt;*.raw;'},'name of the three way datatable','*.dso.mat;*.txt;*.raw;');
cd(rep2)
load(nom2);
im2=dso;

reconstrucrion_image(im1,im2,compIR,Comp_F)
else
figure (1);
plot(U(:,1:dimension))
figure (2);
plot(V(:,1:dimension))
% for ii=1:kz
%   dtab2(:,:,ii)=dtab2(:,:,ii)-ones(size(dtab2(:,:,ii),1),1)*mean(dtab2(:,:,ii));  
% end
 
Comp_F=zeros(size(dtab2,1),3,25);
for jj=1:size(dtab2,3)
    Comp_F(:,:,jj)=dtab2(:,:,jj)*V;
end
F1=U*pinv(diag(sqrt(diag(U'*U))));
compIR=dtab1*F1;
[nom2,rep2]=uigetfile({'*.dso.mat;*.txt;*.raw;'},'name of the three way datatable','*.dso.mat;*.txt;*.raw;');
cd(rep2)
load(nom2);
im1=dso;
clear dso
[nom2,rep2]=uigetfile({'*.dso.mat;*.txt;*.raw;'},'name of the three way datatable','*.dso.mat;*.txt;*.raw;');
cd(rep2)
load(nom2);
im2=dso;
reconstrucrion_image(im1,im2,compIR,Comp_F)
end
end

function [u0,v0,Z]= IB_Tucker3D_Comp1(X,Y)


[ny,py,ky]=size(Y);
[nx,px]=size(X);


u0=rand(px,1);
v0=rand(py,1);
u0= u0/norm(u0);% initialisation u0
v0= v0/norm(v0);%initialisation v0
%Z=zeros(ny,py)
fn=zeros(1000,1);
iter=0;
tol=0.00001;
diff=1;


while (diff>tol)
    iter=iter+1;
 
 Z=zeros(ny,py);
 for k=1:ky
     ak = (1/nx)*(u0'*X'*Y(:,:,k)*v0);
     Z= Z+ak*Y(:,:,k);
 end  
[P,~,Q]=svd(X'*Z);
u1= P(:,1);
v1=Q(:,1);
critere=0;
for k=1:ky
covariance= (1/nx)*(u0'*X'*Y(:,:,k)*v0);
critere=critere+covariance^2;
end
fn(iter,1)=critere;
diff=norm(u1-u0)+ norm(v1-v0);
u0=u1;
v0=v1;
end

 for k=1:ky
     ak = (1/nx)*(u0'*X'*Y(:,:,k)*v0);
 end  

end



function [U,V,Z]= deflation_tucker(X,Y,dimension)



% taille cube fluo
[~,~,ky]=size(Y);
% taille tableau ibnfrarouge
[nx,px]=size(X);
% centrage du tableau infrarouge



% calcul des deux premi�res composante
[u0,v0,Z]= IB_Tucker3D_Comp1(X,Y);
dim=1;
% matrice des composantes
U(:,dim)=u0;
V(:,dim)=v0;

% boucle pour le calcul des d�flations
while(dim<dimension)
    X=X-X*u0*u0';
    for k=1:ky
        Y(:,:,k)=Y(:,:,k)-Y(:,:,k)*v0*v0';
    end
    [u0,v0]= IB_Tucker3D_Comp1(X,Y);
    dim=dim+1;
    
    for k=1:ky      
        CY(:,dim,k)=Y(:,:,k)*v0;  
    end
U(:,dim)=u0;
V(:,dim)=v0;
end

end

function reconstrucrion_image(im1,im2,Comp_IR,Comp_F)


coordX=im1.userdata.dX;
coordY=im1.userdata.dY;
    
coordXfluo=im2.userdata.dX;
coordYfluo=im2.userdata.dY;    
    
% lecture des composantes infrarouge obtenus
comp1i=Comp_IR(:,1);
comp2i=Comp_IR(:,2);
comp3i=Comp_IR(:,3);
% lecture des composantes fluorescence obtenus
voisinage=sqrt(size(Comp_F,3));

comp1f=reshape(permute(Comp_F(:,1,:),[1 3 2]),size(Comp_F,1),size(Comp_F,3));
comp2f=reshape(permute(Comp_F(:,2,:),[1 3 2]),size(Comp_F,1),size(Comp_F,3));
comp3f=reshape(permute(Comp_F(:,3,:),[1 3 2]),size(Comp_F,1),size(Comp_F,3));

val1=unique(coordX); 
val2=unique(coordY);

X=size(val1,1);
Y=size(val2,1);

Im_I=zeros(X,Y,3);

[nom2,rep2]=uigetfile({'*.tif;'},'name of the three way datatable','*.tif;');
cd(rep2)
roitot=imread(nom2);
Im_F=imresize(roitot,0.78);



% cr�ation de l' image infrarouge

for cpt=1:size(im1.data,1)
Im_I(coordX(cpt,1)-min(coordX)+1,coordY(cpt,1)-min(coordY)+1,2)=comp1i(cpt);
Im_I(coordX(cpt,1)-min(coordX)+1,coordY(cpt,1)-min(coordY)+1,1)=comp2i(cpt);
comp1fc=reshape(comp1f(cpt,:),voisinage,voisinage);
comp2fc=reshape(comp2f(cpt,:),voisinage,voisinage);
comp3fc=reshape(comp2f(cpt,:),voisinage,voisinage);

for cpt2=1:voisinage
for cpt1=1:voisinage
comp1fctest(cpt1,cpt2)=comp1f(cpt,(cpt1-1)*5+cpt2);
comp2fctest(cpt1,cpt2)=comp2f(cpt,(cpt1-1)*5+cpt2);
comp3fctest(cpt1,cpt2)=comp3f(cpt,(cpt1-1)*5+cpt2);
end
end


Im_F(coordXfluo(cpt,1):coordXfluo(cpt,1)+voisinage-1,coordYfluo(cpt,1):coordYfluo(cpt,1)+voisinage-1,2)=comp1fc;
Im_F(coordXfluo(cpt,1):coordXfluo(cpt,1)+voisinage-1,coordYfluo(cpt,1):coordYfluo(cpt,1)+voisinage-1,1)=comp2fc;
Im_F(coordXfluo(cpt,1):coordXfluo(cpt,1)+voisinage-1,coordYfluo(cpt,1):coordYfluo(cpt,1)+voisinage-1,3)=comp2fc;
% Im_F((coordXfluo(cpt,1)-min(coordXfluo))*voisinage+1:(coordXfluo(cpt,1)-min(coordXfluo)+1)*voisinage,(coordYfluo(cpt,1)-min(coordYfluo))*voisinage+1:(coordYfluo(cpt,1)-min(coordYfluo)+1)*voisinage,1)=comp2fc;
% Im_F((coordXfluo(cpt,1)-min(coordXfluo))*voisinage+1:(coordXfluo(cpt,1)-min(coordXfluo)+1)*voisinage,(coordYfluo(cpt,1)-min(coordYfluo))*voisinage+1:(coordYfluo(cpt,1)-min(coordYfluo)+1)*voisinage,3)=comp3fc;
% 
% Im_Ftest((coordXfluo(cpt,1)-min(coordXfluo))*voisinage+1:(coordXfluo(cpt,1)-min(coordXfluo)+1)*voisinage,(coordYfluo(cpt,1)-min(coordYfluo))*voisinage+1:(coordYfluo(cpt,1)-min(coordYfluo)+1)*voisinage,2)=comp1fctest;
% Im_Ftest((coordXfluo(cpt,1)-min(coordXfluo))*voisinage+1:(coordXfluo(cpt,1)-min(coordXfluo)+1)*voisinage,(coordYfluo(cpt,1)-min(coordYfluo))*voisinage+1:(coordYfluo(cpt,1)-min(coordYfluo)+1)*voisinage,1)=comp2fctest;
% Im_Ftest((coordXfluo(cpt,1)-min(coordXfluo))*voisinage+1:(coordXfluo(cpt,1)-min(coordXfluo)+1)*voisinage,(coordYfluo(cpt,1)-min(coordYfluo))*voisinage+1:(coordYfluo(cpt,1)-min(coordYfluo)+1)*voisinage,3)=comp3fctest;
% 
% im1=ajusterimage(Im_Ftest(:,:,1),min(min(comp1f)),max(max(comp1f)));
% im2=ajusterimage(Im_Ftest(:,:,2),min(min(comp2f)),max(max(comp2f)));
% im3=ajusterimage(Im_Ftest(:,:,3),min(min(comp3f)),max(max(comp3f)));
% 
% figure
% imshow(im1,[])

end

figure
imshow(Im_I(:,:,1),[])

figure
imshow(Im_I(:,:,2),[])

figure
imshow(Im_F(:,:,1),[])

figure
imshow(Im_F(:,:,2),[])



end





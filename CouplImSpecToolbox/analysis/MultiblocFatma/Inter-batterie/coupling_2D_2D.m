function [dso] = coupling_2D_2D (varargin) %#ok<STOUT>

 %% description
    % coupling and linking two 2 way datatables
    
%% input

   % no inputs
   % two files: -txt file
   %            -raw file
   %            -dso file

    
%% output
    
    %dso: datasetobject of the datatable reduced

%% principe
   % coupling two datasets having the same number of ways (2 dimensions)

%% use
   % [dso]=coupling_2D_2D()
   % [dso]=coupling_2D_2D(file1,file2)
    
%% Comments
    % writen for F.Allouche Phd Thesis
    
    
%% Author
    % F.Allouche
    
%% date
    % 22/04/2011    

%% context variables 
orig=pwd;           % returns the current directory
clear all
close all
%% input

existdso=0;
if nargin ==0
     % input datatable1
     [nom1,rep1]=uigetfile({'*.dso.mat;*.txt;*.raw;'},'name of the first datatable','*.dso.mat;*.txt;*.raw;');
     cd(rep1)
     setappdata(i,'rep1',rep1);
     setappdata(i,'nom1',nom1);
     point=strfind(nom1,'.');
     point=point(length(point));
     ext=nom1((point+1):(length(nom1)));
        if strcmp(ext,'mat')
            point=strfind(nom1,'.');
            point=point(length(point)-1);
            ext=nom1((point+1):(length(nom1)));
            if strcmp(ext,'dso.mat')
            load(nom1);
            dtab1=dso;
            lo1=dtab1.axisscale{2};
            existdso=existdso+1;
            clear dso;
            else
               error('format .dso.mat or .raw or .txt expected')
            end
            if length(size(dtab1.data))==3
                im1=dtab1;
                dtab1=mean(dtab1.data,3);
            else
                im1=dtab1;
                dtab1=dtab1.data;
            end;
            
        elseif strcmp(ext,'raw')
            [dtab1,lo1]=lire_raw(nom1);
            
            if length(size(dtab1))==3
                dtab1=mean(dtab1,3);
            end;
        elseif strcmp(ext,'txt')
            dtab1=lire(nom1); %#ok<NASGU>
            lo1=str2num(dtab1.v);%#ok<NASGU>
            dtab1=dtab1.d;
        else
            error('format .dso.mat or .raw or .txt attendu')
        end;
        
    %input datatable 2   
     [nom2,rep2]=uigetfile({'*.dso.mat;*.txt;*.raw;'},'name of the second datatable','*.dso.mat;*.txt;*.raw;');
     cd(rep2)
     setappdata(i,'rep2',rep2);
     setappdata(i,'nom2',nom2);
     point=strfind(nom2,'.');
     point=point(length(point));
     ext=nom2((point+1):(length(nom2)));
        if strcmp(ext,'mat')
            point=strfind(nom2,'.');
            point=point(length(point)-1);
            ext=nom2((point+1):(length(nom2)));
            if strcmp(ext,'dso.mat')
            load(nom2);
            dtab2=dso;
            lo2=dtab2.axisscale{2};
            clear dso;
            existdso=existdso+1;
            if length(size(dtab2.data))==3
                im2=dtab2;
                dtab2=mean(dtab2.data,3);
            else
                im2=dtab2;
                dtab2=dtab2.data;
            end;
            clear dso;
            else
               error('format .dso.mat or .raw or .txt expected')
            end
        elseif strcmp(ext,'raw')
            [dtab2,lo2]=lire_raw(nom2);
            if size(dtab2)==3
                dtab2=mean(dtab2.data,3);
            end;
        elseif strcmp(ext,'txt')
            dtab2=lire(nom2);
            lo2=dtab2.v;%#ok<NASGU>
            lo2=str2num(lo2);
            dtab2=dtab2.d;
        else
            error('format .dso.mat or .raw or .txt expected')
        end;
        ok=0;
    while ~ok
        prompt = {'Enter the dimension '};
        dlg_title = 'dimension';
        num_lines = 1;

        answer = inputdlg(prompt,dlg_title,num_lines);
        dimension=str2num(cell2mat (answer)); %#ok<ST2NM>
        if strcmp(class(dimension),'double')
            ok=1;
        else 
            ok=0;
        end
    end
        
end;


if nargin==2
    
    dtab1=varargin{1};
    dtab2=varargin{2};
    dimension=varargin{3};
    
    if strcmp(class(dtab1),'dataset')
            
            dtab1=dtab1.data;
            if length(size(dtab1.data))==3
                dtab1=mean(dtab2.data,3);
            end;
   
    elseif strcmp(class(dtab1),'double')
            if length(size(dtab1))==3
                dtab1=mean(dtab1,3);
            end;
    else
            error('format .dso.mat or .raw or .txt expected')
    end;
    
    
    if strcmp(class(dtab2),'dataset')
            
            dtab2=dtab2.data;
            if length(size(dtab2.data))==3
                dtab2=mean(dtab2.data,3);
            end;
   
    elseif strcmp(class(dtab2),'double')
            if ength(size(dtab2.data))==3
                dtab2=mean(dtab2,3);
            end;
    else
            error('format .dso.mat or .raw or .txt expected')
    end;
    
end;

if nargin >2
     error('use: coupling2D_2D or coupling2D_2D(file1,file2)');
end;

%% treatement
dtab1=dtab1-ones(size(dtab1,1),1)*mean(dtab1);
dtab2=dtab2-ones(size(dtab2,1),1)*mean(dtab2);
%couplage

%calcul de la matrice des convariances. 
Z=(1/size(dtab1,1))*dtab1'*dtab2;
[L_I,D,L_F]=svds(Z,dimension);

Imag_IR=dtab1*L_I;
Imag_F=dtab2*L_F;

%Part de variabilit� expliqu�e pour chaque profils
inertiadtab1=trace((1/size(dtab1,1))*(dtab1'*dtab1));% variance totale.
inertiadtab2=trace((1/size(dtab2,1))*(dtab2'*dtab2));
vardtab1=zeros(dimension,1);
vardtab2=zeros(dimension,1);
vardtab1=(1/size(dtab1,1))*diag(Imag_IR'*Imag_IR)
vardtab2=(1/size(dtab1,1))*diag(Imag_F'*Imag_F)

% pourcentage de variance des profils 
pvardtab1=(1/inertiadtab1)*vardtab1;
pvardtab1=100*pvardtab1 %#ok<NASGU>
pvardtab2=(1/inertiadtab2)*vardtab2;
pvardtab2=100*pvardtab2
% pourcentage de co varianceentre profils 
pcov=diag(D).*diag(D)/sum(diag(D).*diag(D))


%%Save



 for dim=1:dimension

 
rep1=getappdata(i,'rep1');
cd(rep1)
nom1=getappdata(i,'nom1');
load(nom1);
   figure ;
im1=dso;
clear('dso');
plot(str2num(im1.label{2}),L_I(:,dim))
xlabel([ im1.title{2} ' (' im1.userdata.unit, ')'],'fontsize',16)
if strcmp(im1.userdata.unit,'cm-1')
    set(gca,'xdir','reverse')
end;
switch im1.userdata.method
    case 'INFRARED'
        ylabel('unit� arbitraire','fontsize',16);
    case 'RAMAN'
        ylabel('unit� arbitraire ','fontsize',16);
    case 'FLUORESCENCE'
        ylabel('unit� arbitraire','fontsize',16);
    otherwise
        ylabel('')
end;
title(im1.name,'fontsize',16)
set(gca,'fontsize',14)

rep2=getappdata(i,'rep2');
cd(rep2)
nom2=getappdata(i,'nom2');
load(nom2);
figure ;
plot(str2num(dso.label{2}),L_F(:,dim))
xlabel([ dso.title{2} ' (' dso.userdata.unit, ')'],'fontsize',16)
if strcmp(dso.userdata.unit,'cm-1')
    set(gca,'xdir','reverse')
end;
switch dso.userdata.method
    case 'INFRARED'
        ylabel('infrared absorbance','fontsize',16);
    case 'RAMAN'
        ylabel('Raman intensity','fontsize',16);
    case 'FLUORESCENCE'
        ylabel('fluorescence intensity','fontsize',16);
    otherwise
        ylabel('')
end;
title(dso.name,'fontsize',16)
set(gca,'fontsize',14)





%reconstrucrion_image(im1,Imag_IR,Imag_F)
 end
 
 % reconstruction 
  
    T1=im1.data;
    T2=im2.data;
    voisinage=5;
    CX1=zeros(size(T1,1),dimension,voisinage^2);
    CX2=zeros(size(T2,1),dimension,voisinage^2);
     
     for k=1:size(T2,3)
        
        T1(:,:,k)=T1(:,:,k)-ones(size(T1(:,:,k),1),1)*mean(dtab1);
        T2(:,:,k)=T2(:,:,k)-ones(size(T2(:,:,k),1),1)*mean(dtab2);  
%         
%         TT2=zeros(size(T2,1),size(T2,2)/3,size(T2,3));
%  
%  
%  
%      % echantillonage of Raman datatable
%         for l=1:3:size(T2,2)-3
%              for j=1:3
%                   TT2(:,(l+2)/3,:)=TT2(:,(l+2)/3,:)+T2(:,l+j,:);
%              end
%         end
%  
%          TT2=TT2/3;
        
        CX1(:,:,k)=T1(:,:,k)*L_I;
        CX2(:,:,k)=T2(:,:,k)*L_F;
        
     end
 
 
reconstruction_imagedso(im1,im2,CX1,CX2,dimension,rep1,rep2,voisinage);
end

function reconstruction_imagedso(im1,im2,Imag_F,Imag_R,dimension,rep1,rep2,voisinage)

    
    SizeimX=42 %im1.imagesize(1);
    SizeimY=12 %im1.imagesize(2);
    
    % Get back coordinates values of pixels analysed
    coordX=im1.userdata.dX;
    coordY=im1.userdata.dY;
    
    
    coordX2=im2.userdata.dX;
    coordY2=im2.userdata.dY;
    
    % Compute the unique values of coordinates
    val1=unique(coordX); 
    val2=unique(coordY);
    
      % Compute the optimum size of the image score
    X=size(val1,1);
    Y=size(val2,1);
    
  

    [nomim,repim]=uigetfile({'*.tif'},'name of the region selected','*.tif');
    cd(repim)
    ref=imread(nomim);
    ref(:,:,3)=ref(:,:,1);
    ref(:,:,2)=ref(:,:,1);

   % Computing all score images Infrared Raman and Fluorescence within the
   % number of dimensions selected
   for dim=1:dimension
    
       Imag_Fdim=reshape(permute(Imag_F(:,dim,:),[1 3 2]),size(im1.data,1),voisinage^2);
       Imag_Rdim=reshape(permute(Imag_R(:,dim,:),[1 3 2]),size(im1.data,1),voisinage^2);
      
       
        
    
        minF=min(min(Imag_F(:,dim)));
        maxF=max(max(Imag_F(:,dim)));
        moyF= (minF+maxF)/2;
        
        
       Im_F(1:SizeimX*5,1:SizeimY*5)=minF;
    
        minR=min(min(Imag_R(:,dim)));
        maxR=max(max(Imag_R(:,dim)));
        moyR=(minR+maxR)/2;
    
     

    
    
    for cpt=1:size(im1.data,1)
        
         

                        l=1;
%        
            for j=1:voisinage
                for i=1: voisinage
                
                    compF(i,j)=Imag_Fdim(cpt,l);
                    compR(i,j)=Imag_Rdim(cpt,l); 
                    l=l+1;
                   
                end
            end
            
            Im_F(coordX2(cpt,1)-floor(voisinage/2):coordX2(cpt,1)+floor(voisinage/2),coordY2(cpt,1)-floor(voisinage/2):coordY2(cpt,1)+ floor(voisinage/2))= compF;
            
            
            Im_R(coordX2(cpt,1)-floor(voisinage/2):coordX2(cpt,1)+floor(voisinage/2),coordY2(cpt,1)-floor(voisinage/2):coordY2(cpt,1)+floor(voisinage/2))= compR;
           

   end
       
     % image adjusting values in 0..255
         
         imF=ajusterimage(Im_F,min(min(Imag_F(:,dim))),max(max(Imag_F(:,dim))));
         imR=ajusterimage(Im_R,min(min(Imag_R(:,dim))),max(max(Imag_R(:,dim))));

    % conversion of values in uint8 type for display
       
        imF=uint8(imF);
        imR=uint8(imR);


    % image scores display 

      
        imF=imF(:,:,1);
        
        imF=imresize(imF,[size(ref,1),size(ref,2)],'nearest');
        BW = roicolor(imF,imF(1,1),imF(1,1));
        
        imFc=ind2rgb(uint8(imF),jet(256));
        imFc=uint8(imFc*255);
        
        imFBW(:,:,1)=immultiply(imFc(:,:,1),~BW);
        imFBW(:,:,2)=immultiply(imFc(:,:,2),~BW);
        imFBW(:,:,3)=immultiply(imFc(:,:,3),~BW);
        
        refBW(:,:,1)=immultiply(ref(:,:,1),BW);
        refBW(:,:,2)=immultiply(ref(:,:,2),BW);
        refBW(:,:,3)=immultiply(ref(:,:,3),BW);
        
        compaddF=imadd(imFBW,refBW);
        
        compoi1=imlincomb(0.5,compaddF,0.5,ref);
        figure
        imshow(compoi1)
        stepF=(max(max(Imag_F(:,dim)))- min(min(Imag_F(:,dim))))/6;
        minF=min(min(Imag_F(:,dim)));
        maxF=max(max(Imag_F(:,dim)));
        colorbar
        colorbar('YTickLabel',...
        {num2str(minF),num2str(minF+stepF),num2str(minF+2*stepF),num2str(minF+3*stepF),...
        num2str(minF+4*stepF),num2str(minF+5*stepF),maxF})
     
        cd (rep1)
        saveas(gcf,strcat('CompF',num2str(dim),'.tif'),'tif')

        imR=imR(:,:,1);
        imR=imresize(imR,[size(ref,1),size(ref,2)],'nearest');
        BW = roicolor(imR,imR(1,1),imR(1,1));
        
        
        imRc=ind2rgb(uint8(imR),jet(256));
        imRc=uint8(imRc*255);
        
        imRBW(:,:,1)=immultiply(imRc(:,:,1),~BW);
        imRBW(:,:,2)=immultiply(imRc(:,:,2),~BW);
        imRBW(:,:,3)=immultiply(imRc(:,:,3),~BW);
        
        refBW(:,:,1)=immultiply(ref(:,:,1),BW);
        refBW(:,:,2)=immultiply(ref(:,:,2),BW);
        refBW(:,:,3)=immultiply(ref(:,:,3),BW);
        
        compaddR=imadd(imRBW,refBW);
        
        
        
        
        compoi1=imlincomb(0.5,compaddR,0.5,ref);
        figure
        imshow(compoi1)
        stepR=(max(max(Imag_R(:,dim)))- min(min(Imag_R(:,dim))))/6;
        minR=min(min(Imag_R(:,dim)));
        maxR=max(max(Imag_R(:,dim)));
        colorbar
        colorbar('YTickLabel',...
        {num2str(minR),num2str(minR+stepR),num2str(minR+2*stepR),num2str(minR+3*stepR),...
        num2str(minR+4*stepR),num2str(minR+5*stepR),maxR})
      
        cd (rep2)
        saveas(gcf,strcat('CompR',num2str(dim),'.tif'),'tif')
  
   end

end





% function reconstrucrion_image(im1,Imag_IR,Imag_F)
% 
% 
% coordX=im1.userdata.dX;
% coordY=im1.userdata.dY;
%     
%     
%     
% % lecture des composantes infrarouge obtenus
% comp1i=Imag_IR(:,1);
% comp2i=Imag_IR(:,2);
% comp3i=Imag_IR(:,3);
% % lecture des composantes fluorescence obtenus
% comp1f=Imag_F(:,1);
% comp2f=Imag_F(:,2);
% comp3f=Imag_F(:,3);
% val1=unique(coordX); 
% val2=unique(coordY);
% 
% X=size(val1,1);
% Y=size(val2,1);
% 
% Im_I=zeros(X,Y,3);
% Im_F=zeros(X,Y,3);
% 
% % cr�ation de l' image infrarouge
% 
% for cpt=1:size(im1.data,1)
% Im_I(coordX(cpt,1)-min(coordX)+1,coordY(cpt,1)-min(coordY)+1,2)=comp1i(cpt);
% Im_I(coordX(cpt,1)-min(coordX)+1,coordY(cpt,1)-min(coordY)+1,1)=comp2i(cpt);
% Im_F(coordX(cpt,1)-min(coordX)+1,coordY(cpt,1)-min(coordY)+1,2)=comp1f(cpt);
% Im_F(coordX(cpt,1)-min(coordX)+1,coordY(cpt,1)-min(coordY)+1,1)=comp2f(cpt);
% end
% 
% figure
% imshow(Im_I(:,:,1),[])
% 
% figure
% imshow(Im_I(:,:,2),[])
% 
% figure
% imshow(Im_F(:,:,1),[])
% 
% figure
% imshow(Im_F(:,:,2),[])
% 
% 
% 
% end

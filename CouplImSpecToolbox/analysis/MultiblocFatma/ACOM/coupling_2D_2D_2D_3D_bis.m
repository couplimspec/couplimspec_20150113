function coupling_2D_2D_2D_3D (varargin) 

 %% description
    % coupling and linking three 2 way datatables using multiple co-inertia
    % analysis
    
%% input

   % no inputs
   % two files: -txt file
   %            -raw file
   %            -dso file

    
%% output
    
    %dso: datasetobject of the datatable reduced

%% principe
   % coupling two datasets having the same number of ways (2 dimensions)

%% use
   % coupling_2D_2D()
   % coupling_2D_2D(file1,file2,file3,dimension)
    
%% Comments
    % writen for F.Allouche Phd Thesis
    
    
%% Author
    % F.Allouche
    
%% date
    % 23/06/2011    

%% context variables 
          % returns the current directory

close all
clear all
orig=pwd; 
%% input


if nargin==0
 
  % input of files   
  [nom1,rep1]=uigetfile({'*.dso.mat;*.txt;*.raw;'},'name of the infrared datatable','*.dso.mat;*.txt;*.raw;');
  cd(rep1)
  [nom2,rep2]=uigetfile({'*.dso.mat;*.txt;*.raw;'},'name of the fluorescence datatable','*.dso.mat;*.txt;*.raw;');
  cd(rep2)
  [nom3,rep3]=uigetfile({'*.dso.mat;*.txt;*.raw;'},'name of the Raman datatble','*.dso.mat;*.txt;*.raw;');
  cd(rep3)
  
  % input of data table 1
  cd (rep1)
  point=strfind(nom1,'.');
  point=point(length(point));
  ext=nom1((point+1):(length(nom1)));
  if strcmp(ext,'mat')
        
    load(nom1);
    X1=dso;
    lo1=X1.axisscale{2};
    clear dso;
    X1=X1.data;
    
 else
    error('format .dso.mat or .raw or .txt expected')
 end
 

% input of data table 2
  
 cd (rep2)
 point=strfind(nom2,'.');
 point=point(length(point));
 ext=nom2((point+1):(length(nom2)));
 
 if strcmp(ext,'mat')
        
    load(nom2);
    X2=dso;
    lo2=X2.axisscale{2};
    clear dso;
  
    
    % compute average datatable if 3 way datatable
    if length(size(X2.data))==3
        T2=X2.data;
        X2=mean(X2.data,3);
        
    else
        X2=X2.data;
    end;

 else
     
     error('format .dso.mat or .raw or .txt attendu')
     
 end

% input of data table 3
  
  cd (rep3)
  point=strfind(nom3,'.');
  point=point(length(point));
  ext=nom3((point+1):(length(nom3)));
  
  if strcmp(ext,'mat')
        
    load(nom3);
    X3=dso;
    lo3=X3.axisscale{2};
    clear dso;
  
    
    % compute average datatable if 3 way datatable
    if length(size(X3.data))==3
        T3=X3.data;
        X3=mean(X3.data,3);
    else
        X3=X3.data;
    end;
    

 else
    error('format .dso.mat or .raw or .txt attendu')
 end
 

% input number of dimension to be analysed
ok=0;
while ~ok
    prompt = {'Enter the dimension '};
    dlg_title = 'dimension';
    num_lines = 1;

    answer = inputdlg(prompt,dlg_title,num_lines);
    dimension=str2double(cell2mat (answer)); %#ok<ST2NM>
    if strcmp(class(dimension),'double')
        ok=1;
    else 
        ok=0;
    end
end
end

% three input parameters (function input parameters)
if nargin==3
    
    X1=varargin{1};
    X2=varargin{2};
    X3=varargin{3};
    dimension=varargin{4};
    
    if strcmp(class(X1),'dataset')
            
            X1=X1.data;
            
            if length(size(X1.data))==3
                X1=mean(X2.data,3);
            end;
   
    elseif strcmp(class(X1),'double')
            
            if length(size(X1))==3
                X1=mean(X1,3);
            end;
    else
            error('format .dso.mat or .raw or .txt expected')
    end;
    
    
    if strcmp(class(X2),'dataset')
            
            X2=X2.data;
            if length(size(X2.data))==3
                X2=mean(X2.data,3);
            end;
   
    elseif strcmp(class(X2),'double')
            if ength(size(X2.data))==3
                X2=mean(X2,3);
            end;
    else
            error('format .dso.mat or .raw or .txt expected')
    end;
    
    if strcmp(class(X3),'dataset')
            
            X3=X3.data;
            if length(size(X3.data))==3
                X3=mean(X3.data,3);
            end;
   
    elseif strcmp(class(X3),'double')
           
            if length(size(X3.data))==3
                X3=mean(X3,3);
            end;
    else
            error('format .dso.mat or .raw or .txt expected')
    end;
    
end;

if nargin >4
     
    error('use: coupling2D_2D or coupling2D_2D(file1,file2)');
    
end;

%% treatement

 X11=X1-ones(size(X1,1),1)*mean(X1);
 X22=X2-ones(size(X2,1),1)*mean(X2);
 X33=X3-ones(size(X3,1),1)*mean(X3);


%coupling datatables using multiple co-inertia analysis

 [U,V1,V2,V3,L,CX1,CX2,CX3,method]= McoA (X11,X22,X33,dimension);
  
Computing_parameters_ACOM(X11,X22,X33,CX1,CX2,CX3,U,L, dimension)
 
 
 
 % Calcul des paramètres
 
 Computing_parameters(X11,CX1,CX2,CX3,U,L,dimension)
 L1=L(size(X11,2):end,:);
 Computing_parameters(X22,CX2,CX1,CX3,U,L1,dimension)
 L2=L1(size(X22,2):end,:);
 Computing_parameters(X33,CX3,CX1,CX2,U,L2,dimension)
 
 
 % folder number 1 
    cd(rep1)
    load(nom1)
    im1=dso;
    clear('dso')
    createdso(V1,'loading',im1);
    createdso(CX1,'score',im1);
    createdso(U,'common.score',im1);
  % folder number 2 
    cd(rep2)
    load(nom2);
    im2=dso;
    clear ('dso')
    createdso(V2,'loading',im2); 
    createdso(CX2,'score',im2);
   % folder number 3 
    figure(3);
    cd(rep3)
    load(nom3);
    im3=dso;
    clear('dso');
    createdso(V3,'loading',im3);
    createdso(CX3,'score',im3);
   


% total variance
inertiaX1=trace((1/size(X11,1))*(X11'*X11));
inertiaX2=trace((1/size(X22,1))*(X22'*X22));
inertiaX3=trace((1/size(X33,1))*(X33'*X33));



%computing variance for the three datatables
varX1=(1/size(X11,1))*diag(CX1'*CX1)
varX2=(1/size(X22,1))*diag(CX2'*CX2)
varX3=(1/size(X33,1))*diag(CX3'*CX3)

% variance percentage
pvarX1=(1/inertiaX1)*varX1;
pvarX1=(100*pvarX1) %#ok<NASGU>

pvarX2=(1/inertiaX2)*varX2;
pvarX2=(100*pvarX2)

pvarX3=(1/inertiaX3)*varX3;
pvarX3=(100*pvarX3)

% covariance percentage

for dim=1:dimension
    
    TIR(:,:,dim)=CX1(:,dim)*U(:,dim)' ;
    TF(:,:,dim)=CX2(:,dim)*U(:,dim)' ;
    TR(:,:,dim)=CX3(:,dim)*U(:,dim)' ;
%     T(:,:,dim)=[CX1(:,dim)*U(:,dim)' CX2(:,dim)*U(:,dim)' CX3(:,dim)*U(:,dim)'];
    T(:,:,dim)=[CX1(:,dim) CX2(:,dim) CX3(:,dim)];
    d=svds(T(:,:,dim),1);
    d1=svds(TIR(:,:,dim),1);
    d2=svds(TF(:,:,dim),1);
    d3=svds(TR (:,:,dim),1);
    
    %percentage of contribution
    
    pourcentage1(dim,1)=d1*d1/trace(T(:,:,dim)'*T(:,:,dim))
    pourcentage2(dim,1)=d2*d2/trace(T(:,:,dim)'*T(:,:,dim))
    pourcentage3(dim,1)=d3*d3/trace(T(:,:,dim)'*T(:,:,dim))
    
    % correlation coeffecient
    
    corr=corrcoef ([CX1(:,dim) CX2(:,dim) CX3(:,dim) U(:,dim)])
    correlation(:,:,dim)=corr;
    

end        
%% Save loadings and image scores
  

% reconstruction 
  
   
    voisinage=5;
    CX22=zeros(size(X2,1),dimension,voisinage^2);
    CX33=zeros(size(X3,1),dimension,voisinage^2);
    
    X=cat(2,X11,X22,X33);
   
     for k=1:size(T2,3)
        
        T2(:,:,k)=T2(:,:,k)-ones(size(T2(:,:,k),1),1)*mean(X2);
        T3(:,:,k)=T3(:,:,k)-ones(size(T3(:,:,k),1),1)*mean(X3);  
        
        CX22(:,:,k)=T2(:,:,k)*V2;
        CX33(:,:,k)=T3(:,:,k)*V3;
        
     end
     
     cd (rep2)
     createdso(CX22,'projected.score',im2);
     
     cd(rep3)
     createdso(CX33,'projected.score',im3); 
   
  
    
    reconstruction_imagedso(V1,V2,V3,im1,im2,im3,CX1,CX22,CX33,U,pvarX1,pvarX2,pvarX3,pourcentage1,pourcentage2,pourcentage3,correlation,dimension,rep1,rep2,rep3,voisinage,method);

    % image scores reconstruction
   
    
%% matlab function tracking  

fid=fopen(strcat(im1.name,'.treat','.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end;

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'Compute loadings and image scores using multiple co-inertia analysis MCoA \r\n');
fprintf(fid,'__________________________________________________________________________\r\n');

fprintf(fid,'\r\ninput file name: %s%s\r\n',nom1,'.dso.mat');
fprintf(fid,'data folder: %s\r\n',rep1);
fprintf(fid,'\r\ninput file name: %s%s\r\n',nom2,'.dso.mat');
fprintf(fid,'data folder: %s\r\n',rep2);
fprintf(fid,'\r\ninput file name: %s%s\r\n',nom3,'.dso.mat');
fprintf(fid,'data folder: %s\r\n',rep3);

for dim=1:dimension  
    
    fprintf(fid,'\r\nsaved file name : %s \r\n',strcat(im1.name,'LoadingR',num2str(dim),'.tif'));
    fprintf(fid,'data folder: %s\r\n',rep1);
    fprintf(fid,'\r\nsaved file name : %s \r\n',strcat(im1.name,'CompIR',num2str(dim),'.tif'));
    fprintf(fid,'data folder: %s\r\n',rep1);
    
end

for dim=1:dimension  
    
    fprintf(fid,'\r\nsaved file name : %s \r\n',strcat(im2.name,'LoadingR',num2str(dim),'.tif'));
    fprintf(fid,'data folder: %s\r\n',rep2);
    fprintf(fid,'\r\nsaved file name : %s \r\n',strcat(im2.name,'CompIR',num2str(dim),'.tif'));
    fprintf(fid,'data folder: %s\r\n',rep2);
    
end

for dim=1:dimension 
    
    fprintf(fid,'\r\nsaved file name : %s \r\n',strcat(im3.name,'LoadingR',num2str(dim),'.tif'));
    fprintf(fid,'data folder: %s\r\n',rep3);
    fprintf(fid,'\r\nsaved file name : %s \r\n',strcat(im3.name,'CompIR',num2str(dim),'.tif'));
    fprintf(fid,'data folder: %s\r\n',rep3);
    
end


% save of function used

fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end

cd (orig)    
end




function reconstruction_imagedso(V1,V2,V3,im1,im2,im3,Imag_IR,Imag_F,Imag_R,Imag_Com,pvarX1,pvarX2,pvarX3,pourcentage1,pourcentage2,pourcentage3,correlation,dimension,rep1,rep2,rep3,voisinage,method)

    


    SizeimX=11 %im1.imagesize(1);
    SizeimY=50 %im1.imagesize(2);
    
    % Get back coordinates values of pixels analysed
    coordX=im1.userdata.dX;
    coordY=im1.userdata.dY;
    
    
    coordX2=im2.userdata.dX;
    coordY2=im2.userdata.dY;
    
    % Compute the unique values of coordinates
    val1=unique(coordX); 
    val2=unique(coordY);
    
      % Compute the optimum size of the image score
    X=size(val1,1);
    Y=size(val2,1);
    
  
    [nomim,repim]=uigetfile({'*.tif'},'name of the region selected','*.tif');
    cd(repim)
    ref=imread(nomim);
    ref(:,:,3)=ref(:,:,1);
    ref(:,:,2)=ref(:,:,1);

    
   % Computing all score images Infrared Raman and Fluorescence within the
   % number of dimensions selected
   for dim=1:dimension
    
       
       
       Imag_Fdim=reshape(permute(Imag_F(:,dim,:),[1 3 2]),size(im1.data,1),voisinage^2);
       Imag_Rdim=reshape(permute(Imag_R(:,dim,:),[1 3 2]),size(im1.data,1),voisinage^2);
      
       
        minCom=min(min(Imag_Com(:,dim)));
        maxCom=max(max(Imag_Com(:,dim)));
        moyCom=(minCom+maxCom)/2;
       
        Im_Com(1:SizeimX,1:SizeimY)=minCom;
       
       
        minIR=min(min(Imag_IR(:,dim)));
        maxIR=max(max(Imag_IR(:,dim)));
        moyIR=(minIR+maxIR)/2;
       
        Im_IR(1:SizeimX,1:SizeimY)=minIR;
   
    
        minF=min(min(Imag_F(:,dim)));
        maxF=max(max(Imag_F(:,dim)));
        moyF= (minF+maxF)/2;
        
        
       Im_F(1:SizeimX*5,1:SizeimY*5)=minF;
    
        minR=min(min(Imag_R(:,dim)));
        maxR=max(max(Imag_R(:,dim)));
        moyR=(minR+maxR)/2;
    
       Im_R (1:SizeimX*5,1:SizeimY*5)=minR;

    
       

       mini_im=min([minIR,minR,minF]);
       max_im=max([maxIR,maxR,maxF]);
       
       Im_IR(1:SizeimX,1:SizeimY)=mini_im;
       Im_Com(1:SizeimX,1:SizeimY)=minCom;
       Im_F(1:SizeimX*5,1:SizeimY*5)=mini_im;
       Im_R (1:SizeimX*5,1:SizeimY*5)=mini_im;
    
    
    for cpt=1:size(im1.data,1)
        
            Im_IR(coordX(cpt,1),coordY(cpt,1))=Imag_IR(cpt,dim);  
            Im_Com(coordX(cpt,1),coordY(cpt,1))=Imag_Com(cpt,dim);  

                        l=1;
%        
            for j=1:voisinage
                for i=1: voisinage
                
                    compF(i,j)=Imag_Fdim(cpt,l);
                    compR(i,j)=Imag_Rdim(cpt,l); 
                    l=l+1;
                   
                end
            end
            
            Im_F(coordX2(cpt,1)-floor(voisinage/2):coordX2(cpt,1)+floor(voisinage/2),coordY2(cpt,1)-floor(voisinage/2):coordY2(cpt,1)+ floor(voisinage/2))= compF;
            
            
            Im_R(coordX2(cpt,1)-floor(voisinage/2):coordX2(cpt,1)+floor(voisinage/2),coordY2(cpt,1)-floor(voisinage/2):coordY2(cpt,1)+floor(voisinage/2))= compR;
          

   end
       

     % image adjusting values in 0..255
     
     
         imIR=ajusterimage(Im_IR,min(min(Imag_IR(:,dim))),max(max(Imag_IR(:,dim))));
         imCom=ajusterimage(Im_Com,min(min(Imag_Com(:,dim))),max(max(Imag_Com(:,dim))));
         imF=ajusterimage(Im_F,min(min(Imag_F(:,dim))),max(max(Imag_F(:,dim))));
         imR=ajusterimage(Im_R,min(min(Imag_R(:,dim))),max(max(Imag_R(:,dim))));



    
         
     % conversion of values in uint8 type for display
        imIR=uint8(imIR);
        imF=uint8(imF);
        imR=uint8(imR);
        imCom=uint8(imCom);
  
    % display common component results

        imCom=imCom(:,:,1);
        
        imCom=imresize(imCom,[size(ref,1),size(ref,2)],'nearest'); 
        BW = roicolor(imCom,imCom(1,1),imCom(1,1));
       
        imComc=ind2rgb(uint8(imCom),jet(256));
        imComc=uint8(imComc*255);
        
        imComBW(:,:,1)=immultiply(imComc(:,:,1),~BW);
        imComBW(:,:,2)=immultiply(imComc(:,:,2),~BW);
        imComBW(:,:,3)=immultiply(imComc(:,:,3),~BW);
        
        refBW(:,:,1)=immultiply(ref(:,:,1),BW);
        refBW(:,:,2)=immultiply(ref(:,:,2),BW);
        refBW(:,:,3)=immultiply(ref(:,:,3),BW);
        
        compaddIR=imadd(imComBW,refBW);
     
        figure 
        compoicom=imlincomb(0.5,compaddIR,0.5,ref);
        imshow(compoicom) 
        
      
        stepCom=(max_im-mini_im)/6;
       
        colorbar
        colorbar('YTickLabel',...
        {num2str(round(1000*minCom)*0.001),num2str(round((minCom+stepCom)*1000)*0.001),num2str(round((minCom+2*stepCom)*1000)*0.001),num2str(round((minCom+3*stepCom)*1000)*0.001),...
        num2str(round((minCom+4*stepCom)*1000)*0.001),num2str(round((minCom+5*stepCom)*1000)*0.001),num2str(round(1000*maxCom)*0.001)})
        cd (rep1)
        saveas(gcf,strcat('CompCom',method,num2str(dim),'.tif'),'tif')
 




     
    
    
    
    
    
    
    
    
    % Display infrared results
    figure
    plot(str2num(im1.label{2}),V1(:,dim),'LineWidth',3)
    hold on
    plot(str2num(im1.label{2}),0,'--','LineWidth',3)
    
    xlabel([ im1.title{2} ' (' im1.userdata.unit, ')'],'fontsize',16)
    if strcmp(im1.userdata.unit,'cm-1')
        set(gca,'xdir','reverse')
    end;
    
  
   
    switch im1.userdata.method
        case 'INFRARED'
            ylabel('arbitrary unit','fontsize',16);
            title(strcat(im1.name,'.',im1.userdata.method,'.dimension=', num2str(dim)),'fontsize',16)
            set(gca,'fontsize',14)
            saveas(gcf,strcat(im1.name,method,'LoadingIR',num2str(dim),'.tif'));
        
        case 'RAMAN'
            ylabel('arbitrary unit','fontsize',16);
            title(strcat(im1.name,'.',im1.userdata.method,'.dimension=', num2str(dim)),'fontsize',16)
            set(gca,'fontsize',14)
            saveas(gcf,strcat(im1.name,method,'LoadingR',num2str(dim),'.tif'));
       
        case 'FLUORESCENCE'
            ylabel('arbitrary unit','fontsize',16);
            title(strcat(im1.name,'.',im1.userdata.method,'.dimension=', num2str(dim)),'fontsize',16)
            set(gca,'fontsize',14)
            saveas(gcf,strcat(im1.name,method,'LoadingF',num2str(dim),'.tif'));
        otherwise
            ylabel('')
   end;
  

   
   
   
   
   % Infrared scores
        imIR=imIR(:,:,1);
        
        imIR=imresize(imIR,[size(ref,1),size(ref,2)],'nearest'); 
        BW = roicolor(imIR,imIR(1,1),imIR(1,1));
       
        imIRc=ind2rgb(uint8(imIR),jet(256));
        imIRc=uint8(imIRc*255);
        
        imIRBW(:,:,1)=immultiply(imIRc(:,:,1),~BW);
        imIRBW(:,:,2)=immultiply(imIRc(:,:,2),~BW);
        imIRBW(:,:,3)=immultiply(imIRc(:,:,3),~BW);
        
        refBW(:,:,1)=immultiply(ref(:,:,1),BW);
        refBW(:,:,2)=immultiply(ref(:,:,2),BW);
        refBW(:,:,3)=immultiply(ref(:,:,3),BW);
        
        compaddIR=imadd(imIRBW,refBW);
     
        compoi1=imlincomb(0.5,compaddIR,0.5,ref);
        
        figure
        imshow(compoi1) 
        
        Xlim=get(gca,'Xlim');
        Ylim=get(gca,'Ylim');
       
%         text(Xlim(2)+(Xlim(2)-Xlim(1)),Ylim(1),strcat('percentage of participation','=',num2str(pourcentage1(dim,1))),'fontsize',14)
%         text(Xlim(2)+(Xlim(2)-Xlim(1)),Ylim(1)+((Ylim(2)-Ylim(1))/4),strcat('variance explicated','=',num2str(pvarX1(dim,1))),'fontsize',14)
%         text(Xlim(2)+(Xlim(2)-Xlim(1)),Ylim(1)+((Ylim(2)-Ylim(1))/4)*2,strcat('correlation fluorescence/infrared','=',num2str(correlation(2,1,dim))),'fontsize',14)
%         text(Xlim(2)+(Xlim(2)-Xlim(1)),Ylim(1)+((Ylim(2)-Ylim(1))/4)*3,strcat('correlation raman/infrared','=',num2str(correlation(1,3,dim))),'fontsize',14)
%         text(Xlim(2)+(Xlim(2)-Xlim(1)),Ylim(1)+((Ylim(2)-Ylim(1))),strcat('correlation common/infrared','=',num2str(correlation(4,1,dim))),'fontsize',14)
%         
        
        stepIR=(max(max(Imag_IR(:,dim)))-min(min(Imag_IR(:,dim))))/6;
        minIR=min(min(Imag_IR(:,dim)));
        maxIR=max(max(Imag_IR(:,dim)));

%         stepIR=(max_im-mini_im)/6;
%         minIR=mini_im;
%         maxIR=max_im;
%        
        colorbar
        colorbar('YTickLabel',...
        {num2str(round(1000*minIR)*0.001),num2str(round((minIR+stepIR)*1000)*0.001),num2str(round((minIR+2*stepIR)*1000)*0.001),num2str(round((minIR+3*stepIR)*1000)*0.001),...
        num2str(round((minIR+4*stepIR)*1000)*0.001),num2str(round((minIR+5*stepIR)*1000)*0.001),num2str(round(1000*maxIR)*0.001)})
        cd (rep1)
        saveas(gcf,strcat('CompIR',method,num2str(dim),'.tif'),'tif')
 




        figure
       
        plot(str2num(im2.label{2}),V2(:,dim),'LineWidth',3)
        hold on
        plot(str2num(im2.label{2}),0,'--','LineWidth',5)
        xlabel([ im2.title{2} ' (' im2.userdata.unit, ')'],'fontsize',16)
    
    
 
   
   if strcmp(im2.userdata.unit,'cm-1')
        set(gca,'xdir','reverse')
   end;
   %


   % Display fluorescence results
    switch im2.userdata.method
        case 'INFRARED'
            ylabel('arbitrary unit','fontsize',16);
            title(strcat(im2.name,'.',im2.userdata.method,'.dimension=', num2str(dim)),'fontsize',16)
            set(gca,'fontsize',14)
            saveas(gcf,strcat(im2.name,method,'LoadingIR',num2str(dim),'.tif'));
        case 'RAMAN'
            ylabel('arbitrary unit','fontsize',16);
            title(strcat(im2.name,'.',im2.userdata.method,'.dimension=', num2str(dim)),'fontsize',16)
            set(gca,'fontsize',14)
            saveas(gcf,strcat(im2.name,method,'LoadingR',num2str(dim),'.tif'));
        case 'FLUORESCENCE'
            ylabel('arbitrary unit','fontsize',16);
            title(strcat(im2.name,'.',im2.userdata.method, '.dimension=', num2str(dim)),'fontsize',16)
            set(gca,'fontsize',14)
            saveas(gcf,strcat(im2.name,method,'LoadingF',num2str(dim),'.tif'));
        otherwise
            ylabel('')
    end;
        
        % fluorescence scores
        imF=imF(:,:,1);
        
        imF=imresize(imF,[size(ref,1),size(ref,2)],'nearest');
        BW = roicolor(imF,imF(1,1),imF(1,1));
        
        imFc=ind2rgb(uint8(imF),jet(256));
        imFc=uint8(imFc*255);
        
        imFBW(:,:,1)=immultiply(imFc(:,:,1),~BW);
        imFBW(:,:,2)=immultiply(imFc(:,:,2),~BW);
        imFBW(:,:,3)=immultiply(imFc(:,:,3),~BW);
        
        refBW(:,:,1)=immultiply(ref(:,:,1),BW);
        refBW(:,:,2)=immultiply(ref(:,:,2),BW);
        refBW(:,:,3)=immultiply(ref(:,:,3),BW);
        
        compaddF=imadd(imFBW,refBW);
        
        compoi2=imlincomb(0.5,compaddF,0.5,ref);
       
        figure
        imshow(compoi2)
        
        Xlim=get(gca,'Xlim');
        Ylim=get(gca,'Ylim');
        
%         text(Xlim(2)+(Xlim(2)-Xlim(1)),Ylim(1),strcat('percentage of participation','=',num2str(pourcentage2(dim,1))),'fontsize',14)
%         text(Xlim(2)+(Xlim(2)-Xlim(1)),Ylim(1)+((Ylim(2)-Ylim(1))/4),strcat('variance explicated','=',num2str(pvarX2(dim,1))),'fontsize',14)
%         text(Xlim(2)+(Xlim(2)-Xlim(1)),Ylim(1)+((Ylim(2)-Ylim(1))/4)*2,strcat('correlation fluorescence/infrared','=',num2str(correlation(2,1,dim))),'fontsize',14)
%         text(Xlim(2)+(Xlim(2)-Xlim(1)),Ylim(1)+((Ylim(2)-Ylim(1))/4)*3,strcat('correlation fluorescence/raman','=',num2str(correlation(2,3,dim))),'fontsize',14)
%         text(Xlim(2)+(Xlim(2)-Xlim(1)),Ylim(1)+((Ylim(2)-Ylim(1))),strcat('correlation fluorescence/common','=',num2str(correlation(4,2,dim))),'fontsize',14)
       
        stepF=(max(max(Imag_F(:,dim)))- min(min(Imag_F(:,dim))))/6;
        minF=min(min(Imag_F(:,dim)));
        maxF=max(max(Imag_F(:,dim)));
%       
%         stepF=(max_im-mini_im)/6;
%         minF=mini_im;
%         maxF=max_im;
        colorbar
        colorbar('YTickLabel',...
        {num2str(round(1000*minF)*0.001),num2str(round((minF+stepF)*1000)*0.001),num2str(round((minF+2*stepF)*1000)*0.001),num2str(round((minF+3*stepF)*1000)*0.001),...
        num2str(round((minF+4*stepF)*1000)*0.001),num2str(round((minF+5*stepF)*1000)*0.001),num2str(round(1000*maxF)*0.001)})
     
        cd (rep2)
        saveas(gcf,strcat('CompF',method,num2str(dim),'.tif'),'tif')


       figure
       plot(str2num(im3.label{2}),V3(:,dim),'LineWidth',3)
       hold on
       plot(str2num(im3.label{2}),0,'--','LineWidth',5)
       xlabel([ im3.title{2} ' (' im3.userdata.unit, ')'],'fontsize',16)

       
  % Display Raman results
        % Raman loadings
       if strcmp(im3.userdata.unit,'cm-1')
            set(gca,'xdir','reverse')
       end;

         switch im3.userdata.method
            case 'INFRARED'
                ylabel('arbitrary unit','fontsize',16);
                title(strcat(im3.name,'.',im3.userdata.method, '.dimension=', num2str(dim)),'fontsize',16)
                set(gca,'fontsize',14)
                saveas(gcf,strcat(im3.name,method,'LoadingIR',num2str(dim),'.tif'));
            case 'RAMAN'
                ylabel('arbitrary unit','fontsize',16);
                title(strcat(im3.name,'.',im3.userdata.method, '.dimension=', num2str(dim)),'fontsize',16)
                set(gca,'fontsize',14)
                saveas(gcf,strcat(im3.name,method,'LoadingR',num2str(dim),'.tif'));
            case 'FLUORESCENCE'
                ylabel('arbitrary unit','fontsize',16);
                title(strcat(im3.name,'.',im3.userdata.method, '.dimension=', num2str(dim)),'fontsize',16)
                set(gca,'fontsize',14)
                saveas(gcf,strcat(im3.name,method,'LoadingF',num2str(dim),'.tif'));
            otherwise
                ylabel('')
        end;  
        
        % Raman scores
        imR=imR(:,:,1);
        imR=imresize(imR,[size(ref,1),size(ref,2)],'nearest');
        BW = roicolor(imR,imR(1,1),imR(1,1));
        
        
        imRc=ind2rgb(uint8(imR),jet(256));
        imRc=uint8(imRc*255);
        
        imRBW(:,:,1)=immultiply(imRc(:,:,1),~BW);
        imRBW(:,:,2)=immultiply(imRc(:,:,2),~BW);
        imRBW(:,:,3)=immultiply(imRc(:,:,3),~BW);
        
        refBW(:,:,1)=immultiply(ref(:,:,1),BW);
        refBW(:,:,2)=immultiply(ref(:,:,2),BW);
        refBW(:,:,3)=immultiply(ref(:,:,3),BW);
        
        compaddR=imadd(imRBW,refBW)
        
        compoi3=imlincomb(0.5,compaddR,0.5,ref);
        figure
        imshow(compoi3)
       
       
        stepR=(max(max(Imag_R(:,dim)))- min(min(Imag_R(:,dim))))/6;
        minR=min(min(Imag_R(:,dim)));
        maxR=max(max(Imag_R(:,dim)));
       
%         stepR=(max_im-mini_im)/6;
%         minR=mini_im;
%         maxR=max_im;
%         
        
        
        colorbar
        colorbar('YTickLabel',...
        {num2str(round(1000*minR)*0.001),num2str(round((minR+stepR)*1000)*0.001),num2str(round((minR+2*stepR)*1000)*0.001),num2str(round((minR+3*stepR)*1000)*0.001),...
        num2str(round((minR+4*stepR)*1000)*0.001),num2str(round((minR+5*stepR)*1000)*0.001),num2str(round(1000*maxR)*0.001)})
        cd (rep3) 
        
        Xlim=get(gca,'Xlim');
        Ylim=get(gca,'Ylim');
        
%         text(Xlim(2)+(Xlim(2)-Xlim(1)),Ylim(1),strcat('percentage of participation','=',num2str(pourcentage3(dim,1))),'fontsize',14)
%         text(Xlim(2)+(Xlim(2)-Xlim(1)),Ylim(1)+((Ylim(2)-Ylim(1))/4),strcat('variance explicated','=',num2str(pvarX3(dim,1))),'fontsize',14)
%         text(Xlim(2)+(Xlim(2)-Xlim(1)),Ylim(1)+((Ylim(2)-Ylim(1))/4)*2,strcat('correlation raman/infrared','=',num2str(correlation(3,1,dim))),'fontsize',14)
%         text(Xlim(2)+(Xlim(2)-Xlim(1)),Ylim(1)+((Ylim(2)-Ylim(1))/4)*3,strcat('correlation fluorescence/raman','=',num2str(correlation(2,3,dim))),'fontsize',14)
%         text(Xlim(2)+(Xlim(2)-Xlim(1)),Ylim(1)+((Ylim(2)-Ylim(1))),strcat('correlation common/raman','=',num2str(correlation(4,3,dim))),'fontsize',14)
%         
        saveas(gcf,strcat('CompIR',method,num2str(dim),'.tif'),'tif')

      comp_overload(:,:,1)=imIR;
      comp_overload(:,:,2)=imF;
      comp_overload(:,:,3)=imR;
        
      BW = roicolor(comp_overload(:,:,1), comp_overload(1,1), comp_overload(1,1));
        
        comp_overloadBW(:,:,1)=immultiply( comp_overload(:,:,1),~BW);
        comp_overloadBW(:,:,2)=immultiply( comp_overload(:,:,2),~BW);
        comp_overloadBW(:,:,3)=immultiply( comp_overload(:,:,3),~BW);
        
        refBW(:,:,1)=immultiply(ref(:,:,1),BW);
        refBW(:,:,2)=immultiply(ref(:,:,2),BW);
        refBW(:,:,3)=immultiply(ref(:,:,3),BW);
        
        compadd=imadd(comp_overloadBW,refBW)
        
       comp_overload=imlincomb(0.7,compadd,0.3,ref);
       figure
       imshow(comp_overload)
        saveas(gcf,strcat('CompOverload',method,num2str(dim),'.tif'),'tif') 
        
   end

 end



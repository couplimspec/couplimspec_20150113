
function [U,V1,V2,V3,L,CX1,CX2,CX3,method]= McoA (X1,X2,X3,dimension)
  %% description
    
    % coupling and linking three 2 way datatables using multiple co-inertia
    % analysis
    
%% input

   
   % Three datatables  X1,X2,X3        
   % dimensions to be analysed after decomposition
    
%% output
    
    % U global loadings
    % V1 loading of the first datatable
    % V2 loading of the second datatable
    % V3 loading of the third datatable

%% principe
   
    % coupling two datasets having the same number of ways (2 dimensions)

%% use
  
    % [U,V1,V2,V3]= McoA (X1,X2,X3,dimension)
  
    
%% Comments
    
    % writen for F.Allouche Phd Thesis
    
    
%% Author
   
    % F.Allouche
    
%% date
    % 23/06/2011    

%% input variables
     
     [x1,y1]=size(X1);
     [x2,y2]=size(X2);
     [x3,y3]=size(X3);

%% Treatment     
% initialisation

    dim=1;
    v1= zeros(y1,1);
    v2= zeros(y2,1);
    v3= zeros(y3,1);
    X11=X1;
    X22=X2;
    X33=X3;
    
      
       % choice of the method
%     choix={'Hanafi','Wold', 'QI'};
%     codemethod={'HANAFI','WOLD','QI'};
%     [nummethod,ok] = listdlg('ListString',choix, 'SelectionMode','single','PromptString','choose method',...
%         'name','method to compute grey level image','listsize',[300 160]);
%     if ok 
%         method=codemethod{nummethod};
%     else if isempty(nummethod)  % cancel of no method chosen
%             disp('method choice cancelled showspectrimage');
%             return;
%         end;
%     end;
    method='HANAFI';
% iteration
    while (dim <= dimension)  
       
        [v1,v2,v3,u,l]=compute_MCoA_component(X1,X2,X3); 
        
        %X1tot(:,:,dim)=X1;
       % X2tot(:,:,dim)=X2;
        %X3tot(:,:,dim)=X3;
        
        U(:,dim)=u;
        V1(:,dim)=v1;
        V2(:,dim)=v2;
        V3(:,dim)=v3; 
        L(:,dim)=l;
        CX1(:,dim)=X1*v1;
        CX2(:,dim)=X2*v2;
        CX3(:,dim)=X3*v3;
        
        % normalisation
        CX1n(:,dim)=X1*v1/norm(X1*v1);
        CX2n(:,dim)=X2*v2/norm(X2*v2);
        CX3n(:,dim)=X3*v3/norm(X3*v3);
        
         
        
        
        
        
        
        % deflation MCOA: CPCA with orthogonal block loadings : bon
        % compromis
        
%         X1=X1-CX1*v1';
%         X2=X2-CX2*v2';
%         X3=X3-CX3*v3';

 switch method

    case 'QI'
        % deflation QI
             P1=eye(size(X11,1))-CX1n(:,1:dim)*CX1n(:,1:dim)';
             P2=eye(size(X22,1))-CX2n(:,1:dim)*CX2n(:,1:dim)';
             P3=eye(size(X33,1))-CX3n(:,1:dim)*CX3n(:,1:dim)';
             X1=P1*X11;
             X2=P2*X22;
             X3=P3*X33;  
    case 'HANAFI'  
% deflation Hanafi
             P1=eye(size(X11,2))-V1(:,1:dim)*V1(:,1:dim)';
             P2=eye(size(X22,2))-V2(:,1:dim)*V2(:,1:dim)';
             P3=eye(size(X33,2))-V3(:,1:dim)*V3(:,1:dim)';
             X1=X11*P1;
             X2=X22*P2;
             X3=X33*P3;
 case 'WOLD'
%              % deflation Wold
             P1=eye(size(X11,1))-U(:,1:dim)*U(:,1:dim)';
             P2=eye(size(X22,1))-U(:,1:dim)*U(:,1:dim)';
             P3=eye(size(X33,1))-U(:,1:dim)*U(:,1:dim)';
             X1=P1*X11;
             X2=P2*X22;
             X3=P3*X33; 
           
 otherwise
 end;
         
       

% % deflation  CPCA with orthogonal global scores  ==> redondance des
% profils spectraux ???
%         X1=X1-u*u'*X1;
%         X2=X2-u*u'*X2;
%         X3=X3-u*u'*X3;



      % % deflation  CPCA with orthogonal block scores   : qualit�c des composantes images 
%         CX1=CX1/sqrt(CX1'*CX1)
%         %X1=X1-((CX1*CX1')/(CX1'*CX1))*X1;
%         X1=X1-CX1*CX1'*X1;
%         
%         %X2=X2-((CX2*CX2')/(CX2'*CX2))*X2;
%         CX2=CX2/sqrt(CX2'*CX2)
%         X2=X2-CX2*CX2'*X2;
%         
%         %X3=X3-((CX3*CX3')/(CX3'*CX3))*X3;
%         CX3=CX3/sqrt(CX3'*CX3);
%         X3=X3-CX3*CX3'*X3;

%         for i=1:dim
%              
%              X1=X11-CX1(:,dim)*(CX1(:,dim))';
%              X2=X22-CX2(:,dim)*(CX2(:,dim))';
%              X3=X33-CX3(:,dim)*(CX3(:,dim))';
%             
%         end


        dim=dim+1;
        
    end
    
           
end

function [v1,v2,v3,u,L]=compute_MCoA_component(X1,X2,X3)

% compute first component
 
 X=cat(2,X1,X2,X3);
 
 [U,S,L]=svds(X,1);
 
 [x1,y1]=size(X1);
 [x2,y2]=size(X2);
 [x3,y3]=size(X3);
 
 % compute loadings
 
 u=U(:,1);
 L=L(:,1);
 
% computing percentage of participation
 

% %  figure
% %  plot (V)


%  
%  Xlim=get(gca,'Xlim');
%  Ylim=get(gca,'Ylim');
%  text(Xlim(2)/2,Ylim(1)+((Ylim(2)-Ylim(1))/8),strcat('percentage of infrared','=',num2str(round(p1*1000)*0.1)),'fontsize',12)
%  text(Xlim(2)/2,Ylim(1)+((Ylim(2)-Ylim(1))/8)*2,strcat('percentage of fluorescence','=',num2str(round(p2*1000)*0.1)),'fontsize',12)
%  text(Xlim(2)/2,Ylim(1)+((Ylim(2)-Ylim(1))/8)*3,strcat('percentage of raman','=',num2str(round(p3*1000)*0.1)),'fontsize',12)
 
 
 % split loadings
 v1= L(1:y1,:);
 v2= L(y1+1:y1+y2,1);
 v3= L(y1+y2+1:y1+y2+y3,1);
 
 
 % normalise loadings
 v1= v1/norm(v1);
 v2= v2/norm(v2);
 v3= v3/norm(v3);
 

end



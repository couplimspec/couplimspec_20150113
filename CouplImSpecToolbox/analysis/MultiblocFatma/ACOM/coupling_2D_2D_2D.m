function coupling_2D_2D_2D (varargin) 

 %% description
    % coupling and linking three 2 way datatables using multiple co-inertia
    % analysis
    
%% input

   % no inputs
   % two files: -txt file
   %            -raw file
   %            -dso file

    
%% output
    
    %dso: datasetobject of the datatable reduced

%% principe
   % coupling two datasets having the same number of ways (2 dimensions)

%% use
   % coupling_2D_2D()
   % coupling_2D_2D(file1,file2,file3,dimension)
    
%% Comments
    % writen for F.Allouche Phd Thesis
    
    
%% Author
    % F.Allouche
    
%% date
    % 23/06/2011    

%% context variables 
orig=pwd;           % returns the current directory

existdso=0;


%% input


if nargin==0
 
  % input of files   
  [nom1,rep1]=uigetfile({'*.dso.mat;*.txt;*.raw;'},'name of the infrared datatable','*.dso.mat;*.txt;*.raw;');
  cd(rep1)
  [nom2,rep2]=uigetfile({'*.dso.mat;*.txt;*.raw;'},'name of the fluorescence datatable','*.dso.mat;*.txt;*.raw;');
  cd(rep2)
  [nom3,rep3]=uigetfile({'*.dso.mat;*.txt;*.raw;'},'name of the Raman datatble','*.dso.mat;*.txt;*.raw;');
  cd(rep3)
  
  % input of data table 1
  cd (rep1)
  point=strfind(nom1,'.');
  point=point(length(point));
  ext=nom1((point+1):(length(nom1)));
  if strcmp(ext,'mat')
        
    load(nom1);
    X1=dso;
    lo1=X1.axisscale{2};
    clear dso;
    existdso=existdso+1;
    X1=X1.data;
    

 elseif strcmp(ext,'raw')
        
   [X1,lo1]=lire_raw(nom1);
   
   
 elseif strcmp(ext,'txt')
       
   X1=lire(nom1); %#ok<NASGU>
   lo1=str2num(X1.v);%#ok<NASGU>
   X1=X1.d;

 else
    error('format .dso.mat or .raw or .txt expected')
 end
 

% input of data table 2
  
 cd (rep2)
 point=strfind(nom2,'.');
 point=point(length(point));
 ext=nom2((point+1):(length(nom2)));
 if strcmp(ext,'mat')
        
    load(nom2);
    X2=dso;
    lo2=X2.axisscale{2};
    clear dso;
    existdso=existdso+1;
    
    % compute average datatable if 3 way datatable
    if length(size(X2.data))==3
        X2=mean(X2.data,3);
    else
        X2=X2.data;
    end;

 elseif strcmp(ext,'raw')
        
   [X2,lo2]=lire_raw(nom2);
   
   if length(size(X2))==3
        X2=mean(X2,3);
   end;

 elseif strcmp(ext,'txt')
       
   X2=lire(nom2); %#ok<NASGU>
   lo2=str2num(X2.v);%#ok<NASGU>
   X2=X2.d;

 else
     
     error('format .dso.mat or .raw or .txt attendu')
     
 end

% input of data table 3
  
  cd (rep3)
  point=strfind(nom3,'.');
  point=point(length(point));
  ext=nom3((point+1):(length(nom3)));
  
  if strcmp(ext,'mat')
        
    load(nom3);
    X3=dso;
    lo3=X3.axisscale{2};
    clear dso;
    existdso=existdso+1;
    
    % compute average datatable if 3 way datatable
    if length(size(X3.data))==3
        X3=mean(X3.data,3);
    else
        X3=X3.data;
    end;
    

 elseif strcmp(ext,'raw')
        
   [X3,lo3]=lire_raw(nom3);
   if length(size(X3))==3
        X3=mean(X3,3);
   end;

 elseif strcmp(ext,'txt')
       
   X3=lire(nom3); %#ok<NASGU>
   lo3=str2num(X3.v);%#ok<NASGU>
   X3=X3.d;

 else
    error('format .dso.mat or .raw or .txt attendu')
 end
 

% input number of dimension to be analysed
ok=0;
while ~ok
    prompt = {'Enter the dimension '};
    dlg_title = 'dimension';
    num_lines = 1;

    answer = inputdlg(prompt,dlg_title,num_lines);
    dimension=str2double(cell2mat (answer)); %#ok<ST2NM>
    if strcmp(class(dimension),'double')
        ok=1;
    else 
        ok=0;
    end
end
end

% three input parameters (function input parameters)
if nargin==3
    
    X1=varargin{1};
    X2=varargin{2};
    X3=varargin{3};
    dimension=varargin{4};
    
    if strcmp(class(X1),'dataset')
            
            X1=X1.data;
            
            if length(size(X1.data))==3
                X1=mean(X2.data,3);
            end;
   
    elseif strcmp(class(X1),'double')
            
            if length(size(X1))==3
                X1=mean(X1,3);
            end;
    else
            error('format .dso.mat or .raw or .txt expected')
    end;
    
    
    if strcmp(class(X2),'dataset')
            
            X2=X2.data;
            if length(size(X2.data))==3
                X2=mean(X2.data,3);
            end;
   
    elseif strcmp(class(X2),'double')
            if ength(size(X2.data))==3
                X2=mean(X2,3);
            end;
    else
            error('format .dso.mat or .raw or .txt expected')
    end;
    
    if strcmp(class(X3),'dataset')
            
            X3=X3.data;
            if length(size(X3.data))==3
                X3=mean(X3.data,3);
            end;
   
    elseif strcmp(class(X3),'double')
           
            if length(size(X3.data))==3
                X3=mean(X3,3);
            end;
    else
            error('format .dso.mat or .raw or .txt expected')
    end;
    
end;

if nargin >4
     
    error('use: coupling2D_2D or coupling2D_2D(file1,file2)');
    
end;

%% treatement

%coupling datatables using multiple co-inertia analysis

[U,V1,V2,V3]= McoA (X1,X2,X3,dimension);

% total variance
inertiaX1=trace((1/size(X1,1))*(X1'*X1));
inertiaX2=trace((1/size(X2,1))*(X2'*X2));
inertiaX3=trace((1/size(X3,1))*(X3'*X3));

% computing components
CX1=X1*V1;
CX2=X2*V2;
CX3=X3*V3;

%computing variance for the three datatables
varX1=(1/size(X1,1))*diag(CX1'*CX1)
varX2=(1/size(X2,1))*diag(CX2'*CX2)
varX3=(1/size(X3,1))*diag(CX3'*CX3)

% variance percentage
pvarX1=(1/inertiaX1)*varX1;
pvarX1=fix(100*pvarX1) %#ok<NASGU>

pvarX2=(1/inertiaX2)*varX2;
pvarX2=fix(100*pvarX2)

pvarX3=(1/inertiaX3)*varX3;
pvarX3=fix(100*pvarX3)

% covariance percentage

pcov1= diag(CX1'*U)/size(X1,1);
pcov2= diag(CX2'*U)/size(X2,1);
pcov3= diag(CX3'*U)/size(X3,1);
pcov=pcov1+pcov2+pcov3 

        
%% Save loadings and image scores

if (existdso==3)
  
    
cd(rep1)
load(nom1)
im1=dso;
clear('dso')

for dim=1:dimension

    figure (dim) ;
    plot(str2num(im1.label{2}),V1(:,dim),'LineWidth',3)
    xlabel([ im1.title{2} ' (' im1.userdata.unit, ')'],'fontsize',16)
    if strcmp(im1.userdata.unit,'cm-1')
        set(gca,'xdir','reverse')
    end;

    switch im1.userdata.method
        case 'INFRARED'
            ylabel('infrared absorbance','fontsize',16);
            title(im1.name,'fontsize',16)
            set(gca,'fontsize',14)
            saveas(gcf,strcat(im1.name,'LoadingIR',num2str(dim),'.tif'));
        case 'RAMAN'
            ylabel('Raman intensity','fontsize',16);
            title(im1.name,'fontsize',16)
            set(gca,'fontsize',14)
            saveas(gcf,strcat(im1.name,'LoadingR',num2str(dim),'.tif'));
        case 'FLUORESCENCE'
            ylabel('Fluorescence intensity','fontsize',16);
            title(im1.name,'fontsize',16)
            set(gca,'fontsize',14)
            saveas(gcf,strcat(im1.name,'LoadingF',num2str(dim),'.tif'));
        otherwise
            ylabel('')
   end;

end

cd(rep2)
load(nom2);
im2=dso;
clear ('dso')

for dim=1:dimension

   figure(dim+dimension);
   plot(str2num(im2.label{2}),V2(:,dim),'LineWidth',3)
   xlabel([ im2.title{2} ' (' im2.userdata.unit, ')'],'fontsize',16)

   if strcmp(im2.userdata.unit,'cm-1')
        set(gca,'xdir','reverse')
   end;

    switch im2.userdata.method
        case 'INFRARED'
            ylabel('infrared absorbance','fontsize',16);
            title(im2.name,'fontsize',16)
            set(gca,'fontsize',14)
            saveas(gcf,strcat(im2.name,'LoadingIR',num2str(dim),'.tif'));
        case 'RAMAN'
            ylabel('Raman intensity','fontsize',16);
            title(im2.name,'fontsize',16)
            set(gca,'fontsize',14)
            saveas(gcf,strcat(im2.name,'LoadingR',num2str(dim),'.tif'));
        case 'FLUORESCENCE'
            ylabel('Fluorescence intensity','fontsize',16);
            title(im2.name,'fontsize',16)
            set(gca,'fontsize',14)
            saveas(gcf,strcat(im2.name,'LoadingF',num2str(dim),'.tif'));
        otherwise
            ylabel('')
    end;

end

figure(3);
cd(rep3)
load(nom3);
im3=dso;
clear('dso');

for dim=1:dimension

   figure(dim+dimension*2);
   plot(str2num(im3.label{2}),V3(:,dim),'LineWidth',3)
   xlabel([ im3.title{2} ' (' im3.userdata.unit, ')'],'fontsize',16)
   if strcmp(im3.userdata.unit,'cm-1')
        set(gca,'xdir','reverse')
   end;

     switch im3.userdata.method
        case 'INFRARED'
            ylabel('infrared absorbance','fontsize',16);
            title(im3.name,'fontsize',16)
            set(gca,'fontsize',14)
            saveas(gcf,strcat(im3.name,'LoadingIR',num2str(dim),'.tif'));
        case 'RAMAN'
            ylabel('Raman intensity','fontsize',16);
            title(im3.name,'fontsize',16)
            set(gca,'fontsize',14)
            saveas(gcf,strcat(im3.name,'LoadingR',num2str(dim),'.tif'));
        case 'FLUORESCENCE'
            ylabel('Fluorescence intensity','fontsize',16);
            title(im3.name,'fontsize',16)
            set(gca,'fontsize',14)
            saveas(gcf,strcat(im3.name,'LoadingF',num2str(dim),'.tif'));
        otherwise
            ylabel('')
    end;   
end

    reconstruction_imagedso(im1,CX1,CX2,CX3,dimension,rep1,rep2,rep3);
else
    % when the data format used is not a dso
    figure (1);
    plot(lo1,V1(:,1:dimension))
    figure (2);
    plot(lo2,V2(:,1:dimension))
    figure (3);
    plot(lo3,V3(:,1:dimension))
    reconstruction_image(CX1,CX2,CX3,dimension,rep1,rep2,rep3)
end

    % image scores reconstruction
   
    
%% matlab function tracking  

fid=fopen(strcat(im1.name,'.treat','.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end;

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'Compute loadings and image scores using multiple co-inertia analysis MCoA \r\n');
fprintf(fid,'__________________________________________________________________________\r\n');

fprintf(fid,'\r\ninput file name: %s%s\r\n',nom1,'.dso.mat');
fprintf(fid,'data folder: %s\r\n',rep1);
fprintf(fid,'\r\ninput file name: %s%s\r\n',nom2,'.dso.mat');
fprintf(fid,'data folder: %s\r\n',rep2);
fprintf(fid,'\r\ninput file name: %s%s\r\n',nom3,'.dso.mat');
fprintf(fid,'data folder: %s\r\n',rep3);

for dim=1:dimension  
    
    fprintf(fid,'\r\nsaved file name : %s \r\n',strcat(im1.name,'LoadingR',num2str(dim),'.tif'));
    fprintf(fid,'data folder: %s\r\n',rep1);
    fprintf(fid,'\r\nsaved file name : %s \r\n',strcat(im1.name,'CompIR',num2str(dim),'.tif'));
    fprintf(fid,'data folder: %s\r\n',rep1);
    
end

for dim=1:dimension  
    
    fprintf(fid,'\r\nsaved file name : %s \r\n',strcat(im2.name,'LoadingR',num2str(dim),'.tif'));
    fprintf(fid,'data folder: %s\r\n',rep2);
    fprintf(fid,'\r\nsaved file name : %s \r\n',strcat(im2.name,'CompIR',num2str(dim),'.tif'));
    fprintf(fid,'data folder: %s\r\n',rep2);
    
end

for dim=1:dimension 
    
    fprintf(fid,'\r\nsaved file name : %s \r\n',strcat(im3.name,'LoadingR',num2str(dim),'.tif'));
    fprintf(fid,'data folder: %s\r\n',rep3);
    fprintf(fid,'\r\nsaved file name : %s \r\n',strcat(im3.name,'CompIR',num2str(dim),'.tif'));
    fprintf(fid,'data folder: %s\r\n',rep3);
    
end


% save of function used

fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end

cd (orig)    
end




function reconstruction_imagedso(im1,Imag_IR,Imag_F,Imag_R,dimension,rep1,rep2,rep3)

    
    SizeimX=42 %im1.imagesize(1);
    SizeimY=12 %im1.imagesize(2);
    
   
    % Get back coordinates values of pixels analysed
    coordX=im1.userdata.dX;
    coordY=im1.userdata.dY;
%     
%     % Compute the unique values of coordinates
%     val1=unique(coordX); 
%     val2=unique(coordY);
%     
   
   
    [nomim,repim]=uigetfile({'*.tif'},'name of the region selected','*.tif');
    cd(repim)
    ref=imread(nomim);
    ref(:,:,3)=ref(:,:,1);
    ref(:,:,2)=ref(:,:,1);

   % Computing all score images Infrared Raman and Fluorescence within the
   % number of dimensions selected
   for dim=1:dimension
    
        minIR=min(min(Imag_IR(:,dim)));
        maxIR=max(max(Imag_IR(:,dim)));
        moyIR=(minIR+maxIR)/2;
       
        Im_IR(1:SizeimX,1:SizeimY)=minIR;
   
    
        minF=min(min(Imag_F(:,dim)));
        maxF=max(max(Imag_F(:,dim)));
        moyF= (minF+maxF)/2;
        
        
       Im_F(1:SizeimX,1:SizeimY)=minF;
    
        minR=min(min(Imag_R(:,dim)));
        maxR=max(max(Imag_R(:,dim)));
        moyR=(minR+maxR)/2;
    
       Im_R (1:SizeimX,1:SizeimY)=minR;

       
       for cpt=1:size(im1.data,1)
        
            Im_IR(coordX(cpt,1),coordY(cpt,1))=Imag_IR(cpt,dim);
            Im_F(coordX(cpt,1),coordY(cpt,1))=Imag_F(cpt,dim);
            Im_R(coordX(cpt,1),coordY(cpt,1))=Imag_R(cpt,dim);

       end
       
     % image adjusting values in 0..255
         imIR=ajusterimage(Im_IR,min(min(Imag_IR(:,dim))),max(max(Imag_IR(:,dim))));
         imF=ajusterimage(Im_F,min(min(Imag_F(:,dim))),max(max(Imag_F(:,dim))));
         imR=ajusterimage(Im_R,min(min(Imag_R(:,dim))),max(max(Imag_R(:,dim))));

    % conversion of values in uint8 type for display
        imIR=uint8(imIR);
        imF=uint8(imF);
        imR=uint8(imR);


    % image scores display 

        imIR=imIR(:,:,1);
        imIR=imresize(imIR,[size(ref,1),size(ref,2)],'nearest');
        imIRc=ind2rgb(uint8(imIR),jet(256));
        imIRc=uint8(imIRc*255);
        compoi1=imlincomb(0.5,imIRc,0.5,ref);
        figure
        imshow(compoi1)
        stepIR=(max(max(Imag_IR(:,dim)))-min(min(Imag_IR(:,dim))))/6;
        minIR=min(min(Imag_IR(:,dim)));
        maxIR=max(max(Imag_IR(:,dim)));
        colorbar
        colorbar('YTickLabel',...
        {num2str(minIR),num2str(minIR+stepIR),num2str(minIR+2*stepIR),num2str(minIR+3*stepIR),...
        num2str(minIR+4*stepIR),num2str(minIR+5*stepIR),maxIR})
        cd (rep1)
        saveas(gcf,strcat('CompIR',num2str(dim),'.tif'),'tif')


        imF=imF(:,:,1);
        imF=imresize(imF,[size(ref,1),size(ref,2)],'nearest');
        imFc=ind2rgb(uint8(imF),jet(256));
        imFc=uint8(imFc*255);
        compoi1=imlincomb(0.5,imFc,0.5,ref);
        figure
        imshow(compoi1)
        stepF=(max(max(Imag_F(:,dim)))- min(min(Imag_F(:,dim))))/6;
        minF=min(min(Imag_F(:,dim)));
        maxF=max(max(Imag_F(:,dim)));
        colorbar
        colorbar('YTickLabel',...
        {num2str(minF),num2str(minF+stepF),num2str(minF+2*stepF),num2str(minF+3*stepF),...
        num2str(minF+4*stepF),num2str(minF+5*stepF),maxF})
     
        cd (rep2)
        saveas(gcf,strcat('CompF',num2str(dim),'.tif'),'tif')

        imR=imcomplement(imR(:,:,1));
        imR=imresize(imR,[size(ref,1),size(ref,2)],'nearest');
        imRc=ind2rgb(uint8(imR),jet(256));
        imRc=uint8(imRc*255);
        compoi1=imlincomb(0.5,imRc,0.5,ref);
        figure
        imshow(compoi1)
        stepR=(max(max(Imag_R(:,dim)))- min(min(Imag_R(:,dim))))/6;
        minR=min(min(Imag_R(:,dim)));
        maxR=max(max(Imag_R(:,dim)));
        colorbar
        colorbar('YTickLabel',...
        {num2str(minR),num2str(minR+stepR),num2str(minR+2*stepR),num2str(minR+3*stepR),...
        num2str(minR+4*stepR),num2str(minR+5*stepR),maxR})
      
        cd (rep3)
        saveas(gcf,strcat('CompIR',num2str(dim),'.tif'),'tif')
  
   end

end

function reconstruction_image(Imag_IR,Imag_F,Imag_R,dimension,rep1,rep2,rep3)

    
    coord=lire();
    
    % Get back coordinates values of pixels analysed
    coordX=coord.d(:,1);
    coordY=coord.d(:,2);
    T=size(coord.d);
    % Compute the unique values of coordinates
    val1=unique(coordX); 
    val2=unique(coordY);
    
    % Compute the optimum size of the image score
    X=size(val1,1);
    Y=size(val2,1);
    
 
  


   % Computing all score images Infrared Raman and Fluorescence within the
   % number of dimensions selected
   for dim=1:dimension
    
         % Initialisation of score image at zero
          
        minIR=min(min(Imag_IR(:,dim)));
        maxIR=max(max(Imag_IR(:,dim)));
        moyIR=(minIR+maxIR)/2;
       
        Im_IR(1:SizeimX,1:SizeimY)=minIR;
   
    
        minF=min(min(Imag_F(:,dim)));
        maxF=max(max(Imag_F(:,dim)));
        moyF= (minF+maxF)/2;
        
        
       Im_F(1:SizeimX*5,1:SizeimY*5)=minF;
    
        minR=min(min(Imag_R(:,dim)));
        maxR=max(max(Imag_R(:,dim)));
        moyR=(minR+maxR)/2;
    
       Im_R (1:SizeimX*5,1:SizeimY*5)=minR;

       
       for cpt=1:T(1)
        
            Im_IR(coordX(cpt,1)-min(coordX)+1,coordY(cpt,1)-min(coordY)+1)=Imag_IR(cpt,dim);
            Im_F(coordX(cpt,1)-min(coordX)+1,coordY(cpt,1)-min(coordY)+1)=Imag_F(cpt,dim);
            Im_R(coordX(cpt,1)-min(coordX)+1,coordY(cpt,1)-min(coordY)+1)=Imag_R(cpt,dim);

       end
       
     % image adjusting values in 0..255
     imIR=ajusterimage(Im_IR,min(min(Imag_IR(:,dim))),max(max(Imag_IR(:,dim))));
     imF=ajusterimage(Im_F,min(min(Imag_F(:,dim))),max(max(Imag_F(:,dim))));
     imR=ajusterimage(Im_R,min(min(Imag_R(:,dim))),max(max(Imag_R(:,dim))));
    
    % conversion of values in uint8 type for display
     imIR=uint8(imIR);
     imF=uint8(imF);
     imR=uint8(imR);
     
     
  
    % image scores display 

[nomim,repim]=uigetfile({'*.tif'},'name of the region selected','*.tif');
cd(repim)
ref=imread(nomim);
ref(:,:,3)=ref(:,:,1);
ref(:,:,2)=ref(:,:,1);

imIR=imIR(:,:,1);
imIR=imresize(imIR,[210,60],'nearest');
imIRc=ind2rgb(uint8(imIR),jet(256));
imIRc=uint8(imIRc*255);
compoi1=imlincomb(0.5,imIRc,0.5,ref);
imshow(compoi1)
colorbar
cd (rep1)
saveas(gcf,strcat('CompIR',num2str(dim),'.tif'),'tif')


imF=imF(:,:,1);
imF=imresize(imF,[210,60],'nearest');
imFc=ind2rgb(uint8(imF),jet(256));
imFc=uint8(imFc*255);
compoi1=imlincomb(0.5,imFc,0.5,ref);
imshow(compoi1)
colorbar
cd (rep2)
saveas(gcf,strcat('CompIR',num2str(dim),'.tif'),'tif')

imR=imR(:,:,1);
imR=imresize(imR,[210,60],'nearest');
imRc=ind2rgb(uint8(imR),jet(256));
imRc=uint8(imRc*255);
compoi1=imlincomb(0.5,imRc,0.5,ref);
imshow(compoi1)
colorbar
cd (rep3)
saveas(gcf,strcat('CompIR',num2str(dim),'.tif'),'tif')

  
   end

end


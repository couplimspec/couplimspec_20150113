function [U,V,W,CX,CY,CZ,alpha0,Beta0,T,TYY,TZZ]=ACOM3D(X,Y,Z,maxdim,tol)


%% Initialisation
     
   
     
   [x1,y1]=size(X);
   [x2,y2,z2]=size(Y);  
   [x3,y3,z3]=size(Z) ;
   
   %Initialisation   
    for k=1:100
        alpha0=rand(z2,1);
        Beta0=rand(z3,1);
    end
    
    alpha0= alpha0/norm(alpha0);% initialisation alpha0
    Beta0= Beta0/norm(Beta0);%initialisation beta0;
    
    % Calcul des tableaux 
    Ybar=zeros(x2,y2);
    Zbar=zeros(x3,y3);
    
    dim=1;
    diff=1000;
    iter=0;
    fn=zeros(1000,1)

 %% Treatment
 while (dim<maxdim)  
  while(diff>tol)
      
    iter=iter+1;
    for K=1:z2
        
        a=alpha0(K,1)*Y(:,:,K);
        Ybar=Ybar+a;
        
        b=Beta0(K,1)*Z(:,:,K);
        Zbar=Zbar+b;
    end
   
    Ybar=Ybar*1/z2;
    Zbar=Zbar*1/z3;
    
    

   [T0,U0,V0,W0]=MCoA(X,Ybar,Zbar,1)
   
   TY=Y(:,:,1)*V0;
   TZ=Z(:,:,1)*W0;
   
   for K=2:z2
    a=Y(:,:,K)*V0;
    TY=cat(2,TY,a);
    b=Z(:,:,K)*W0;
    TZ=cat(2,TZ,b);
   end
   
   alpha1= TY'*T0/norm(TY'*T0);
   Beta1=TZ'*T0/norm(TZ'*T0);
   
   
   alpha0=alpha1;
   Beta0=Beta1;
   
   
    for K=1:z2
        
        a=alpha0(K,1)*Y(:,:,K);
        Ybar=Ybar+a;
        
        b=Beta0(K,1)*Z(:,:,K);
        Zbar=Zbar+b;
    end
   
    Ybar=Ybar*1/z2;
    Zbar=Zbar*1/z3;
%% Treatment     
    

   [T1,U1,V1,W1]=MCoA(X,Ybar,Zbar,1)

   diff=norm(U1-U0)+ norm(V1-V0)+norm(W1-W0);
   fn(iter,:)=diff;
  end
  
  
  plot(fn)
  alpha(:,dim)=alpha0;
  Beta(:,dim)=Beta0;
  U(:,dim)=U1;
  V(:,dim)=V1;
  W(:,dim)=W1;
  T(:,dim)=T1;
  CX(:,dim)=X*U1;
  CY(:,dim)=Ybar*V1;
  CZ(:,dim)=Zbar*W1;
  TYY(:,:,dim)=TY;
  TZZ(:,:,dim)=TZ;
  dim=dim+1;
  end
end
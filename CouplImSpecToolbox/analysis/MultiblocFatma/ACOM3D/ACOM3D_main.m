function [U,V,W,L,CX,CY,CZ,alpha,Beta,T,TYY,TZZ,fn]=ACOM3D_main(X,Y,Z,tol,maxdim)
    

    dim=1;
    
    while(dim<=maxdim)
      
      [Ybar,Zbar,U1,V1,W1,L1,alpha1,Beta1,T1,TY,TZ,fn]=ACOM3D_1(X,Y,Z,tol);
      
      alpha(:,dim)=alpha1;
      Beta(:,dim)=Beta1;
      U(:,dim)=U1;
      V(:,dim)=V1;
      W(:,dim)=W1;
      L(:,dim)=L1;
      T(:,dim)=T1;
      CX(:,dim)=X*U1;
      CY(:,dim)=Ybar*V1;
      CZ(:,dim)=Zbar*W1;
      TYY(:,:,dim)=TY;
      TZZ(:,:,dim)=TZ;
     
      
      [X,Y,Z]=Deflation_ACOM3D(X,Y,Z,U1,V1,W1);
      
      dim=dim+1;
   
    end



end


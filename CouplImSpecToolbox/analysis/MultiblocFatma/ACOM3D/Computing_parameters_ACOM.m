
function Computing_parameters_ACOM(X1,X2,X3,CX1,CX2,CX3,C,L, dimension)

    
 
 
 % contribution au profils spectral global
    for dim=1:dimension
     uktilde=0;
     for k=1:size(X1,2)
         uktilde=uktilde+L(k,dim)^2;
     end
     uktildes(dim,:)=uktilde;
    end
    uktildes=uktildes.*100;
    
    uktildes
    
    clear uktilde
    clear uktildes
    
    
    L1=L(size(X1,2):end,:);

    for dim=1:dimension
        uktilde=0;
        for k=1:size(X2,2)
            uktilde=uktilde+L1(k,dim)^2;
        end
         uktildes(dim,:)=uktilde;
    end
    
    uktildes=uktildes.*100;
    
    uktildes
    
    clear uktilde
    clear uktildes
    
    L2=L1(size(X2,2):end,:);
     
    for dim=1:dimension
        uktilde=0;
        for k=1:size(X3,2)
            uktilde=uktilde+L2(k,dim)^2;
        end
         uktildes(dim,:)=uktilde;
    end
    
    uktildes=uktildes.*100;
    
    uktildes
    
    
%   X2bar=zeros(size(X2,1),size(X2,2));
%   X3bar=zeros(size(X3,1),size(X3,2));
%         for k=1:size(X2,3)   
%             a=alpha(k,1)*X2(:,:,k);
%             X2bar=X2bar+a;
%         end
%         
%         for k=1:size(X3,3)
%            b=Beta(k,1)*X3(:,:,k);
%            X3bar=X3bar+b;
%         end
%     
 
% for dim=1:dimension
%     CX2pond(:,dim)= CX2(:,:,dim)*alpha(:,dim);
%     CX3pond(:,dim)= CX3(:,:,dim)*Beta(:,dim);
% end   
%     
    varcx1=diag(CX1'*CX1);
    varcx2=diag(CX2'*CX2);  
    varcx3=diag(CX3'*CX3);  
    
    traceX1=trace(X1'*X1);
    traceX2=trace(X2*X2');
    traceX3=trace(X3*X3');
   
    pvarcx1=(varcx1./traceX1).*100
    pvarcx2=(varcx2./traceX2).*100
    pvarcx3=(varcx3./traceX3).*100
  
   for dim=1:dimension
       
       covar2cx1(dim)=sum(C(:,dim).*CX1(:,dim))^2; 
       covar2cx2(dim)=sum(C(:,dim).*CX2(:,dim))^2;
       covar2cx3(dim)=sum(C(:,dim).*CX3(:,dim))^2;
   end
   
   covar2cx1;
   covar2cx2;
   covar2cx3;
   
  for dim=1:dimension 
  communx1(dim)=covar2cx1(dim)/(varcx1(dim)+varcx2(dim)+varcx3(dim));
  communx2(dim)=covar2cx2(dim)/(varcx1(dim)+varcx2(dim)+varcx3(dim));
  communx3(dim)=covar2cx3(dim)/(varcx1(dim)+varcx2(dim)+varcx3(dim));
  commun=covar2cx1(dim)/(varcx1(dim)+varcx2(dim)+varcx3(dim))+covar2cx2(dim)/(varcx1(dim)+varcx2(dim)+varcx3(dim))+covar2cx3(dim)/(varcx1(dim)+varcx2(dim)+varcx3(dim))
  end

   communx1=communx1.*100
   communx2=communx2.*100
   communx3=communx3.*100
   
    
   for dim=1:dimension 
    corrx1=corrcoef ([CX1(:,dim) C(:,dim)])
    corrx2=corrcoef ([CX2(:,dim) C(:,dim)])
    corrx3=corrcoef ([CX3(:,dim) C(:,dim)])
   end
        
   
end

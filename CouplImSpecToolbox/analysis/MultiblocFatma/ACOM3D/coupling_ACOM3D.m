function coupling_ACOM3D 
%% description
    % coupling and linking three  datatables (1 two-way and 2 three-way)
    % using  the extention of multiple co-inertia analysis ACOM3D
    
%% input

   % no inputs
   % three files: dso file

%% output
    
    %dso: datasetobject of the datatable reduced

%% principe
   % coupling three datasets having the same number of ways (2 dimensions)

%% use
   % coupling_ACOM3D()
    
%% Comments
    % writen for F.Allouche Phd Thesis
    
    
%% Author
    % F.Allouche
    
%% date
    % 06/04/2012    

%% context variables 
          % returns the current directory

close all
clear all
orig=pwd; 

%% input


if nargin==0
 
  % input of files   
  
  [nom1,rep1]=uigetfile({'*.dso.mat;'},'name of the infrared datatable','*.dso.mat;');
  
  [nom2,rep2]=uigetfile({'*.dso.mat;'},'name of the fluorescence datatable','*.dso.mat;');
  
  [nom3,rep3]=uigetfile({'*.dso.mat;'},'name of the Raman datatble','*.dso.mat;');

else
    
    error('format .dso.mat or .raw or .txt expected')
end

  % input of datatable1
  cd (rep1)
  point=strfind(nom1,'.');
  point=point(length(point));
  ext=nom1((point+1):(length(nom1)));
  if strcmp(ext,'mat')        
    load(nom1);
    im1=dso;
    clear dso;
    X1=im1.data;   
 else
    error('format .dso.mat expected')
  end
  
  % input of datatable 2
 cd (rep2)
 point=strfind(nom2,'.');
 point=point(length(point));
 ext=nom2((point+1):(length(nom2)));
 
  if strcmp(ext,'mat')
        
    load(nom2);
    im2=dso;
    clear dso;
    X2=im2.data;  
  else
    error('format .dso.mat expected')
  end
  
  
  % input of datatable 3
  cd (rep3)
  point=strfind(nom3,'.');
  point=point(length(point));
  ext=nom3((point+1):(length(nom3)));
  
  if strcmp(ext,'mat')
        
    load(nom3);
    im3=dso;
    clear dso;
    X3=im3.data;  
 else
    error('format .dso.mat expected')
  end
  
  tol=0.001;
  tol=0.000001;
  
  % input number of dimension to be analysed
ok=0;
while ~ok
    prompt = {'Enter the dimension '};
    dlg_title = 'dimension';
    num_lines = 1;

    answer = inputdlg(prompt,dlg_title,num_lines);
    maxdim=str2double(cell2mat (answer)); %#ok<ST2NM>
    if strcmp(class(maxdim),'double')
        ok=1;
    else 
        ok=0;
    end
end

%==============================================================================option0
%  X1c=(X1-ones(size(X1,1),1)*mean(X1)); 
%  for k=1:size(X2,3)
%     X2c(:,:,k)=(X2(:,:,k)-ones(size(X2(:,:,k),1),1)*mean(mean(X2,3)));
%     X3c(:,:,k)=X3(:,:,k)-ones(size(X3(:,:,k),1),1)*mean(mean(X3,3));
%  end

% ==========================================================================option 1  
% X1c=(X1-ones(size(X1,1),1)*mean(X1));
% X1c=X1c./trace(X1c*X1c');
%  
% for k=1:size(X2,3)
%     X2c(:,:,k)=(X2(:,:,k)-ones(size(X2(:,:,k),1),1)*mean(mean(X2,3)))
%     X2c(:,:,k)=X2c(:,:,k)./trace(X2c(:,:,k)*X2c(:,:,k)');
%     X3c(:,:,k)=X3(:,:,k)-ones(size(X3(:,:,k),1),1)*mean(mean(X3,3))/trace(X2c(:,:,k)*X2c(:,:,k)');
%     X3c(:,:,k)=X3c(:,:,k)./trace(X3c(:,:,k)*X3c(:,:,k)');
% end

% ========================================================================option 2

%   X1c=(X1-ones(size(X1,1),1)*mean(X1));
%   [U,S,V]=svd(X1c);
%   lamda1=S(1,1);
%  
%  
%  for k=2:size(X2,3)
%       X2c(:,:,k)=(X2(:,:,k)-ones(size(X2(:,:,k),1),1)*mean(mean(X2,3)));
%       X2=cat(1,X2c(:,:,k),X2cc);
%       X3c(:,:,k)=X3(:,:,k)-ones(size(X3(:,:,k),1),1)*mean(mean(X3,3));
%      X3(:,:,k)=cat(1,X3c(:,:,k),X3cc);
%  end
%   
%     tmp=permute(X2c,[1 3 2]);
%     X2cc=reshape(tmp,size(X2c,1)*size(X2c,3),size(X2c,2));  
%     
%     tmp=permute(X3c,[1 3 2]);
%     X3cc=reshape(tmp,size(X3c,1)*size(X3c,3),size(X3c,2));
%   
%   [U,S,V]=svd(X2cc);
%   lamda2=S(1,1);
%   
%   [U,S,V]=svd(X3cc);
%   lamda3=S(1,1);
%   
%   X1c=X1c./sqrt(lamda1);
%   X2c=X2c./sqrt(lamda2);
%   X3c=X3c./sqrt(lamda3);
% % %  
 %=============================================================================== option 3
X1c=(X1-ones(size(X1,1),1)*mean(X1));
X1c=X1c./sqrt(trace(X1c*X1c'));
traceX2c=0;
traceX3c=0;
for k=1:size(X2,3)
    X2c(:,:,k)=(X2(:,:,k)-ones(size(X2(:,:,k),1),1)*mean(mean(X2,3)));
    traceX2c= traceX2c+trace(X2c(:,:,k)'*X2c(:,:,k));
    X3c(:,:,k)=X3(:,:,k)-ones(size(X3(:,:,k),1),1)*mean(mean(X3,3));
    traceX3c= traceX3c+trace(X3c(:,:,k)'*X3c(:,:,k));
end 


X2c=X2c./sqrt(traceX2c);
X3c=X3c./sqrt(traceX3c);
%  
 %==========================================================================option 4
%  X1c=(X1-ones(size(X1,1),1)*mean(X1));
%  [U,S,V]=svd(X1c);
%  lamda1=S(1,1);
%  X1c=X1c./sqrt(lamda1);
%  
%  for k=1:size(X2,3)
%     X2c(:,:,k)=(X2(:,:,k)-ones(size(X2(:,:,k),1),1)*mean(mean(X2,3)));
%     [U,S,V]=svd(X2c(:,:,k));
%     lamda2=S(1,1);
%     X2c(:,:,k)=X2c(:,:,k)./sqrt(lamda2);
%     
%     X3c(:,:,k)=X3(:,:,k)-ones(size(X3(:,:,k),1),1)*mean(mean(X3,3));
%     [U,S,V]=svd(X3c(:,:,k));
%     lamda3=S(1,1);
%     X3c(:,:,k)=X3c(:,:,k)./sqrt(lamda3);
%  end
 
 %% Treatment
  [U,V,W,L,CX,CY,CZ,alpha,Beta,T,TYY,TZZ,fn]=ACOM3D_main(X1c,X2c,X3c,tol,maxdim);
  
  %% save results as dso file
 
     for k=1:maxdim
         figure
         plot (alpha)
         figure
         plot(Beta)
         alpha_im=reshape(alpha(:,k),5,5)
         beta_im=reshape (Beta(:,k),5,5);
         ajusterimage(alpha_im,min(alpha(:,k)), max(alpha(:,k)));
         ajusterimage(alpha_im,min(alpha(:,k)), max(alpha(:,k)));
         alpha_im=imresize(alpha_im,20,'nearest');
         beta_im=imresize(beta_im,20,'nearest');
         imwrite(alpha_im,strcat('alpha_im',num2str(k),'.tif'),'tif','compression', 'none');
         imwrite( beta_im,strcat('beta_im',num2str(k),'.tif'),'tif','compression', 'none');
     end
  
  % folder number 1 
    cd(rep1)
    createdso(U,'loading',im1);
    createdso(CX,'score',im1);
    createdso(T,'common.score',im1);
    createdso(L,'global.loading',im1);
    
    % folder number 2 
    cd(rep2)
    createdso(V,'loading',im2);
    CY3=permute(TYY,[1 3 2]);
    createdso(CY3,'score',im2);
    createdso(L,'global.loading',im1);
    createdso(alpha,'imagette',im2);
  
    % folder number 3 
    
    cd(rep3)
    createdso(W,'loading',im3);
    CZ3=permute(TZZ,[1 3 2]);
    createdso(CZ3,'score',im3);
    createdso(Beta,'imagette',im3);
  
  
  %% reconstruction 
  
    voisinage=5;

  
  %% Display Results
  
  % Reference image
  [nomim,repim]=uigetfile({'*.tif'},'name of the region selected','*.tif');
  cd(repim)
  ref=imread(nomim);
%   ref(:,:,3)=ref(:,:,1);
%   ref(:,:,2)=ref(:,:,1);

  % Reference image : region selected
  cd(rep1)
  SizeimX=str2num(im1.description(8,end-2:end));
  SizeimY=str2num(im1.description(9,end-2:end));
  
  % Display results
  Afficher_loading(im1,U,maxdim)
  %Display_image_score(im1,SizeimX,SizeimY,ref,voisinage,CX,maxdim);
  Display_image_score_gris(im1,SizeimX,SizeimY,ref,voisinage,CX,maxdim);
  
  cd(rep2)
  Afficher_loading(im2,V,maxdim);
  CY3=permute(TYY,[1 3 2]);
  %Display_image_score(im2,SizeimX,SizeimY,ref,voisinage,CY3,maxdim);
  Display_image_score_gris(im2,SizeimX,SizeimY,ref,voisinage,CY3,maxdim);
  
  cd(rep3)
  Afficher_loading(im3,W,maxdim);
  CZ3=permute(TZZ,[1 3 2]);
  %Display_image_score(im3,SizeimX,SizeimY,ref,voisinage,CZ3,maxdim);
  Display_image_score_gris(im3,SizeimX,SizeimY,ref,voisinage,CZ3,maxdim);
  
  
  Computing_parameters(X1c,CX,TYY,TZZ,T,L,maxdim)
  L1=L(size(X1c,2):end,:);
  Computing_parameters(X2c,TYY,CX,TYY,T,L1,maxdim)
  L2=L1(size(X2c,2):end,:);
  Computing_parameters(X3c,TZZ,CX,TZZ,T,L2,maxdim)

  Computing_parameters1(X1c,X2c,X3c,CX,TYY,TZZ,T,L,alpha,Beta,maxdim)
  Display_loading(im1,U,maxdim)
  
  %Display_image_score(im1,SizeimX,SizeimY,ref,voisinage,T,maxdim);
  
  
  
  
  
  
  
  
  
  
  
  
end
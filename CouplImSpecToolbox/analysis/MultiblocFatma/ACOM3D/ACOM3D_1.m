function [Ybar,Zbar,U1,V1,W1,L,alpha1,Beta1,T1,TY,TZ,fn]=ACOM3D_1(X,Y,Z,tol)


%% Initialisation       
   [nx,py]=size(X);
   [ny,py,qy]=size(Y);  
   [nz,pz,qz]=size(Z) ;

   %test sur les dimensions nx=ny=nz??  
   
   %Initialisation   
   % entre -1 et 1

        alpha0=rand(qy,1);
        Beta0=rand(qz,1);
        
        alpha0= alpha0/norm_vector(alpha0);% initialisation alpha0
        Beta0= Beta0/norm_vector(Beta0);%initialisation beta0;
    
       %Calcul de U0,VO,ect... 
        % Calcul des tableaux 
        
        diff=1000;
        fn=zeros(1000,1);
        iter=0;
        %% Treatment  
    tol=0.001;
    
    while(diff>tol)
        iter=iter+1;
        
        Ybar=zeros(ny,py);
        Zbar=zeros(nz,pz);
        for k=1:qy   
            a=alpha0(k,1)*Y(:,:,k);
            Ybar=Ybar+a;
        end
        
        for k=1:qz
           b=Beta0(k,1)*Z(:,:,k);
           Zbar=Zbar+b;
        end
    
    [T0,U0,V0,W0,L]=MCoA(X,Ybar,Zbar,1);
  
   %=========================================================
  
   TY=Y(:,:,1)*V0;
   
   for k=2:qy
    a=Y(:,:,k)*V0;
    TY=cat(2,TY,a);
   end
   
   alpha1= TY'*T0/norm_vector(TY'*T0);
   TZ=Z(:,:,1)*W0;
  
   for  k=2:qz
    b=Z(:,:,k)*W0;
    TZ=cat(2,TZ,b);
   end
   
   Beta1=TZ'*T0/norm_vector(TZ'*T0);
  % f(iter,1)=(T0'*X*U0)^2 +(T0'*TY*alpha0)^2+(T0'*TZ*Beta0)^2
  
   Ybar=zeros(ny,py);
   Zbar=zeros(nz,pz);
   for k=1:qy   
            a=alpha1(k,1)*Y(:,:,k);
            Ybar=Ybar+a;
   end
   
   for k=1:qz
        b=Beta1(k,1)*Z(:,:,k);
        Zbar=Zbar+b;
    end
   [T1,U1,V1,W1]=MCoA(X,Ybar,Zbar,1);
   
   TY=Y(:,:,1)*V1;
   
   for k=2:qy
    a=Y(:,:,k)*V1;
    TY=cat(2,TY,a);
   end
   
   TZ=Z(:,:,1)*W1;
  
   for  k=2:qz
    b=Z(:,:,k)*W1;
    TZ=cat(2,TZ,b);
   end
 %  f(iter+1,1)=(T1'*X*U1)^2 +(T1'*TY*alpha1)^2+(T1'*TZ*Beta1)^2;
   %diff=norm(U1-U0)+ norm(V1-V0)+norm(W1-W0)+norm(alpha1-alpha0)+ norm(Beta1-Beta0);
   diff=norm(alpha1-alpha0)+ norm(Beta1-Beta0);
   diff
   %diff=abs(f(iter,1)-f(iter+1,1))
  % diff=f(iter+1,1)-f(iter,1)
   U0=U1;
   V0=V1;
   W0=W1;
   alpha0=alpha1;
   Beta0=Beta1;
    
    end
  
 % fn=f(1:iter+1,:);
 % plot(fn)
 
end



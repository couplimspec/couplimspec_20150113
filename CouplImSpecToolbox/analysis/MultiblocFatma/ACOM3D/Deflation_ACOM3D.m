 
function [X,Y,Z]=Deflation_ACOM3D(X,Y,Z,U1,V1,W1)

    % taille cube fluo
    [~,~,ky]=size(Y);
    
    
    % taille cube fluo
    [~,~,kz]=size(Z);
    
    % taille tableau ibnfrarouge
    
    [nx,px]=size(X);


    % deflation tableau infrarouge
    X=X-X*U1*U1';

   % deflation tableau fluorescence
    for k=1:ky
        Y(:,:,k)=Y(:,:,k)-Y(:,:,k)*V1*V1';
    end
    
    % deflation taleau Raman 
    for k=1:kz
        Z(:,:,k)=Z(:,:,k)-Z(:,:,k)*W1*W1';
    end
    
end

function [U,V1,V2,V3]= Extended_McoA (X1,X2,X3,dimension)
  %% description
    
    % coupling and linking three 2 way datatables using multiple co-inertia
    % analysis
    
%% input

   
   % Three datatables  X1,X2,X3        
   % dimensions to be analysed after decomposition
    
%% output
    
    % U global loadings
    % V1 loading of the first datatable
    % V2 loading of the second datatable
    % V3 loading of the third datatable

%% principe
   
    % coupling two datasets having the same number of ways (2 dimensions)

%% use
  
    % [U,V1,V2,V3]= McoA (X1,X2,X3,dimension)
  
    
%% Comments
    
    % writen for F.Allouche Phd Thesis
    
    
%% Author
   
    % F.Allouche
    
%% date
    % 23/06/2011    

%% input variables
     
     [x1,y1]=size(X1);
     [x2,y2,z2]=size(X2);
     [x3,y3,z3]=size(X3);

%% Treatment     
% initialisation

    dim=1;
   

% iteration
    while (dim <= dimension)  
       
        X1=X1-ones(size(X1,1),1)*mean(X1);
        for k=1:z2
            X2(:,:,k)=X2(:,:,k)-ones(size(X2(:,:,k),1),1)*mean(X2(:,:,k));  
            X3(:,:,k)=X3(:,:,k)-ones(size(X3(:,:,k),1),1)*mean(X3(:,:,k));  
        end
        
        [v1,v2,v3,u]=compute_MCoA_component(X1,X2,X3); 
        

        U(:,dim)=u;
        V1(:,dim)=v1;
        V2(:,dim)=v2;
        V3(:,dim)=v3; 

        % deflation MCOA: CPCA with orthogonal block loadings : bon
        % compromis
%          X1=X1-X1*v1*v1';
%        
%         for k=1:z2
%             X2(:,:,k)=X2(:,:,k)-X2(:,:,k)*v2*v2';
%             X3(:,:,k)=X3(:,:,k)-X3(:,:,k)*v3*v3';
%         end
% 
%        
       
% % deflation  CPCA with orthogonal global scores  ==> redondance des
% profils spectraux ???

%         X1=X1-u*u'*X1;
%        
%         for k=1:z2
%             X2(:,:,k)=X2(:,:,k)-u*u'*X2(:,:,k);
%             X3(:,:,k)=X3(:,:,k)-u*u'*X3(:,:,k);
%         end


%         X1=X1-u*u'*X1;
%         X2=X2-u*u'*X2;
%         X3=X3-u*u'*X3;

      




% % deflation  CPCA with orthogonal block scores   : qualit�c des composantes images 
        X1=X1-((X1*v1)*(X1*v1)')/((X1*v1)*(X1*v1)')*X1;


        for k=1:z2
            CX2=X2(:,:,k)*v2;
            CX3=X3(:,:,k)*v3;
            X2(:,:,k)=X2(:,:,k)-((CX2*CX2')/(CX2'*CX2))*X2(:,:,k);
            X3(:,:,k)=X3(:,:,k)-((CX3*CX3')/(CX3'*CX3))*X3(:,:,k);
        end





        dim=dim+1;
        
    end
    
           
end

function [v10,v20,v30,u]=compute_MCoA_component(X1,X2,X3)

% compute first component
    [x1,y1]=size(X1);
    [x2,y2,z2]=size(X2);
    [x3,y3,z3]=size(X3);
    
    v10=rand(y1,1);
    v20=rand(y2,1);
    v30=rand(y3,1);
    
    v10= v10/norm(v10);% initialisation u0
    v20= v20/norm(v20);%initialisation v0
    v30= v30/norm(v30);
    
    fn=zeros(1000,1);
    iter=0;
    tol=0.00001;
    diff=1;
    
    while (diff>tol)
        
        iter=iter+1;
        X=X1*v10;
       
        for i=1:z2
            X=cat(2,X2(:,:,i)*v20);
        end
        
         for i=1:z2
            X=cat(2,X3(:,:,i)*v30);
        end
        
        
        [U,S,V]=svds(X);
        u=U(:,1);
       
        v1=(X1'*u)/norm(X1'*u);
        v2=X2(:,:,1)'*u/norm(X2(:,:,1)'*u);
        v3=X3(:,:,1)'*u/norm(X3(:,:,1)'*u);
        
        for i=2:z2
            v2= cat (2,v2,X2(:,:,i)'*u/norm(X2(:,:,i)'*u));
            v3= cat (2,v3,X3(:,:,i)'*u/norm(X3(:,:,i)'*u));
        end
        
        [V2,S2,U2]=svds(v2);
        [V3,S3,U3]=svds(v3);
        
        
        diff=norm(v10-v1)+ norm(v20-V2(:,1))+norm(v30-V3(:,1));
        v10=v1;
        v20=V2(:,1);
        v30=V3(:,1);
    end

 
 % normalise loadings
 v10= v10/norm(v1);
 v20= v20/norm(v2);
 v30= v30/norm(v3);
 

end



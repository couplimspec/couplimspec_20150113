  function code=findCode(vecChar)

 %% description
    % find code in a vector of char define by a position
    % similar to unique for numbers
    
%% input

    %  vecChar : vector of character array
    
%% output
    
    %   code : char array of the code
%% principe
    % check the unique strings in the vector of char and return tham as
    % codes


%% use
    % d=lire;
    % vecChar=d.i(2:3);
    % code=findCode(vecChar)
    
    
%% Comments
    % 
    
    
%% Author
    %  MF Devaux
    % INRA Nantes
    % BIA-PVPP
    
%% date
    % 13 aout 2013


%% start

if nargin >1
    error('use: code=findCode(vecChar)');
end


%% treatement
nb=length(vecChar);

code=vecChar(1,:);
j=1;

for i=2:nb
    if ~strcmp(vecChar(i,:),code(j,:))
        j=j+1;
        code=char(code,vecChar(i,:));
    end;
end


%% matlab function tracking  

% no fucntion tracking
%% end


    
  function catdso(rfolder,sname,sfolder,flagNom)

 %% description
    % concatenate all the dso files of a folder into a single dso 
    % dso is a dataset object from eigenvector research see
    % http://www.eigenvector.com/software/dataset.htm
    
%% input
    % nothing
    %or
    %  rfolder : inout folder
    %  sname : name of resultlting dso file
    % sfolder : save folder
    
    
%% output
    
    % 

%% principe
    % all the dso files are concatenated usinf the cat dataset command.
    % it is checked that variables are the sames
    % 

%% use
    % catdso
    % or
    % catdso(rfolder,sname,sfolder);
    
    
%% Comments
    % written for F Allouche PhD thesis and macrofluo images
    
    
%% Author
    % MF Devaux
    % BIA PVPP - INRA  Nantes
    
%% date
    % 9 aout 20111
    % 8 octobre 2013 : cumul eds donnees userdata de type coordonnees
    % 5 novrembre 2013: ajout d'un code pour les labels individus = nom du
    % fichier d'origine

%% context variables 
orig=pwd;           % returns the current directory

%% input
if nargin >3
    error('use: catdso(rfolder,sname,sfolder)');
end

if nargin >0;
    if ~exist(rfolder,'dir')
        error('no directory found of name %s',rfolder)
    end;    
end;

if nargin ==0
    % input image
    [~,rfolder]=uigetfile({'*.dso.mat'},'name of the first spectral image','*.dso.mat');
end;

if nargin <=1
    % file.dso.mat and track file will be saved in the same folder than the
    % original  file
    cd(rfolder)
    cd ..
    sfolder=pwd;
    indice=strfind(rfolder,'\');
    sname=rfolder((indice(length(indice)-1)+1):(indice(length(indice))-1));
    sname=strcat(sname,'.dso.mat');
end


if nargin ==0
    % ouput file
    [sname,sfolder]=uiputfile({'*.dso.mat'},'name of the resulting file',sname);    
end;

if nargin ==2
    sfolder=rfolder;
end;
    

if nargin ==3
    if ~exist(sfolder,'dir')
        error('no directory found of name %s',sfolder)
    end; 
end;


     

%% treatement
% liste of dso file into the folder
cd(rfolder)
liste=dir('*.dso.mat');

if isempty(liste)
    error('pas de fichiers dso.mat dans le dossier %s',rfolder);
end;

% liste des fichiers
for i=1:length(liste)
    disp(liste(i).name);
    dso=load(liste(i).name);
    ims=getfield(dso,char(fieldnames(dso)));
    nbp=size(ims.data,1);
    nbclass= size(ims.classid,2);
    ims.classid{1,nbclass+1}=repmat(strrep(ims.name,'.dso.mat',''),nbp,1);
    clear dso;
    
    if i==1
        tot=ims;
        refx=ims.axisscale{2};
        userdata=ims.userdata;
        if isfield(userdata,'visim')
            userdata=rmfield(userdata,'visim');
        end;
        if isfield(userdata,'dX')
            userdata=rmfield(userdata,'dX');
        end;
        if isfield(userdata,'dY')
            userdata=rmfield(userdata,'dY');
        end;
        if isfield(userdata,'coord')
            coord{1}=userdata.coord;
            flCoord=1;
        else
            flCoord=0;
        end;


    else
        
        % test des variables
        x=ims.axisscale{2};
        if sum(x-refx)~=0
            error('les variables du fichier %s diff�rent de celles du fichier %s',liste(i).name,liste(1).name);
        end;
        
        tot=[tot;ims];
        
                
        if flCoord && isfield(ims.userdata,'coord')
            coord{i}=ims.userdata.coord;
        else
            flCoord=0;
        end;
        

    end;
end;

tot.name=sname;
tot.title{1}='';
tot.userdata=userdata;
tot.userdata.listevcat=liste.name;
tot.userdata.listevcatp=rfolder;
if flCoord
    tot.userdata.coord=coord;
end
       


%% save 
cd(sfolder)
if ~strfind(sname,'.dso.mat')
    sname=strcat(sname,'.dso.mat');
end;
save(sname,'tot');

%% matlab function tracking  

cd(sfolder)
tname=strcat(strrep(sname,'.dso.mat','.track.txt'));
fid=fopen(tname,'w');

if fid==0
    errordlg('enable to open track file');
end;

fprintf(fid,'\r\n%s\r\n',datestr(now,0));
fprintf(fid,' Concatenation de fichiers dso.mat dans un seul fichiers : sens individus \r\n');
fprintf(fid,'__________________________________________________________________________\r\n\r\n');

fprintf(fid,'\r\n');
% processed files
fprintf(fid,'Input files folder: %s\r\n',rfolder);

% save file
fprintf(fid,'saved file : %s\r\n',sname);
% ouput folder
fprintf(fid,'Output folder: %s\r\n',sfolder);

fprintf(fid,'\r\n');
% save function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
repprog=fileparts(info);
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);


%% end

cd (orig)
    
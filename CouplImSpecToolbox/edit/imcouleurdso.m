function varargout = imcouleurdso(varargin)
% IMCOULEURDSO M-file for imcouleurdso.fig
%      IMCOULEURDSO, by itself, creates a new IMCOULEURDSO or raises the existing
%      singleton*.
%
%      H = IMCOULEURDSO returns the handle to a new IMCOULEURDSO or the handle to
%      the existing singleton*.
%
%      IMCOULEURDSO('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in IMCOULEURDSO.M with the given input arguments.
%
%      IMCOULEURDSO('Property','Value',...) creates a new IMCOULEURDSO or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before imcouleur_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to imcouleurdso_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help imcouleurdso

% Last Modified by GUIDE v2.5 12-Aug-2011 14:00:51

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @imcouleurdso_OpeningFcn, ...
                   'gui_OutputFcn',  @imcouleurdso_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before imcouleurdso is made visible.
function imcouleurdso_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to imcouleurdso (see VARARGIN)

% Choose default command line output for imcouleurdso
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes imcouleurdso wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = imcouleurdso_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in imc_load_dso.
function imc_load_dso_Callback(hObject, eventdata, handles)
% hObject    handle to imc_load_dso (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% input image
[nom,repl]=uigetfile({'*.dso.mat'},'name of spectral image','*.dso.mat');

cd(repl)
dso=load(nom);
ims=getfield(dso,char(fieldnames(dso)));
clear dso;

% affichage des images
im=reshape(ims.imagedata,ims.imagesize(1),ims.imagesize(2),1,size(ims.data,2));
axes(handles.imc_im_montage)
montage(im);
clear im;

if strcmp(class(ims.data),'uint16')
    imc=uint16(zeros([ims.imagesize 3]));
else
    imc=uint8(zeros([ims.imagesize 3]));
end;
imorig=zeros(3,1);

% mise � jour de la liste des images
h=findobj('Tag','imcouldso_sel');
affiche=strcat(num2str((1:size(ims.data,2))'),' : ',ims.label{2});
set(h,'string',affiche);

set(handles.figure1, 'UserData', {imc,ims,imorig,nom,repl});


% --- Executes on button press in imcouldso_list_ok.
function imcouldso_list_ok_Callback(hObject, eventdata, handles) %#ok<DEFNU>
% hObject    handle to imcouldso_list_ok (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

data=get(handles.figure1,'UserData');
imc=data{1};
ims=data{2};
imorig=data{3};

h=findobj('Tag','imc_dest');
hdest1=get(h,'SelectedObject');
ncd=imc_dest(hdest1);


h=findobj('Tag','imcouldso_sel');
ncs=get(h,'Value');


imc(:,:,ncd)=ims.imagedata(:,:,ncs);
imorig(ncd)=ncs;

figure(27)
imshow(imc,[]);

data{1}=imc;
data{3}=imorig;
set(handles.figure1, 'UserData', data);




% --- Executes on button press in imc_save_imc.
function imc_save_imc_Callback(hObject, eventdata, handles)
% hObject    handle to imc_save_imc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

data=get(handles.figure1,'UserData');
imc=data{1};
ims=data{2};
imorig=data{3};
nom=data{4};
repl=data{5};


[noms,reps]=uiputfile({'*.tif'},'sauver image couleur',strcat(ims.name,'-x.tif'));

cd(reps)
noms=strcat(strrep(noms,'.tif',''),'.tif');
imwrite(imc,noms,'tif','compression','none')



% remplissage d'un fichier trace pour indiquer les t�ches effectu�es
% actualisation du fichier trace.txt
fic=fopen(strcat(strrep(noms,'.tif',''),'.trace.txt'),'w');
if fic==0
    errordlg('probl�me d''�criture du fichier trace')
end;

fprintf(fic,'\r\n\r\n%s\r\n',datestr(now,0));
fprintf(fic,'----------------------------------------------------------------\r\n');
fprintf(fic,'-\t CREATION D''IMAGEs COULEUR A PARTIR D''IMAGES MULTISPECTRALES DSO\r\n');
fprintf(fic,'----------------------------------------------------------------\r\n');

fprintf(fic,'\r\nImages de d�part : %s\r\n',nom);
fprintf(fic,'\r\nR�pertoire de lecture : %s\r\n',repl);

fprintf(fic,'\r\n');
fprintf(fic,'\r\nImages s�lectionn�es : \r\n');
for i=1:3
    switch i
        case 1
            fprintf(fic,'\t- Canal rouge : ');
        case 2
            fprintf(fic,'\t- Canal vert  : ');
        case 3
            fprintf(fic,'\t- Canal bleu  : ');
    end;
    fprintf(fic,'%s \r\n ',ims.label{2}(imorig(i),:));
    
end

fprintf(fic,'\n- r�pertoire de sauvegarde des r�sultats : %s\r\n',reps);
fprintf(fic,'\nimage sauvegard�e : %s\r\n',noms);

fprintf(fic,'\r\n');

% save function used
fprintf(fic,'\r\n------------------------------------------------------------------------------------------------\r\n');
info=which (mfilename);
repprog=fileparts(info);
fprintf(fic,'function name: %s \r\n',mfilename);
res=dir(info);
fprintf(fic,'on %s \r\n',res.date);
fprintf(fic,'function folder: %s \r\n',repprog);
fprintf(fic,'------------------------------------------------------------------------------------------------\r\n\r\n\r\n\r\n');

fclose(fic);




% --- Executes when selected object is changed in imc_dest
function canal=imc_dest(hObject)
% hObject    handle to the selected object in imc_dest1 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

tag=get(hObject,'Tag');

switch tag(1:(length(tag)-1))     % Get Tag of selected object
    case 'imc_dr'
        canal=1;
    case 'imc_dv'
        canal=2;
    case 'imc_db'
        canal=3;
end



% --- Executes on selection change in imcouldso_sel.
function imcouldso_sel_Callback(hObject, eventdata, handles)
% hObject    handle to imcouldso_sel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns imcouldso_sel contents as cell array
%        contents{get(hObject,'Value')} returns selected item from imcouldso_sel


% --- Executes during object creation, after setting all properties.
function imcouldso_sel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to imcouldso_sel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes on button press in imcoul_col.
function imcoul_col_Callback(hObject, eventdata, handles)
% hObject    handle to imcoul_col (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data=get(handles.figure1,'UserData');
ims=data{2};
imorig=data{3};
repl=data{5};


cd(repl)
liste=dir('*.dso.mat');
nbim=length(liste);

if ~exist('imcoul','dir')
    mkdir('imcoul')
end;
cd('imcoul')
trace=dir('c*.imcoul.track.txt');
if isempty(trace)
    num=1;
else
    nb=length(trace);
    %trace=strrep(trace.name,'.imcoul.track.txt','');
    %trace=strrep(trace,'c','');
    %num=str2double(trace)+1;
    num=nb+1;
end;

for i=1:nbim
    cd(repl)
    % lecture de l'image
    disp(liste(i).name);
    dso=load(liste(i).name);
    ims=getfield(dso,char(fieldnames(dso)));
    clear dso;
    
    % creation de l'image couleur
    for j=1:3
        imc(:,:,j)=ims.imagedata(:,:,imorig(j));
    end;
    
    % sauvegarde
    cd('imcoul')
    noms=strcat('c',num2str(num),'.',strrep(liste(i).name,'.dso.mat',''),'.tif');
    imwrite(imc,noms,'tif','compression','none')
end;



% remplissage d'un fichier trace pour indiquer les t�ches effectu�es
% actualisation du fichier trace.txt
fic=fopen(strcat('c',num2str(num),'.imcoul.track.txt'),'w');
if fic==0
    errordlg('probl�me d''�criture du fichier trace')
end;

fprintf(fic,'\r\n\r\n%s\r\n',datestr(now,0));
fprintf(fic,'-------------------------------------------------------------------\r\n');
fprintf(fic,'- CREATION D''IMAGES COULEUR A PARTIR D''IMAGES MULTISPECTRALES DSO -\r\n');
fprintf(fic,'-------------------------------------------------------------------\r\n');

fprintf(fic,'\r\nR�pertoire de lecture des images de d�part : %s\r\n',repl);
fprintf(fic,'\r\n\r\nToutes les images du r�pertoire sont trait�es\r\n');

fprintf(fic,'\r\n');
fprintf(fic,'\r\nImages s�lectionn�es : \r\n');
for i=1:3
    switch i
        case 1
            fprintf(fic,'\t- Canal rouge : ');
        case 2
            fprintf(fic,'\t- Canal vert  : ');
        case 3
            fprintf(fic,'\t- Canal bleu  : ');
    end;
    fprintf(fic,'%s \r\n ',ims.label{2}(imorig(i),:));
    
end

fprintf(fic,'\r\n- r�pertoire de sauvegarde des r�sultats : %s\r\n',strcat(repl,'imcoul'));

fprintf(fic,'\r\n');

% save function used
fprintf(fic,'\r\n------------------------------------------------------------------------------------------------\r\n');
info=which (mfilename);
repprog=fileparts(info);
fprintf(fic,'function name: %s \r\n',mfilename);
res=dir(info);
fprintf(fic,'on %s \r\n',res.date);
fprintf(fic,'function folder: %s \r\n',repprog);
fprintf(fic,'------------------------------------------------------------------------------------------------\r\n\r\n\r\n\r\n');

fclose(fic);



    

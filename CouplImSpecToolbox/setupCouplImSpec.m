function setupCouplImSpec(varargin)
%SETUPCOUPLIMSPEC  Setup files for couplImSpec ToolBox
%
%   Usage:
%       setupCouplImSpec;
%
%   Example
%   setupCouplImSpec
%
%   See also
%
 
% ------
% Author: CouplImSpec development team
% Created: 2015-01-13,    using Matlab 8.4.0.150421 (R2014b)
% Copyright 2015 INRA - Cepia Software Platform.

% extract library path
fileName = mfilename('fullpath');
libDir = fileparts(fileName);

moduleNames = {...
    'analysis', ...
    'basic', ...
    'dso', ...
    'edit', ...
    'exempledata', ...
    'help', ...
    'import', ...
    'plots', ...
    'preprocess', ...
    'processCol', ...
    'register', ...
    'template'};

disp('Installing CouplImSpec Toolbox Library');
addpath(libDir);

% add all library modules
for i = 1:length(moduleNames)
    name = moduleNames{i};
    fprintf('Adding module: %-20s', name);
    addpath(fullfile(libDir, name));
    disp(' (ok)');
end

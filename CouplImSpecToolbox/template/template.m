  function im=noir_et_blanc(imc)
%

 %% description
    % conversion d'une image couleur rgb en image noir et blanc
    
%% input

    %  imc : image couleur 
    
%% output
    
    %   im : image monochrome de meem type que l'image d'entree

%% principe
    % les images sont converties en monochrome selon 3 possibilites
        % choix d'un des trois canaux RGB
        % utilisation de la fonction rgb2gray de matlab : rgb2gray converts RGB values to grayscale values by forming a weighted sum of the R, G, and B components:

                    % 0.2989 * R + 0.5870 * G + 0.1140 * B 

        % calcul de l'image en niveaux de gris a partir de la formule
                    % 0.3333 * R + 0.3333 * G + 0.3333 * B 


%% use
    % [dso]= readspc('filename.spc'); return dataset "dso" of spc map.
    % dso= readomnic;
    % dso is a dataset object from eigenvector research see http://www.eigenvector.com/software/dataset.htm
    
%% Comments
    % 
    
    
%% Author
    % 
    
%% date
    % 

%% context variables 
orig=pwd;           % returns the current directory

%% start

if nargin >1
    error('use: [dso_read]= readspc(filename)');
end


%% input


%% treatement


%% save 


%% matlab function tracking  

fid=fopen(strcat(name,'.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end;

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'Import data set object from LAbspec *.spc map file \r\n');
fprintf(fid,'__________________________________________________________________________\r\n');

% fprintf(fid,'\r\ninput file name: %s%s\r\n',name,ext);
% fprintf(fid,'data folder: %s\r\n',pathname);
% 
% fprintf(fid,'\r\nsaved file name : %s \r\n',sname);
% fprintf(fid,'data folder: %s\r\n',sfolder);

% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end

cd (orig)
    
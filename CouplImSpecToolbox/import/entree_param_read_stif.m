  function [fnamegen,rfolder,sfolder,code,codebf,method,codevar]=entree_param_read_stif
  %% description
    % centr�e des param�tres de la focntion read_stif
    
%% input

    %  nothing
    
%% output
    
    % fnamegen : generic name of images 
    % rfolder : read folder for tiff images
    % sfolder : save folder for dso.mat file
    % code : series of extension to be added to generic name to read the file
    % codebf : code of brightfield image, if there is none : codebf must be ''
    % method : m�thode d'acquisition des images
    % codevar : code des variables spectrales

%% principe
    % entree interactive des param�tres pour les fonctions read_stif et
    % process_col (TODO)


%% use
    % [fnamegen,rfolder,sfolder,code,codebf,method,codevar]=entree_param_read_stif;
    % dso= read_stif(fnamegen,rfolder,sfolder,code,codebf,method,codevar);

    % dso is a dataset object from eigenvector research see http://www.eigenvector.com/software/dataset.htm
    
%% Comments
    % 
    
    
%% Author
    % MF Devaux
    % BIA-PVPP
    
%% date
    % 28 f�vrier 2013

%% context variables 
orig=pwd;           % returns the current directory

%% start

if nargin >0
    error('use: [fnamegen,rfolder,sfolder,code,codebf,method,codevar]=entree_param_read_stif;');
end


%% input
    % open file from folder 
    [nom,rfolder]=uigetfile({'*.tif'},'first image of the multispectral image','*.tif');
    cd(rfolder)
    % d�termination du nom g�n�rique
    ln=length(nom);
    for i=1:min(10, ln-6);
        listenom{i}=nom(1:(ln-(i+3)));
    end;
    listenom{i+1}='autre';
    [selection ]=listdlg('liststring',listenom,'selectionmode','single','initialvalue',3,'name','nom g�n�rique','promptstring','nom g�n�rique');
    fnamegen=char(listenom(selection));
    if strcmp(fnamegen,'autre')
        fnamegen=char(inputdlg('entrer le nom g�n�rique','nom g�n�rique',1,{nom(1:(ln-4))}));
    end;
    ext=nom((ln-2):ln);
    
    
    %nom de la m�thode d'acquisition
    listemethod={'CONFOCAL','MACROFLUO','autre'};
    [selection ]=listdlg('liststring',listemethod,'selectionmode','single','initialvalue',1,'name','m�thode d''acquisition des images','promptstring','nom g�n�rique');
    method=char(listemethod(selection));
   
    % D�finition des variables spectrales 
    switch method
        case {'CONFOCAL'}
            [nomlo,pathlo]=uigetfile({'*.txt'},'file of spectral variable','*vspecmcbl*.txt');
            cd(pathlo)
            codevar=lire(nomlo);
            %lo=lire(nomlo);
            %codevar=[num2str(lo.d(:,1)) repmat('-',size(lo.d,1),1) num2str(lo.d(:,2))];
            %clear lo;
            
        case {'MACROFLUO'}
            [nomlo,pathlo]=uigetfile({'*.txt'},'file of spectral variable','*vspecmcfluo*.txt');
            cd(pathlo)
            codevar=lire_vec_char(nomlo);
        otherwise
            codevar='';
    end;
        
        
    % selection of code and brightfield image
    liste=dir(strcat(fnamegen,'*.',ext));
    for i=1:length(liste)
        codei=strrep(liste(i).name,fnamegen,'');
        codei=strrep(codei,strcat('.',ext),'');
        code{i}=codei;
    end;
    
    if length(code)>1
        % image brighfield
        [selection ]=listdlg('liststring',['aucune',code],'selectionmode','multiple','name','image brightfield','promptstring','code de l''image brightfield');
        if selection==1
            codebf='';
        else
            codebf=code{selection-1};
            code=cellstr(char(code{[1:(selection-2) selection:end]}));
        end;

        % les images de la s�rie
        if length(code)>1
            [selection ]=listdlg('liststring',code,'selectionmode','multiple','initialvalue',1:length(code),'name','code des images � lire','promptstring','s�lectionner les codes');
            code=code(selection);
        end
    else
        codebf='';
    end;
      

    % name and folder to save results
    sname=strcat(fnamegen,'.dso.mat');
    [~,sfolder] = uiputfile({'*.mat'},'result file name',sname);
    


%% matlab function tracking  
% no tracking for this fucntion

%% end

cd (orig)
    
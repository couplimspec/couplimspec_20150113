  function [dso]=read_stif(fnamegen,rfolder,sfolder,code,codebf,method,codevar)

 %% description
    % Read series of tif images acquired for the same sample to form a multispectral or multivariate dso structure
    
    
%% input

    % nothing 
    % or
    % fnamegen : generic name of images 
    % rfolder : read folder for tiff images
    % sfolder : save folder for dso.mat file
    % code : series of extension to be added to generic name to read the file
    % codebf : code of brightfield image, if there is none : codebf must be ''
    % method : m�thode d'acquisition des images
    % codevar : code des variables spectrales

    
%% output
    
    % dso: datasetobject of multivariate image 

%% principe
    % all the file with the generic name are considered
    % those for which the extension is in code are put in the multivariate
    % image
    % if a brightfield image exist, she is added to the dso file

%% use
    % code={'_B',_G','_UV'};
    % codebf='FC;
    % fnamegen='FF'
    % rfolder = '.'
    % sfolder='imported'
    % method='macrofluo'
    % codevar={'UBR','UBV','UBB','UAR','UAV','UAB'}
    % [dso]=read_sim(fnamegen,sfolder,code,codebf,method,codevar)
    % dso is a dataset object from eigenvector research see http://www.eigenvector.com/software/dataset.htm
    
%% Comments
    % writen ro read images acquired with a fluorescence macroscope (C.
    % Barron) histochem project
    % modified to take into account any multi page tiff image
    
    
%% Author
    % Mf Devaux 
    % INRA Nantes BIA - PVPP
    
%% date
    % 08/08/2011  
    % 11/05/2012
    % 29/01/2013 : liste des noms + variables spectrales
    % 28/02/2013 : prise en compte des variables du macrofluo + entree des
    % param�tres
    % 13/06/2013 : mise dans l'ordre des longueurs d'onde des images du
    % macrofluo
    % correction d'un bug pour le confocal sur le nom des variables
    % 19/08/2013 : recherche du code dans le nom des variables confocale
    %               en remplacement de la comparaison stricte + recherche
    %               du nom de la longueur d'onde d'excitation
    
%% context and global variables 
orig=pwd;           % returns the current directory

% code des blocs du macrofluo
codemacro={'UA','UB','BL','VE'};

% correspondance des images confocales avec les longueurs d'onde
% d'excitation
codeconfocal={375,'UV'; 488,'BL';561,'VE'};

%% start

if nargin ~=7 && nargin ~=0 
    error('use: [dso_read]= read_stif(fnamegen,rfolder,sfolder,code,codebf,method,codevar) or [dso_read]= read_stif');
end

if nargin ==0;
    [fnamegen,rfolder,sfolder,code,codebf,method,codevar]=entree_param_read_stif;
end;

if nargin >0;
    liste=dir(strcat(fnamegen,'*.tif'));
    if isempty(liste)
        error('no file of generic name %s in folder %s',fnamegen,orig)
    end;
end;

ext='tif';

%% load brightfield 
cd(rfolder)
if ~isempty(codebf)
    nombfim=strcat(fnamegen,codebf,'.',ext);
    bfim=imread(nombfim);
    if size(bfim,3)==3
        bfim=rgb2gray(bfim);
    end;
end;
    

    
%% load files to workspace
for i=1:length(code)
    nom=strcat(fnamegen,code{i},'.',ext);
    disp(nom);
    info=imfinfo(nom);
    nb=length(info);
    
    % nombre d'images dans la s�quence
    if nb==1
        if strcmp(info.PhotometricInterpretation,'RGB')
            nim=3;
        else
            nim=1;
        end;
    else
        nim=nb;
    end;
    
    % lecture de l'image
    if nb==1
        im=imread(nom);
        x=size(im,1);
        y=size(im,2);
    else
        for j=1:nim
            disp(j);
            tmp=imread(nom,j);
            if j==1
                im=tmp;
                x=size(im,1);
                y=size(im,2);
            else
                im(:,:,j)=tmp;
            end;
        end
           
    end
                
        
    % variable de l'image en cours de lecture
    if nim==1
        vsim=code(i);
    end
    if nim==3
        vsim=strcat(code(i),['R';'G';'B']);
    else
        if nim>1
            switch method
                case {'MACROFLUO'}
                    vsim=codevar;
                case {'CONFOCAL'}
                    vsim=[];
                    for j=1:size(codeconfocal,1);
                        if ~isempty(strfind(lower(code{i}),lower(codeconfocal{j,2}))) |  ~isempty(strfind(lower(code{i}),num2str(codeconfocal{j,1})));
                            lo(i)=codeconfocal{j,1};
                        end
                    end
                    if length(lo)~=i
                        error('code confocal %s non identifi�',code{i});
                    end
                    nind(i)=size(im,3);
                    if i==1
                        ddi(i)=1;
                        ffi(i)=sum(codevar.d(:,1)==lo(i));
                    else
                        ddi(i)=ffi(i-1)+1;
                        ffi(i)=ffi(i-1)+sum(codevar.d(:,1)==lo(i));
                    end
                otherwise
                    if nim<10 
                        vsim=strcat(code{i},num2str((1:nim)'));
                    else
                        vsim=strcat(code{i},'0',num2str((1:9)'));
                        vsim=[vsim;strcat(code{i},num2str((10:nim)'))];
                    end;
            end;
         end;
    end;
    
    if strcmp(method,'MACROFLUO')
        % renversement des mesures pour mettre les images dans le sens
        % des longueurs d'onde : sont consid�r�s : UA, UB, BL et VE
        % on consid�re �galement que les images couleurs sont enregistr�es dans le sens BGR et non pas
        % RGB
        
        % les deux premi�res lettres du code variables sont test�es : 
        vs=vsim(:,1:2);
        
        % comparaison avec les codes macrofluo
        
        for k=1:size(vs,1)
            trouve=0;
            j=1;
            while ~strcmp(vs(k,:),codemacro{j}) && j<=4;
                j=j+1;
            end;
            if strcmp(vs(k,:),codemacro{j})
               pos(k)=j;
               trouve=1;
            end;
  
            if ~trouve
                error('code %s non connu pour le macroFluo',vs(i,:));
            end;
        end
        
        tmp=im;
        vtmp=vsim;
        id=1;
        for k=1:4
            ind=(pos==k);
            tmp(:,:,id:(id+2))=im(:,:,ind);
            vtmp(id:(id+2),:)=vsim(ind,:);
            id=id+3;
        end;
        im=tmp;
        vsim=vtmp;
    end;
    
    if i==1
        data=reshape(im,x*y,nim);
        vs=vsim;
        liste=nom;
        x1=x;
        y1=y;
    else
        if x~=x1 || y~=y1
            error('les tailles des images diff�rent')
        end
        vs=[vs;vsim];
        data=[data , reshape(im,x*y,nim)];
        liste=char(liste,nom);
    end;
    clear vsim
    
end;


% tests de coh�rence
if strcmp(method,'CONFOCAL')
    if size(data,2)~=size(codevar.d,1)
        error('le nombre d''image CONFOCALE %d  diff�re du nombre attendu : %d',size(data,2),size(codevar,1));
    else
        tmp=data;
        [nlo,ordre]=sort(lo);
        for i=1:length(nlo)
                       
            if i==1
                di=1;
                fi=nind(ordre(i));
                vs=[num2str(codevar.d(di:fi,1)) repmat('-',nind(ordre(i)),1) num2str(codevar.d(di:fi,2))];
            else
                di=fi+1;
                fi=fi+nind(ordre(i));
                vs=[vs;num2str(codevar.d(di:fi,1)) repmat('-',nind(ordre(i)),1) num2str(codevar.d(di:fi,2))];
            end
            data(:,di:fi)=tmp(:,ddi(ordre(i)):ffi(ordre(i)));
        end
    
    end
end;

%% dso: data set object
dso=dataset;
dso.name=fnamegen;
dso.author='MFD';
dso.data=data;
dso.axisscale{1}=1:x*y;
dso.title{1}='pixel';
dso.title{2}='images';
dso.type='image';
dso.imagemode=1;
dso.imagesize=[x y];
dso.label{2}=vs;
nbim=size(dso.label{2},1);    
dso.axisscale{2}=1:nbim;

dso.userdata.unit='';           % unit of wavenumber
dso.userdata.method=method;         %  image acquisition method
dso.userdata.liste=liste;           % liste of file included in the multivariate image
dso.userdata.path=rfolder;   % pathname of the reading directory
if ~isempty(codebf)
    dso.userdata.brightfieldimage=bfim; % brightfield image
end
 
%% plot imagedata sum 
 
h=figure(27);
set(h,'windowstyle','docked');
area=sum(dso.imagedata,3);
 
% works with %the PLS - toolbox
%imagesc(dso.imageaxisscale{2,1},dso.imageaxisscale{1,1},area); 
 
imagesc(area);
colormap(jet)
title(fnamegen,'Fontsize',12);
set(gca,'dataAspectRatio',[1 1 1])
 

%% save dso
disp('sauvegarde....')
cd(sfolder);
sname=strcat(fnamegen,'.dso.mat');
save(sname,'dso');
 
%% save sum of wavenumbers as a grey level image
 
% adjust values to 0-255 grey levels
mini=min(area(:));
maxi=max(area(:));
 
a=255/(maxi-mini);
b=-a*mini;
 
imaff=uint8(area*a+b);
 
imwrite(imaff,strcat(fnamegen,'.sum.tif'),'tif','compression','none');



    

%% matlab function tracking  

fid=fopen(strcat(fnamegen,'.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end;

fprintf(fid,'\r\n%s\r\n',datestr(now,0));
fprintf(fid,'Build a dso structure of multivariate image from a set of tif files \r\n');
fprintf(fid,'__________________________________________________________________\r\n');

fprintf(fid,'\r\ninput generic file name: %s%s\r\n',fnamegen,ext);
fprintf(fid,'data folder: %s\r\n',rfolder);

fprintf(fid,'\r\nsaved file name : %s \r\n',sname);
fprintf(fid,'data folder: %s\r\n',sfolder);

fprintf(fid,'\r\nfiles included in the multivariate image\r\n');
for i=1:length(code)
    fprintf(fid,'\t- %s\r\n',liste(i,:));
end;
fprintf(fid,'\r\n');

fprintf(fid,'\r\nname of images in the multivariate image\r\n');
for i=1:size(vs,1)
    fprintf(fid,'\t- %s\r\n',vs(i,:));
end;
fprintf(fid,'\r\n');

if ~isempty(codebf)
    fprintf(fid,'brightfield image : %s\r\n',nombfim);
    fprintf(fid,'\r\n');
else
    fprintf(fid,'no brightfield image\r\n');
    fprintf(fid,'\r\n');
end;

fprintf(fid,'\r\nmethode d''acquisition des images : %s\r\n',method);
fprintf(fid,'\r\n');

% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);


%% end

cd (orig)
    
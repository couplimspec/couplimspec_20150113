function [out,xaxis,auditlog,description]=spcreadr(filename,subs,wlrange,options);
%SPCREADR Reads Galactic SPC files.
%  INPUT:
%   filename = a text string with the name of a SPC file or
%              a cell of strings of SPC filenames.
%     If (filename) is omitted or an empty cell {}, the user will be
%     prompted to select a folder and then one or more SPC files in the
%     identified folder. If (filename) is a blank string, the user will be
%     prompted to select a file. 
%
% OPTIONAL INPUTS:
%      subs = a scalar or vector indicating the sub-files to read
%             {default = all subfiles}.
%   wlrange = a two element vector of the wavelength range to return
%             (endpoints are  inclusive)  {default = whole spectrum}.
%   options = structure array with the following fields:
%          waitbar : [ 'off' | 'on' |{'auto'}] governs the display of a
%                     waitbar when loading multiple files. If 'auto',
%                     waitbar is displayed for larger sets of files only.
%     axismatching : [ 'none' | 'intersect' |{'interpolate'} ] defines
%                     action taken when the x-axes of two spectra being 
%                     read do not match. The options are:
%                     'intersect' returns only the points where the 
%                       spectral x-axis values overlap excatly.
%                     'interpolate' returns the overlapping portions with 
%                       linear interpolation to match spectral points 
%                       exactly. As no extrapolation will be done, the 
%                       returned spectra will cover the smallest common 
%                       spectral range. 
%                     'none' ignores x-axis differences as long as the 
%                       number of data points is the same in all spectra.
%    textauditlog : [{'no'}| 'yes'] governs output of audit log contents.
%                   When 'yes', the auditlog is returned as a raw text
%                   array. Otherwise, the auditlog is returned as a
%                   structure with field names taken from  auditlog keys.
%
%  Outputs can be in two forms.
%  1) With a single output, the data is returned as a DataSet object.
%  2) With more than one output, the outputs are a matrix of intensities
%    (data), the corresponding wavelength vector (wl), the text audit
%    log from the file (auditlog), and the description of the file from the
%    comment field (description).
%
%I/O: x = spcreadr(filename,subs,wlrange,options);             %dataset output
%I/O: [data,xaxis,auditlog,description] = spcreadr(filename,subs,wlrange,options); %data array output
%
%See also: AREADR, XCLGETDATA, XCLPUTDATA, XCLREADR

%Copyright Eigenvector Research, Inc. 2002-2010
%Licensee shall not re-compile, translate or convert "M-files" contained
% in PLS_Toolbox for use with any software other than MATLAB�, without
% written permission from Eigenvector Research, Inc.
%
% 8/21/00 JMS now reads auditlog too
% 2/03 JMS added copyright and recompile notes
% 2/03 JMS removed all references to spcload
% 5/03 JMS changed # of points to be double word
%  -fixed audit-log reading error (added readkeyedline)
% 9/8/03 JMS ignore nsubs=0 (assume at least one subfile is present)
% 9/22/03 JMS added multiple-file read
% 9/29/03 JMS adjusted handling of scaling=0 and scaling>128
% 10/13/03 JMS -added ability to read unequally spaced SPC files
%   -test for different x-axis for each subfile (not currently supported)
%   -revised handling of read on a Mac to use big-endian open instead of manual reveral
% 10/14/03 JMS 
%   -fixed bug in unequally spaced SPC file read code
%   -added interpretation of axis types
% 11/6/03 JMS
%   -allow input of string arrays of filenames
% 1/9/04 JMS
%   -blank input gives load dialog
% 3/2/04 jms
%   -cancel from load dialog returns empty (used to give error w/outputs)
%   -added special "multiple file" selection mode (numeric input)
% 3/15/04 jms
%   -fixed MAC byte ordering (fopen should use "le" not "be")
% 3/19/04 jms -added R14 support of multi-file uigetfile
%   -changed "select multiple files from current folder" to be triggered by
%     an empty cell instead of "0"
%   -no outputs returns dataset (not double)
% 4/19/04 jms -added test for dataset objects not defined
%   -multi-file auditlogs stored in cell array
%   -fixed bug with multi-file output as raw data
%   -added test for PLS_Toolbox not loaded
% 5/6/04 jms -added selection of folder to load multiple
%   -fixed auditlog concatination with multi-file load
% 11/18/04 jms -use file exponent if subfile exponent not set
%   -moved cell parsing into subroutine
%   -fixed exponent handling (again)
% 12/10/04 jms -disabled R14 "special code" for multifiles (has limitation
%    on number of files selectable due to bug in Matlab)
% 1/5/04 jms -allow multi-file read with raw variables as output
%    -fixed auditlog creation with multiple file read and raw var output
% 2/11/04 jms -read in z-axis time stamps
%    -assign name and author fields in DSO
% 7/26/05 jms -added interpolate option when x-axis doesn't match
%    -added options input
%    -added waitbar
% 9/25/06 nbg modified help
%% modified  by F Allouche and MF Devaux
%               BIA -PVPP INRA Nantes
%               3 mars 2011
% remove access to function addsourceinfo (PLS toolbox ?)

usedatasets   = exist('dataset','file');
%useplstoolbox = exist('evriio','file');
useplstoolbox =0;
%define default options
defaultoptions = [];
defaultoptions.axismatching = 'interpolate';  % 'none' | 'intersect' | 'interpolate'
defaultoptions.textauditlog = 'no';           % 'no' | 'yes'
defaultoptions.waitbar = 'auto';

if nargin == 0 | (isa(filename,'char') & isempty(filename))
  filename = {};  %trigger parsecell below (it will offer select file dialog)
else
  if useplstoolbox & isstr(filename) & ~exist(filename,'file') & ismember(filename,evriio([],'validtopics'));
    if nargout==0; clear out; evriio(mfilename,filename,defaultoptions); else; out = evriio(mfilename,filename,defaultoptions); end
    return; 
  end
end

%find and reconcile options
if nargin>1 & isstruct(subs)
  options = subs;
  subs = [];
elseif nargin>2 & isstruct(wlrange)
  options = wlrange;
  wlrange = [];
elseif nargin<4
  options = [];
end
if ~useplstoolbox  %Options settings for non-PLS_Toolbox version
  if isempty(options);
    options = defaultoptions;
  end
else
  options = reconopts(options,'spcreadr');
end

%Handle cell arrays first - recursively call ourselves with each file name
if isa(filename,'char') & size(filename,1)>1;  %more than one row char array? Do as cell
  filename = mat2cell(filename,ones(size(filename,1),1),size(filename,2));
end
if isa(filename,'struct')
  if isfield(filename,'name');  %structure with "name" field?
    filename = {filename.name};
  else
    error('A structure containing filenames must have the names stored in field "name"');
  end
end

%Cell is list of filenames (or empty cell implies load multiple from current folder)
if iscell(filename);
  options.norecon = true;  %set flag saying we don't have to reconcile options (already done)
  switch nargin
    case 2
      otherargs = {subs [] options};
    case 3
      otherargs = {subs wlrange options};
    case 4
      otherargs = {subs wlrange options};
    otherwise
      otherargs = {[] [] options};
  end
  if nargout>1;
    usedatasets = 0;  %even if we HAVE DSOs, don't use them if the user asked for multiple outputs
  end
  [out,xaxis,auditlog] = parsecell(filename,otherargs,usedatasets,options);
  return
end

%=============================================
%Read a file (we have a text file name here)

if nargin<2;
  subs    = [];
end
if nargin<3;
  wlrange = [];
end
textauditlog = strcmp(options.textauditlog,'yes');

if isempty(findstr('.spc',lower(filename))); filename=[filename '.spc']; end;

ismac=~isempty(findstr(computer,'MAC'));
if ismac
  [f,MESSAGE]=fopen(filename,'r','ieee-le');
else
  [f,MESSAGE]=fopen(filename);
end  

if f==-1;
  disp([MESSAGE]);
  disp(['Current folder: ' pwd]);
  error(['File ''' filename ''' does not exist or some other similar error (check directory)']);
end;

try
  ftflgs = double(fread(f,1,'uint8'));    %   BYTE   ftflgs; /* Flag bits defined below */
  fversn = double(fread(f,1,'uint8'));    %   BYTE   fversn; /* 4Bh=> new LSB 1st, 4Ch=> new MSB 1st, 4Dh=> old format */
  
  %handle ftflgs
  % 1 #define TSPREC	1	/* Single precision (16 bit) Y data if set. */
  % 2 #define TCGRAM	2	/* Enables fexper in older software (CGM if fexper=0) */
  % 3 #define TMULTI	4	/* Multiple traces format (set if more than one subfile) */
  % 4 #define TRANDM	8	/* If TMULTI and TRANDM=1 then arbitrary time (Z) values */
  % 5 #define TORDRD	16	/* If TMULTI abd TORDRD=1 then ordered but uneven subtimes */
  % 6 #define TALABS	32	/* Set if should use fcatxt axis labels, not fxtype etc.  */
  % 7 #define TXYXYS	64	/* If TXVALS and multifile, then each subfile has own X's */
  % 8 #define TXVALS	128	/* Floating X value array preceeds Y's  (New format only) */
  ftflgs_bin = mod(floor(ftflgs./2.^[0:7]),2)==1;   %convert ftflgs to binary digits
  
  if fversn==75 | fversn==76
    fexper = fread(f,1,'uint8');    %   BYTE   fexper; /* Reserved for internal use (experiment-1) */
    fexp   = fread(f,1,'schar');    %   char   fexp;   /* Fraction scaling exponent integer (80h=>float) */
    fnpts  = fread(f,1,'uint32');   %   DWORD  fnpts;  /* Integer number of points (or TXYXYS directory position) */
    ffirst = fread(f,1,'float64');   %   double ffirst; /* Floating X coordinate of first point */
    flast  = fread(f,1,'float64');   %   double flast;  /* Floating X coordinate of last point */
    fnsub  = fread(f,1,'uint16');    %   DWORD  fnsub;  /* Integer number of subfiles (1 if not TMULTI) */
    fxtype = fread(f,1,'uint8');     %   BYTE   fxtype; /* Type of X units (see definitions below) */
    fytype = fread(f,1,'uint8');     %   BYTE   fytype; /* Type of Y units (see definitions below) */
    fztype = fread(f,1,'uint8');     %   BYTE   fztype; /* Type of Z units (see definitions below) */
    fpost  = fread(f,1,'uint8');     %   BYTE   fpost;  /* Posting disposition (see GRAMSDDE.H) */
    fdate  = fread(f,3,'uint16');    %   DWORD  fdate;  /* Date/Time LSB: min=6b,hour=5b,day=5b,month=4b,year=12b */
    fres    = char(fread(f,9,'char')');     % char fres[9]; /* Resolution description text (null terminated) */
    fsource = char(fread(f,9,'char')');    % char fsource[9]; /* Source instrument description text (null terminated) */
    fpeakpt = fread(f,1,'uint16');         % WORD fpeakpt; /* Peak point number for interferograms (0=not known) */
    fspare  = fread(f,8,'float32');        % float fspare[8]; /* Used for Array Basic storage */
    fcmnt   = char(fread(f,130,'char')');  % char fcmnt[130]; /* Null terminated comment ASCII text string */
    fcatxt  = char(fread(f,30,'char')');   % char fcatxt[30]; /* X,Y,Z axis label strings if ftflgs=TALABS */
    flogoff = fread(f,1,'uint16');         % DWORD flogoff; /* File offset to log block or 0 (see above) */
    fmods   = fread(f,1,'uint16');         % DWORD fmods; /* File Modification Flags (see below: 1=A,2=B,4=C,8=D..) */
    % BYTE fprocs; /* Processing code (see GRAMSDDE.H) */
    % BYTE flevel; /* Calibration level plus one (1 = not calibration data) */
    % WORD fsampin; /* Sub-method sample injection number (1 = first or only ) */
    % float ffactor; /* Floating data multiplier concentration factor (IEEE-32) */
    % char fmethod[48]; /* Method/program/data filename w/extensions comma list */
    % float fzinc; /* Z subfile increment (0 = use 1st subnext-subfirst) */
    % DWORD fwplanes; /* Number of planes for 4D with W dimension (0=normal) */
    % float fwinc; /* W plane increment (only if fwplanes is not 0) */
    % BYTE fwtype; /* Type of W axis units (see definitions below) */
    % char freserv[187]; /* Reserved (must be set to zero) */
    
    remainder = fread(f,(512-ftell(f)),'char');  % Read to end of header
  elseif fversn==77
    %   fclose(f);
    %   error(['Wrong/Old File Type (' num2str(fversn) ')! Read file into Grams and save as a new file']);
    [fexper, fexp, fnpts, ffirst, flast, fnsub, fxtype, fytype, fztype, fpost, fdate, fcmnt] = readoldheader(f);
    [fres, fsource, fcatxt] = deal('');
    
    if ftflgs_bin(3)
      %TMULTI flag on
      fnsub = inf;   %trick code below to extract as many subfiles as it can (we can't tell how many with this file format)
    end
  else
    error(['Unsupported SPC File Version (' dec2hex(fversn) ').']);
  end
  
  if ftflgs_bin(7)
    error('Unable to read SPC sub-files with differently spaced x-axes. Break into separate sub-files')
  end
  
  %create or read x-axis
  if ~ftflgs_bin(8);
    fstep = ((flast-ffirst)/(fnpts-1));
    xaxis = ffirst:fstep:flast;
  else  %unequally spaced x-axis. read in xaxis
    xaxis = fread(f,fnpts,'float32');   %Read current subfile (float format)
  end
  
  %find sub-range (if requested)
  if isempty(wlrange);
    wlrange=1:fnpts;
  else
    if length(wlrange)==2; %end points given? do inclusive range w/all points
      wlrange = find(xaxis>=wlrange(1) & xaxis<=wlrange(2));
    else
      wlrange = interp1(xaxis,1:length(xaxis),wlrange,'nearest');
      wlrange(~isfinite(wlrange)) = [];  %drop invalid points
    end
    xaxis = xaxis(wlrange);
  end;
  
  %identify sub-files we want
  if fnsub==0
    fnsub = 1;   %try for at least one sub-file if none are found
  end
  if isempty(subs);
    if isfinite(fnsub)
      max_subs  = fnsub;
      totalsubs = fnsub;
    else
      max_subs  = inf;
      totalsubs = 1;  %will mean we don't pre-allocate to right size, but we can't tell how many we've got anyway
    end
  else
    if isinf(subs); subs=1:fnsub;end;
    if max(subs)>fnsub; error(['Not that many subfiles']); end;
    max_subs  = max(subs);
    totalsubs = length(subs);
  end
  
  % if subs=0 then tell user how many are in the file and the xaxis info
  if ~isempty(subs) & subs==0;
    if fversn==77
      error('Cannot return number of subfiles with old file format');
    end
    out   = fnsub;
    xaxis = [min(xaxis) max(xaxis) length(xaxis)];
    fclose(f);
    return;
  end
  
  index      = 0; 
  totalwls   = length(wlrange);
  
  %locate end of file
  posnow = ftell(f);
  fseek(f,0,1);  %locate end of file
  flength = ftell(f);
  fseek(f,posnow,-1);  %reset to spot we were reading from
  
  %Read sub-file(s)
  out=zeros(totalwls,totalsubs);   %set up output array (i.e. check memory)
  j = 0;
  while j<max_subs;
    j = j+1;
    if flength==ftell(f); break; end
    if feof(f); break; end
    [currentframe,subhead] = readsubfile(f,fnpts,fexp,fversn,fnsub);
    if isempty(subs) | any(j==subs);         %user wants this one?
      index=index+1;
      out(:,index) = currentframe(wlrange,:);  %then store it
      zaxis(index) = subhead(4);  %grab any z-axis info
    end;
  end;
  
  if nargout<2 & usedatasets
    textauditlog = true; %always do text log when doing dataset
  end
  auditlog = readauditlog(f,textauditlog);
catch
  fclose(f);
  rethrow(lasterror)
end

fclose(f);

out = out';  %transpose to row convention

%get description information
description = { fcmnt fcatxt };
description(cellfun('isempty',description)) = [];
description = char(description);

if usedatasets & nargout<=1;
  %create dataset if only one output requested
  out = dataset(out);

  out.description = description;
  
  [pth,out.name] = fileparts(filename);
  out.author = 'SPCREADR';
  
  out.axisscale{2} = xaxis;

  [pth,nm] = fileparts(filename);
  out.label{1} = repmat(nm,size(out.data,1),1);
  
  %Add x-units if available from file
  units = definexunits;
  index = find(ismember([units{:,1}],fxtype));
  if ~isempty(index)
    out.axisscalename{2} = units{index(1),2};
  end
  
  %Add y-units if available from file
  units = defineyunits;
  index = find(ismember([units{:,1}],fytype));
  if ~isempty(index)
    out.axisscalename{1} = units{index(1),2};
  end

  if ~all(zaxis==zaxis(1))
    out.axisscale{1} = zaxis;
  end

  out.userdata = auditlog;
  %out = addsourceinfo(out,filename);

end

%-----------------------------------------------------------------
function [currentframe,subhead] = readsubfile(f,fnpts,fexp,fversn,fnsub)
%READSUBFILE reads a subfile
% input f is file ID # and fnpts is # of points in a sub-file

subhead = readsubhdr(f);				% Read sub-header

if fversn==75
  if fnsub==1; subhead(2)=fexp; end;  %not multifile? use fexp
  %NOTE: above line fixes a number of problems seen with exponents when
  %loading files. This reconciles behavior expected with single files and
  %multi-files and works with all of the reported "incorrect" cases. JMS
  if subhead(2)~=-128
    currentframe=fread(f,fnpts,'int32');   %Read current subfile (IBM SPC format)
    currentframe = currentframe*(2^subhead(2))/(2^32);     %adjust for scaling denoted by subhead(2)
  else
    currentframe=fread(f,fnpts,'float32');   %Read current subfile (float format)
  end
else
  currentframe = fread(f,fnpts*2,'int16');
  currentframe = currentframe(2:2:end)+currentframe(1:2:end)*2^16;
  if (subhead(2)~=0 & fexp~=0)
    currentframe = currentframe.*(2^subhead(2))/(2^32)/fexp;
  end
end

%-----------------------------------------------------------------
function out=readsubhdr(f);
%READSUBHDR reads the sub header for a subfile
% input f is file ID #

subflgs=fread(f,1,'uint8');		%   BYTE  subflgs;	/* Flags as defined below */
%define SUBCHGD 1	/* Subflgs bit if subfile changed */
%define SUBNOPT 8	/* Subflgs bit if peak table file should not be used */
%define SUBMODF 128	/* Subflgs bit if subfile modified by arithmetic */
subexp=fread(f,1,'schar');			%   char  subexp;	/* Exponent for sub-file's Y values (80h=>float) */
subindx=fread(f,1,'uint16');		%   WORD  subindx;	/* Integer index number of trace subfile (0=first) */
subtime=fread(f,1,'float32');		%   float subtime;	/* Floating time for trace (Z axis corrdinate) */
subnext=fread(f,1,'float32');		%   float subnext;	/* Floating time for next trace (May be same as beg) */
subnois=fread(f,1,'float32');		%   float subnois;	/* Floating peak pick noise level if high byte nonzero*/
subnpts=fread(f,1,'uint32');		%   DWORD subnpts;	/* Integer number of subfile points for TXYXYS type*/
subscan=fread(f,1,'uint32');		%   DWORD subscan;	/*Integer number of co-added scans or 0 (for collect)*/
subresv=fread(f,8,'uint8');		%   char  subresv[8];	/* Reserved area (must be set to zero) */

% 1 1 2 4 4 4 4 4 8
out=[subflgs subexp subindx subtime subnext subnois subnpts subscan];


%-----------------------------------------------------------------
function log=readauditlog(f,textmode);
% READAUDITLOG reads the end of the file for all the audit log entries.

%read past end of file stuff to audit log (stop if we get to some real characters too)
fread(f,64,'uint8');

log=[];
key='';

while ~strcmp(key,'EOF');
  try;
    oneline=fgetl(f);
    
    if ~isstr(oneline) & oneline==-1;
      key=['EOF'];
      oneline=[];   
    else;
      
      if ~isempty(oneline) & any(oneline==5);
        %a code of 5 appears to mean end of audit log, look for it and take everything up to it.
        if min(find(oneline==5))>1;
          oneline=oneline(1:min(find(oneline==5))-1);
        else
          key=['EOF'];
          oneline=[];   
        end;
      end;
      if ~isempty(oneline);
        if ~textmode
          [key,value]=readkeyedline(oneline);					%read a line
          if ~isempty(key) & ~strcmp(key,'EOF');
            key(find(ismember(key(:),{' ' '.' ':' ';' ','}))) = '_';
            if ~isempty(findstr(lower(key),'date'));
              %is it a date field? try converting it to matlab-normal date format
              if isstr(value)
                log=setfield(log,key,datestr(datenumplus(value)));
              else
                log=setfield(log,key,datestr(value));
              end;
            else
              %not at date field? just write it out as usual
              if checkmlversion('>=','7')
                %Make sure field name is valid.
                key = genvarname(key);
              end
              log=setfield(log,key,value);
            end;
          end;
        else
          %text-only audit log mode, just return text, not structure
          log = strvcat(log,oneline);
        end
      end;
    end;
  catch;
  end;
end;

%----------------------------------------------------
function [key,value]=readkeyedline(line,token)

if nargin<2;
  token = '=';
end

[key,value] = strtok(line,token);
value = value(2:end);

nval = str2double(value);
if ~isnan(nval);  
  %If it WAS convertable using str2double, actually use str2num (more general)
  value = str2num(value);
end   %otherwise, return string

%----------------------------------------------------
function units = definexunits

units = {
  1,'Wavenumber (cm-1)'
  2,'Micrometers'
  3,'Nanometers'
  4,'Seconds'
  5,'Minutes'
  6,'Hertz'
  7,'Kilohertz'
  8,'Megahertz'
  9,'Mass (M/z)'
  10,'Parts per million'
  11,'Days'
  12,'Years'
  13,'Raman Shift (cm-1)'
  14,'Electron Volts (eV)'
  16,'Diode Number'
  17,'Channel'
  18,'Degrees'
  19,'Temperature (F)'
  20,'Temperature (C)'
  21,'Temperature (K)'
  22,'Data Points'
  23,'Milliseconds (mSec)'
  24,'Microseconds (uSec)'
  25,'Nanoseconds (nSec)'
  26,'Gigahertz (GHz)'
  27,'Centimeters (cm)'
  28,'Meters (m)'
  29,'Millimeters (mm)'
  30,'Hours'
  255,'Double interferogram (no display labels)'};

%----------------------------------------------------
function units = defineyunits

units = {
  1,'Interferogram'
  2,'Absorbance'
  3,'Kubelka-Munk'
  4,'Counts'
  5,'Volts'
  6,'Degrees'
  7,'Milliamps'
  8,'Millimeters'
  9,'Millivolts'
  10,'Log (1/R)'
  11,'Percent'
  12,'Intensity'
  13,'Relative Intensity'
  14,'Energy'
  16,'Decibel'
  19,'Temperature (F)'
  20,'Temperature (C)'
  21,'Temperature (K)'
  22,'Index of Refraction [N]'
  23,'Extinction Coeff. [K]'
  24,'Real'
  25,'Imaginary'
  26,'Complex'
  128,'Transmission'
  129,'Reflectance'
  130,'Arbitrary or Single Beam with Valley Peaks'
  131,'Emission'};

%-----------------------------------------------------
function [data,xaxis,auditlog] = parsecell(filename,otherargs,usedatasets,options)

blocksize = 300;
data     = [];
xaxis    = [];
auditlog = [];
labels   = {};
description = {};

if isempty(filename);    %empty cell = choose multiple SPC files
  if checkmlversion('>=','7');
    %Matlab 7.0
    [filename,pathname] = evriuigetfile({'*.spc;*.SPC','Galactic SPC files (*.spc)'},'Select File(s) To Load','MultiSelect','on');
    if isempty(filename) | isnumeric(filename); return; end
  else
    if checkmlversion('<','6.5');      %Matlab <6.5
      [filename,pathname] = evriuigetfile('*.*','Select any file in target folder');
    else      %Matlab 6.5+
      pathname = evriuigetdir(pwd,'Select folder with SPC files to load');
    end
    if isempty(pathname) | ~isstr(pathname); return; end
    filename = cell(0);
    filename = [dir(fullfile(pathname,'*.spc'))];
    filename = {filename.name};
    if isempty(filename)
      error('No SPC files in selected folder');
    end
    filename = filename(listdlg('ListString',filename,'Name','Select SPC files','PromptString','Select SPC file(s) to load'));
  end
  if ~iscell(filename);
    filename = {filename};
  end
  for ind=1:length(filename);
    filename{ind} = fullfile(pathname,filename{ind});
  end
end

%Read files
if strcmp(options.waitbar,'on') | (strcmp(options.waitbar,'auto') & length(filename)>100)
  wbhandle = waitbar(0,'Loading SPC Files...');
else
  wbhandle = [];
end
try
  dindex = 1;
  for fnameindex = 1:length(filename);
    if ishandle(wbhandle) & mod(fnameindex,10)==0
      waitbar(fnameindex/length(filename));
    end
    fname = filename(fnameindex);

    %read in file
    [data1,xaxis1,auditlog1,description1] = spcreadr(fname{:},otherargs{:});
    if ~isempty(xaxis) & ~isempty(xaxis1)
      %do spectral ranges match?
      if length(xaxis)~=length(xaxis1) | any(xaxis~=xaxis1);
        %no, do intersection
        switch options.axismatching
          case 'none'
            if length(xaxis)~=length(xaxis1);
              error(['Two or more spectra have different numbers of points (failure loading ' fname{:} ')']);
            end
            
          case 'interpolate'
            use = xaxis>min(xaxis1) & xaxis<max(xaxis1);
            if isempty(use);
              error(['Two or more spectra have no overlapping spectral regions (failure loading ' fname{:} ')']);
            end
            xaxis = xaxis(use);
            data   = data(:,use);
            data1  = interp1(xaxis1,data1,xaxis,'linear');
            xaxis1 = xaxis;
            
          otherwise
            [xaxis1,ia,ib] = intersect(xaxis1,xaxis);
            if isempty(xaxis1)
              error(['Two or more spectra have no spectral points in common (failure loading ' fname{:} ')']);
            end
            data1 = data1(:,ia);
            data  = data(:,ib);
        end
      end
    else
      %first spectrum, build data block up to length of file list
      data = zeros(length(filename)+1,size(data1,2));  %try allocating memory        
    end
    %add new data to existing matrix
    data(dindex:dindex+size(data1,1)-1,:) = data1;
    dindex = dindex+size(data1,1);
    if size(data,1)<dindex
      data = [data;zeros(blocksize,size(data,2))];
    end
    xaxis = xaxis1;
    if ~isempty(auditlog1);
      if isempty(auditlog);
        auditlog = {auditlog1};
      else
        auditlog(end+1) = {auditlog1};
      end
    end
    
    [pth,mylbl] = fileparts(fname{:});
    labels = [labels;repmat({mylbl},size(data1,1),1)];
    description{end+1} = description1;
    
  end
  
  description = char(description);
  
  %dump unused block padding
  data = data(1:dindex-1,:);
  
  if usedatasets
    %create dataset
    basedso = spcreadr(filename{1},otherargs{:});
    data = dataset(data);
    data.axisscale{2} = xaxis;
    data.axisscalename{1} = basedso.axisscalename{1};
    data.axisscalename{2} = basedso.axisscalename{2};
    data.label{1}  = labels;
    data.userdata  = auditlog;
    data.name      = 'Multiple SPC Files';
    data.author    = basedso.author;
    data.description = description;
    data           = addsourceinfo(data,filename);
  end
  
catch
  if ishandle(wbhandle)
    delete(wbhandle);
  end
  if exist('rethrow')
    rethrow(lasterror)  %only works in ver 6.5 and later
  else
    error(lasterr)  %use for ver 6.1 and earlier
  end
end
if ishandle(wbhandle)
  delete(wbhandle);
end

%---------------------------------------------------------
function out = getmlversion
%GETMLVERSION returns current Matlab version as a double
%
%I/O: version = getmlversion

%Copyright � Eigenvector Research, Inc. 2004
%Licensee shall not re-compile, translate or convert "M-files" contained
% in PLS_Toolbox for use with any software other than MATLAB�, without
% written permission from Eigenvector Research, Inc.

out = getappdata(0,'spcmatlabversion');
if isempty(out);
  out  = version;
  out  = str2num(out(1:3));
  setappdata(0,'spcmatlabversion',out);
end

%===========================================================
function [fexper, fexp, fnpts, ffirst, flast, fnsub, fxtype, fytype, fztype, fpost, fdate, ocmnt] = readoldheader(f)

%** Header items not defined in old format (or defined strangely and punted here) **
fexper = 0;   %   /* Reserved for internal use (experiment-1) */
fnsub  = 1;   %   /* Integer number of subfiles (1 if not TMULTI) */
fztype = 0;   %   /* Type of Z units (see definitions below) */
fpost  = 0;   %   /* Posting disposition (see GRAMSDDE.H) */
fdate  = 0;   %   /* Date/Time LSB: min=6b,hour=5b,day=5b,month=4b,year=12b */

fexp   = fread(f,1,'uint16');     %   short oexp;		/* Word rather than byte */
fnpts  = fread(f,1,'float32');   %   float onpts; 	/* Floating number of points */
ffirst = fread(f,1,'float32');   %   float ofirst;	/* Floating X coordinate of first pnt (SP rather than DP) */
flast  = fread(f,1,'float32');   %   float olast; 	/* Floating X coordinate of last point (SP rather than DP) */
fxtype = fread(f,1,'uint8');     %   BYTE  oxtype;	/* Type of X units */
fytype = fread(f,1,'uint8');     %   BYTE  oytype;	/* Type of Y units */

% remainder = fread(f,(256-ftell(f)-32),'char');  % Read to end of header (except subheader)
% 
% ocmnt = '';

oyear  = fread(f,1,'uint16');     %    WORD  oyear; 	/* Year collected (0=no date/time) - MSB 4 bits are Z type */
omonth = fread(f,1,'uint8');      %    BYTE  omonth;	/* Month collected (1=Jan) */
oday   = fread(f,1,'uint8');      %    BYTE  oday;		/* Day of month (1=1st) */
ohour  = fread(f,1,'uint8');      %    BYTE  ohour; 	/* Hour of day (13=1PM) */
ominute = fread(f,1,'uint8');     %    BYTE  ominute;	/* Minute of hour */
ores    = char(fread(f,8,'char')'); %    char  ores[8];	/* Resolution text (null terminated unless 8 bytes used) */
opeakpt = fread(f,1,'uint16');    %    WORD  opeakpt;
onscans = fread(f,1,'uint16');    %    WORD  onscans;
ospare  = fread(f,7,'float32');   %    float ospare[7];
ocmnt   = char(fread(f,130,'char')');  %    char  ocmnt[130];
ocatxt  = char(fread(f,30,'char')');   %    char  ocatxt[30];

ocmnt(ocmnt==0) = [];  %drop zeros from comment
ocmnt = strtrim(ocmnt);

% remainder = fread(f,(256-ftell(f)-33),'char');  % Read to end of header (except subheader)
% osubh1  = char(fread(f,32,'char')');   %    char  osubh1[32];	/* Header for first (or main) subfile included in main header */


  function [xdep,ydep, xSize, ySize]=findRectimRot(imr)
%

 %% description
    % recherche du plus grand rectangle inscrit dans une image tourn�e
    
%% input

    %  imr : image apr�s rotation
    
%% output
    
    %   [xdep,ydep, xSize, ySize] : coordonn�es x, y du point haut gauche
    %   du rectangle + taille en x, y

%% principe
    % le masque de l'image tourn�e est calcul�
    % les lignes sont parcourues une par une
    % pour chaque ligne : recherche des indices des colonnes du masque
    %                     calcul de la longueur des colonnes trouvees
    %                     calcul de la surface des rectangle comme : plus
    %                     petite longueur * nombre de colonnes trouvees
    % recherche de l'indice de ligne ayant la plus grande surface de
    % rectangle


%% use
    % imr=imrotate(im,angle,'bilinear');
    % [xdep,ydep, xSize, ySize]=findRectimRot(imr);
    % imsel=imr(xdep:(xdep+xSize-1),ydep:ydep+ySize-1);
    
%% Comments
    % ecrit pour recherche de recalages d'images dans des images avec
    % rotation
    
    
%% Author
    % MF Devaux
    % BIA-PVPP
    
%% date
    % 5 septembre 2013

%% context variables 
orig=pwd;           % returns the current directory

%% start

if nargin >1
    error('use: [xdep,ydep, xSize, ySize]=findRectimRot(imr)');
end



%% treatement
% masque 
mask=imr>0;

% nombre de ligne de l'image
nl=size(imr,1);
sr=zeros(size(1,nl));

% boucle sur les lignes
for i=1:nl
    j=find(mask(i,:));% indices des colonnes de valeurs non nulles
    if ~isempty(j)
        s=sum(mask(:,j));               % longueur des colonnes de valeurs non nulles
        sr(i)=min(s)*length(j);         % surface du plus grand rectangle coorrespondant � la ligne i
    else
        sr(i)=0;
    end
end

% recherche du plus grand rectangle
ir=find(sr==max(sr));
ir=find(sr>(max(sr)-25*max(sr)/100));



% selection de la premi�re valeur
ydep=ir(1);
xdep=find(mask(ydep,:),1);              % premier indice des colonnes de valeurs non nulles

xSize=find(mask(ydep,:),1,'last')-xdep+1;
ySize=sum(mask(ydep,:));

 

%% save 


%% matlab function tracking  
% no tracking for this fucntion
% fid=fopen(strcat(name,'.track.txt'),'w');
% 
% if fid==0
%     errordlg('enable to open track file');
% end;
% 
% fprintf(fid,'\r\n%s\t',datestr(now,0));
% fprintf(fid,'Import data set object from LAbspec *.spc map file \r\n');
% fprintf(fid,'__________________________________________________________________________\r\n');
% 
% % fprintf(fid,'\r\ninput file name: %s%s\r\n',name,ext);
% % fprintf(fid,'data folder: %s\r\n',pathname);
% % 
% % fprintf(fid,'\r\nsaved file name : %s \r\n',sname);
% % fprintf(fid,'data folder: %s\r\n',sfolder);
% 
% % save of function used
% fprintf(fid,'__________________________________________________________________________\r\n');
% info=which (mfilename);
% os=computer;        % return the type of computer used : windows, mac...
% switch os(1)
%     case 'P'                        % for windows
%         ind=strfind(info,'\');                          
%     case 'M'                        % for Mac
%         ind=strfind(info,'/');
%     otherwise
%         ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
% end;
% 
% repprog=info(1:(ind(length(ind))-1));
% fprintf(fid,'function name: %s ',mfilename);
% res=dir(info);
% fprintf(fid,'on %s \r\n',res.date);
% fprintf(fid,'function folder: %s \r\n',repprog);
% %fprintf(fid,'__________________________________________________________________________\r\n');
% 
% fclose(fid);

%% end

cd (orig)
    
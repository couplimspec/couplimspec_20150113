function T=matTransform(ang,sca,trans)
% fucntion that return the transform matrix in homogeneous coordinates
% from the rotation, scaling factor and translation vector

% input : 
%       ang : rotation agle in degree
%       sca : scaling factor
%       trans : line vector of the line/column translation coordinates
%

% output : 
%       sca * cos(ang)    - sca * sin(ang)    trans(line)
%
% T=    sca * sin(ang)      sca * cos ( ang)  trans(column)
%
%           0                   0                  1
%

% author : MF Devaux
%           BIA PVPP
%           26 septembre 2013


t=ang;
s=sca;
tl=trans(1);
tc=trans(2);

T=[cosd(t)*s -sind(t)*s tc; ...
    sind(t)*s cosd(t)*s tl; ...
    0 0 1];

% a noter : ligne = y et colonne = x : inverser pour les images et les
% donnees dans matalb....
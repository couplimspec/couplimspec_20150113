function [T]=multMatTransform(T1,T2)
porig=pwd;

if nargin==0
    [nom1,rep1]=uigetfile({'*.txt'},'nom du premier fichier param�tre','*.recal*.txt');
    cd (rep1)

    [angr1,scar1,tr1]=lireRecal(nom1,'.');

    nom1=strrep(nom1,'.recal.txt','');
    listenom1=decoderRecal(nom1);
    
    
    T1=matTransform(angr1,scar1,tr1);

    %sens1=ouinon('sens inverse ?');
    cd(porig);
    
    [nom2,rep2]=uigetfile({'*.txt'},'nom du second fichier param�tre','*.recal*.txt');
    cd (rep2)

    [angr2,scar2,tr2]=lireRecal(nom2,'.');
    %sens2=ouinon('sens inverse ?');
    nom2=strrep(nom2,'.recal.txt','');
    listenom2=decoderRecal(nom2);
    
    if ~strcmp(listenom1{1},strrep(listenom2{end},'.recal.txt',''))
        error('pas sur que ce soit une bonne idee de multiplier ces 2 fichiers %s et %s dans ce sens l�',nom1,nom2);
    else
        nom1=listenom1{2};
        for i=3:length(listenom1)
            nom1=strcat(nom1,'.TO.',listenom1{i});
        end;
        
        nom2=strrep(nom2,'.recal.txt','');
    end;

    T2=matTransform(angr2,scar2,tr2);

    
end;




T=T1*T2;

ecrireRecal(T,strcat(nom2,'.TO.',nom1));

cd(porig)


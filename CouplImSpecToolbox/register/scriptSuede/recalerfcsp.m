function recalerfcsp

close all

pathd=pwd;

[nomc,pathc]=uigetfile({'*.tif'},'image cible :','*.tif');
%pathr='Y:\01_projets\SYNCHROTRON_multimodalit�s\proposal1002\traiter\2f11mc2r-ir-fluo-raman\faisceau_cote\raman\sv.spi.ltf.norm2';
cd(pathc)
% fond noir raman
%nom='2f11mc2rfc2.sv.spi.ltf.norm.sum.tif';
imc=imread(nomc);

taille=lire(strrep(nomc,'.tif','.taille.txt'));
tpixc=taille.d(:,3);


figure('name','cible')
imshow(imc);

imcp{1}=imc;
i=1;
while(min(size(imcp{i}))>50)
    i=i+1;
    imcp{i}=impyramid(imcp{i-1},'reduce');
    figure('name',strcat('confocal : pyramide ',num2str(i)),'windowstyle','docked')
    imshow(imcp{i});
end;

cd(pathd)

[nom,pathr]=uigetfile({'*.tif'},'image � recaler :','*.tif');
%pathr='Y:\01_projets\SYNCHROTRON_multimodalit�s\proposal1002\traiter\2f11mc2r-ir-fluo-raman\faisceau_cote\raman\sv.spi.ltf.norm2';
cd(pathr)
% fond noir raman
%nom='2f11mc2rfc2.sv.spi.ltf.norm.sum.tif';
imr=imread(nom);

taille=lire(strrep(nom,'.tif','.taille.txt'));
tpixr=taille.d(:,3);

figure('name','� recaler dep','windowstyle','docked')
imshow(imr);


% pyramide 
imrp{1}=imr;
i=1;
while(min(size(imrp{i}))>30)
    i=i+1;
    imrp{i}=impyramid(imrp{i-1},'reduce');
    figure('name',strcat('� recaler : pyramide ',num2str(i)),'windowstyle','docked')
    imshow(imrp{i});
end;

ok=0;
while ~ok
    rep=input('niveau de d�part de la pyramide � recaler');
    nps=(rep);
    if nps>=1 && nps <=length(imrp)
        ok=1;
    end
end

ok=0;
while ~ok
    rep=input('niveau de d�part de la pyramide de r�f�rence');
    npc=rep;
    
    
    if npc>=1 && npc <=length(imcp)
        ok=1;
    end
end




%imref=imcp{1};
%imc=imcp{3};
imref=imcp{npc};
tpixRef=tpixc*2^(npc-1);
figure;
imshow(imc);
%[ref posref]=imcrop;
%ref=imc(7:(7+134),83:(83+128));
%posref
%ref=imc;
%imref=ref;
%imarecal=imrp{2};

imarecal=imrp{nps};
tpixRecal=tpixr*2^(nps-1);

% facteur � appliquer � l'image � recaler pour avoir la m�me r�solution que
% l'image de r�f�rence
factTheo=tpixRecal/tpixRef;

% on travaille avec 1/factheo pour travailler entre 0 et 1 sinon borne
% inconnue.
ok=0;
while ~ok
    if factTheo<1           % alors on part avec un mauvais rapport de r�solution entre image de ref et image � recaler
        if npc>1
            npc=npc-1;
            imref=imcp{npc};                
            tpixRef=tpixRef/2;
            factTheo=tpixRecal/tpixRef;
         else if nps<length(imrp)
                nps=nps+1;
                imarecal=imrp{nps};
                tpixRecal=tpixRecal*2;
                factTheo=tpixRecal/tpixRef;
             else
                error('mauvais choix de pyramide');
             end
        end
    else
        ok=1;
    end
end;



bif=1/factTheo-0.20;
bsf=1/factTheo+0.2;
pasf=(bsf-bif)/20;
fact=bif:pasf:bsf;


ok=0;
while ~ok
    rep=inputdlg('valeur approximative d''angle de rotation de l''image � recaler pour s''ajuster sur l''image de r�f�rence');
    angTheo=str2num(char(rep));
    if angTheo>-360 && angTheo<360
        ok=1;
    else
        warndlg('entrer unevaleur comprise entre -360 et +360');
    end
end
        
bia=angTheo-30;        %0;
bsa=angTheo+30;     %360;
pasa=3;
ang=bia:pasa:bsa;

fini=0;
while ~fini
    [ig,jg,il,ic]=trouverrecalage(imref,imarecal,fact,ang);
    
    factTrouve=fact(ig(1));
    angTrouve=ang(jg(1));
    
    if npc>1
        npc=npc-1;
        imref=imcp{npc};
        fc=1/2;
    else
        fc=1;
    end;
    
    if nps>1
        nps=nps-1;
        imarecal=imrp{nps};
        fs=2;
    else
        fs=1;
    end
    
    factTrouve=factTrouve*fc*fs;
    pasf=pasf*fc*fs;
    bif=(factTrouve-2*pasf);
    bsf=(factTrouve+2*pasf);
    pasf=(bsf-bif)/10;
    fact=bif:pasf:bsf;
    
    bia=angTrouve-2*pasa;
    bsa=angTrouve+2*pasa;
    pasa=(bsa-bia)/10;
    ang=bia:pasa:bsa;
    
    if fc==1 && fs==1
        if pasa>1
            fini=0;
        else
            fini=1;
        end
        
    end
end;

recal.i=nom;
recal.v=char('angle','factEchelle','posl','posc');

recal.d=[angTrouve 1/factTrouve il ic];

ecrire(recal,strcat(strrep(nom,'.tif',''),'.TO.',strrep(nomc,'.tif',''),'.recal.txt'));


cd(pathd)

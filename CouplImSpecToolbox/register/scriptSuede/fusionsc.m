function fusion(imref,imarecal,angr, scar,tr,cref,crecal)



porig=pwd;

if nargin==0
    
    [image1,rep1]=uigetfile({'*.tif'},'image ref','*.tif');
    cd(rep1)
    imref=imread(image1);
   % cimref=strrep(image1,'.tif','.taille.txt');
   % cref=lire(cimref);
     
    cd(porig);
    
    [image2,rep2]=uigetfile({'*.tif'},'image � projeter','*.tif');
    cd(rep2)
    imarecal=imread(image2);
 %   cimarecal=strrep(image2,'.tif','.taille.txt');
  %  crecal=lire(cimarecal);
    
    [nom,rep]=uigetfile({'*.txt'},'nom du fichier param�tre','*.recal*.txt');
    cd (rep)

    [angr,scar,tr]=lireRecal(nom,'.')  ; 
    
    
end;
nl1=size(imref,1);
nc1=size(imref,2);
%tpix1=cref.d(3);

Tr=matTransform(angr,scar,tr);

nl=size(imarecal,1);
nc=size(imarecal,2);
%tpix=crecal.d(3);

%d=[1 1 1 ;1 nl 1 ; nc nl 1 ; nc 1 1 ; 1 1 1 ];

%e=Tr*d';


[i,j]=ind2sub([nl nc],1:(nl*nc));
i=imresize(reshape(i,53,48),scar,'nearest');
j=imresize(reshape(j,53,48),scar,'nearest');

vim=imresize(imarecal,scar,'nearest');


cp=[j' i' ones(nl*nc,1)];

cpp=Tr*cp';
cppr=round(cpp(1:2,:));
indice=find(cppr(1,:)>0 & cppr(2,:)>0);
sel=cppr(:,indice);
io=i(indice);
jo=j(indice);



indice=find(sel(1,:)<=(nc1) & sel(1,:)>=1 & sel(2,:)<=(nl1) & sel(2,:)>=1);

io=io(indice);
jo=jo(indice);
sel=sel(:,indice);

indo=sub2ind(size(imarecal),io,jo);
indp=sub2ind(size(imref),sel(2,:),sel(1,:));

vv=uint8(zeros(size(imref)));
if tpix1>tpix
    for i=unique(indp)
        vv(i)=mean(imarecal(indo(indp==i)));
    end
else
    vv(indp)=imarecal(indo);
   % vv=imresize(imresize(vv,1/scar,'bilinear'),size(imref),'bilinear')
end;

% placer l'image � recaller dans l'image ref tourn�e
voir=imref;
voir(:,:,2)=0.25*vv(:,:,1)+0.75*vv;
voir(:,:,3)=0.25*vv(:,:,1)+0.75*vv;

figure

imshow(voir)

cd(porig);

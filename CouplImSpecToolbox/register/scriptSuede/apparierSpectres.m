function apparierSpectres


porig=pwd;

if nargin==0
    
    [image1,rep1]=uigetfile({'*.dso'},'image ref','*.dso');
    cd(rep1)
    dso=load(image1);
    imref=getfield(dso,char(fieldnames(dso)));
    clear dso;
    %cimref=strrep(image1,'.dso','.taille.txt');
   % cref=lire(cimref);
   
    [nom,rep]=uigetfile({'*.txt'},'nom du fichier des coordonnees','*.coord*.txt');
    cd (rep)

    co=lire(nom);     
   % cd(porig);
    
    [image2,rep2]=uigetfile({'*.dso'},'image projetee','*.dso');
    cd(rep2)
    dso=load(image2);
    imarecal=getfield(dso,char(fieldnames(dso)));
    clear dso;
    %cimarecal=strrep(image2,'.tif','.taille.txt');
    %crecal=lire(cimarecal);
    
    %cd(rep1)
    
    [nomir,repir]=uigetfile({'*.txt'},'nom du fichier des coordonnees infrarouge eliminees','*.coord*.txt');
    cd (repir)

    coelir=lire(nomir);
    
    ok=0;
    while ~ok
        rep=inputdlg('taille des imagettes (nombre impair)','taille',1,{'5'});
        taille=str2num(char(rep));
        if taille>0
            ok=1;
        end
    end;
end;


nb=size(co.d,1);

%indcolelim=coelir.d(1,2);
indelim=1;
% les indices colonnes sont rang�es par ordre

for i=1:nb
    indice=coelir.d(:,2)==co.d(i,2);
    sstabelim=coelir.d(indice,1);
    
    trouve=0;
    if ~isempty(sstabelim) 
        for j=1:length(sstabelim)
            if co.d(i,1)==sstabelim(j);
                trouve=1;
            end
        end
    end
    if trouve
        gi(i)=0;
        
    else
        gi(i)=1;
    end;
end;

gi=find(gi);

inl=co.d(gi,1);
inc=co.d(gi,2);

co.d=co.d(gi,:);
co.i=co.i(gi,:);

gi=sub2ind(imarecal.imagesize, inl ,inc);

% ir
spir=imarecal(gi,:);
spir.userdata.coord=[inl inc];

nir=spir.name;





nb=size(co.d,1);
dtail=(taille-1)/2;

% dso

spram=imref;
nram=spram.name;

for i=1:nb
    isel=co.d(i,3);
    isec=co.d(i,4);
    
    
    iml=(isel-dtail):(isel+dtail);
    imc=(isec-dtail):(isec+dtail);
    
    imagette=imref.imagedata(iml,imc,:);
    
%     if i==1
%         data=reshape(imagette,taille*taille,size(imagette,3));
%     else
%         data=[data;reshape(imagette,taille*taille,size(imagette,3))];
%     end
%     
    data(i,:,:)=reshape(imagette,taille*taille,size(imagette,3))';
    iml=reshape(repmat(iml,taille,1)',taille*taille,1);
    imc=reshape(repmat(imc,taille,1),taille*taille,1);
    coord(i,:,1:2)=[iml imc];
end;

dso=dataset;
dso.name=spram.name;
dso.author=spram.author;
dso.data=data;
dso.axisscale{1}=1:size(data,1);
dso.axisscale{2}=spram.axisscale{2};
dso.axisscale{3}=(1:taille^2);


dso.title{1}='pixel';
dso.label{1}=num2str(dso.axisscale{1}');

dso.label{2}=spram.label{2};
dso.title{2}='Wavenumber';

dso.label{3}=num2str((1:taille^2)');

dso.title{3}='Pixel imagette';

dso.description=spram.description;

dso.userdata=spram.userdata;
dso.userdata.coord=coord;
spram=dso;

%% save
cd ..
if ~exist('coinertie','dir')
    mkdir('coinertie')
end
cd('coinertie')

listeRep=decoderPath(rep1);
if ~exist(listeRep{end},'dir')
    mkdir(listeRep{end})
end
cd(listeRep{end})

spram.name=strcat(spram.name,'.apparie.',nir);

savedso(spram,spram.name,'.')

cd ..

listeRep=decoderPath(rep2);
if ~exist(listeRep{end},'dir')
    mkdir(listeRep{end})
end
cd(listeRep{end})

spir.name=strcat(spir.name,'.apparie.',nram);

savedso(spir,spir.name,'.')


cd(porig)


    
    
        
            
        
    
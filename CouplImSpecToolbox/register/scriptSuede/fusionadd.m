function vimref=fusionadd(imref,imarecal)



porig=pwd;

if nargin==0
    
    [image1,rep1]=uigetfile({'*.tif'},'image 1','*.tif');
    cd(rep1)
    imref=imread(image1);
       
    cd(porig);
    
    [image2,rep2]=uigetfile({'*.tif'},'image 2','*.tif');
    cd(rep2)
    imarecal=imread(image2);
      

    
    
end;

if length(size(imref))~=3
    error('de taille d''image 1');
end
if length(size(imarecal))~=3
    error('de taille d''image 2');
end

vimref=0.5*imref+0.5*imarecal;


figure
imshow(vimref)

cd(rep2)
imwrite(vimref,strcat(strrep(image2,'.tif',''),'-add-',image1),'tif','compression','none')


cd(porig);

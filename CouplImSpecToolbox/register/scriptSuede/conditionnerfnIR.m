function conditionnerfnIR
pathd=pwd;
close all


[nom,pathr]=uigetfile({'*.tif'},'image � traiter :','*.tif');
%pathr='Y:\01_projets\SYNCHROTRON_multimodalit�s\proposal1002\traiter\2f11mc2r-ir-fluo-raman\faisceau_cote\raman\sv.spi.ltf.norm2';
cd(pathr)
% fond noir raman
%nom='2f11mc2rfc2.sv.spi.ltf.norm.sum.tif';
imr=imread(nom);


% pathr='Y:\01_projets\SYNCHROTRON_multimodalit�s\proposal1002\traiter\2f11mc2r-ir-fluo-raman\faisceau_cote\raman';
% cd(pathr)
% % fond noir raman
% nom='2f11mc2rfc2.tif';
% imr=imread(nom);
% canal vert
imr=imr(:,:,2);
% �lmination du cadre + croix
imr=imclose(imr,strel('square',5));

figure('name','raman dep','windowstyle','docked')
imshow(imr);

fs=fspecial('unsharp');
imr=imfilter(imr,fs);

figure('name','raman dep sharpen','windowstyle','docked')
imshow(imr);

% correction de fond
tailfond=35;        % 35
imr=imbothat(imr,strel('square',tailfond));

figure('name','raman dep corr fond','windowstyle','docked')
imshow(imr);

% filtrage adapt� paroi (david)
imr=imdirfilter(imr,'immedian','max',11,16,1);

figure('name','raman dep corr fond filtrageparoi','windowstyle','docked')
imshow(imr);

% normalisation des varaitions d'intensit� sur les parois
seuilmin=100;
imrf=imhmin(imr,seuilmin);

fil=fspecial('average',11);
imrf=imfilter(imrf,fil);

imr=uint8(double(imr)./double(imrf)*255);

figure('name','raman dep corr fond filtrageparoi normalisee','windowstyle','docked')
imshow(imr);



% inversion des images pour ressembler � l'image concocale
% imr=imcomplement(imr);
% 
% figure('name','raman dep corr fond filtrageparoi normalisee','windowstyle','docked')
% imshow(imr);

%if ~exist('fondclair','dir')
%    mkdir('fondclair')
%end
%cd('fondclair')
cd ..
paths=pwd;
%paths='Y:\01_projets\SYNCHROTRON_multimodalit�s\proposal1002\traiter\2f11mc2r-ir-fluo-raman\faisceau_cote\raman\fondclair';
cd(paths)
imwrite(imr,nom,'tif','compression','none');

cd(pathd)

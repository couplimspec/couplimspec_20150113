function [ig,jg,il,ic]=trouverrecalage(imref,imarecal,fact,ang)

cc=testerrecalage(imref,imarecal,fact,ang);

[ig,jg]=find(cc==max(cc(:)));

figure
surf(fact,ang,cc','FaceColor','interp','EdgeColor','none','FaceLighting','phong')
xlabel('facteur');
ylabel('angle');

figure('name','raman recalable')
imshow(imrotate(imresize(imarecal,1/fact(ig(1))),-ang(jg(1))))

tmpref=imrotate(imref,ang(jg(1)),'bilinear');
%[xdep,ydep, xSize, ySize]=findRectimRot(tmpref);
%tmpref=tmpref(xdep:(xdep+xSize-1),ydep:(ydep+ySize-1));
f1=size(tmpref,1);
f2=size(tmpref,2);
tmprecal=imresize(imarecal,1/fact(ig(1)),'bilinear');
t1=size(tmprecal,1);
t2=size(tmprecal,2);

%C = normxcorr2(tmpr,iv);
C = normxcorr2(tmprecal,tmpref);
C=C(t1:f1,t2:f2);
% coorodnnes dans l'image de ref tournee
[i,j]=find(C==max(C(:)));
% coordonnees relative au centre de gravit�
ic=i-size(tmpref,1)/2;
jc=j-size(tmpref,2)/2;

% matrice de rotation pour retrouver les coordonnes relative de d�placement
% dans l'image imref avant rotation
R=rotationMatd(-ang(jg(1)));

% coordonnes 
rrc=R*[ic jc ]';
% coordonnes dans le rep�re de imref
rr=rrc+size(imref)'/2;
%il=i;
%ic=j;

%i=i+xdep-1;
%j=j+ydep-1;

tmpref=imrotate(imref,ang(jg(1)),'bilinear');

% placer l'image � recaller dans l'image ref tourn�e
voir=tmpref;
voir(:,:,2)=tmpref;
voir(:,:,3)=tmpref;
voir(i:(i+t1-1),j:(j+t2-1),2)=0.75*tmprecal+0.25*voir(i:(i+t1-1),j:(j+t2-1),2);
voir(i:(i+t1-1),j:(j+t2-1),3)=0.75*tmprecal+0.25*voir(i:(i+t1-1),j:(j+t2-1),3);

vv=imrotate(voir,-ang(jg(1)));

figure
subplot(1,2,1)
imshow(vv)

d=[1 1 1 ;1 size(imarecal,1) 1 ; size(imarecal,2) size(imarecal,1) 1 ; size(imarecal,2) 1 1 ; 1 1 1 ];
%d=[1 1 1 ;1 size(tmprecal,1) 1 ; size(tmprecal,2) size(tmprecal,1) 1 ; size(tmprecal,2) 1 1 ; 1 1 1 ];
t=ang(jg(1));
s=fact(ig(1));
T=[cosd(t)/s -sind(t)/s rr(2);sind(t)/s cosd(t)/s rr(1);0 0 1];
e=T*d';

%coord=[j i ; j i+size(imarecal,1)/fact(ig(1)); j+size(imarecal,2)/fact(ig(1)) i+size(imarecal,1)/fact(ig(1)) ; j+size(imarecal,2)/fact(ig(1)) i ];
%coord=[j i ; j i+size(tmprecal,1); j+size(tmprecal,2) i+size(tmprecal,1) ; j+size(tmprecal,2) i ];

%[Rcoord]=rotCoord([ j i],ang(jg(1)),size(tmpref));
%[Rcoord]=rotCoord(coord,ang(jg(1)),size(tmpref));
%[Rcoord]=rotCoord(coord,ang(jg(1)),size(imref));

%ic=(size(imref,1)+1)/2+Rcoord(1);
%il=(size(imref,2)+1)/2+Rcoord(2);

%Rc=Rcoord+repmat((size(imref)-1)/2,4,1);
%Rc=Rc+1;

%il=Rc(1,2);
%ic=Rc(1,1);

subplot(1,2,2)
imshow(imref)
hold on

%plot([ic ic ic+size(imarecal,2)/fact(ig(1)) ic+size(imarecal,2)/fact(ig(1)) ic],[il il+size(imarecal,1)/fact(ig(1)) il+size(imarecal,1)/fact(ig(1)) il il],'r')
%plot([Rc(1,1) Rc(2,1) Rc(3,1) Rc(4,1) Rc(1,1)],[Rc(1,2) Rc(2,2) Rc(3,2) Rc(4,2) Rc(1,2)],'r')

plot(e(1,:),e(2,:),'r');

% subplot(1,2,1)
% %imshow(imref)
% hold on
% plot(e(1,:),e(2,:),'r');

il=rr(1);
ic=rr(2);


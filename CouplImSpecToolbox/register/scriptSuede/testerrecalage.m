function cc=testerrecalage(imref,imarecal,fact,ang)


fi=0;
cc=zeros(length(fact),length(ang));
for f=fact
    fi=fi+1;
    fprintf('%d : ',fi);
   % tmp=imresize(iv,1/f,'bilinear');
     tmprecal=imresize(imarecal,1/f,'bilinear');

   if min(size(tmprecal))>10
        ai=0;
        for a=ang
            ai=ai+1;
            fprintf('%d ',ai);
            tmpref=imrotate(imref,a,'bilinear');
            
  %          if abs(a)>5
 %               [xdep,ydep, xSize, ySize]=findRectimRot(tmpref);
  %          else
                xdep=1;
                ydep=1;
                xSize=size(tmpref,1);
                ySize=size(tmpref,2);
  %          end;
        
           % if size(tmpr,1)<size(iv,1) && size(tmpr,2)<size(iv,2);
           % if size(tmp,1)<size(tmpf,1) && size(tmp,2)<size(tmpf,2);
            if size(tmprecal,1)<xSize && size(tmprecal,2)<ySize;
                t1=size(tmprecal,1);
                t2=size(tmprecal,2);
                tmpref=tmpref(xdep:(xdep+xSize-1),ydep:(ydep+ySize-1));
                f1=size(tmpref,1);
                f2=size(tmpref,2);
        
                %C = normxcorr2(tmpr,iv);
                C = normxcorr2(tmprecal,tmpref);
                C=C(t1:f1,t2:f2);
                [i,j]=find(C==max(C(:)));
               
                
                cc(fi,ai)=C(i,j);
            end
        end
   end
   fprintf('\r\n');
end
fprintf('\r\n');
function imagescore(spim,C,v)

coord=spim.userdata.coord;
nssim=length(coord);

typTab=length(size(coord{1}));

if typTab==2
    imin=min(C(:,v));
    imax=max(C(:,v));
else
    imin=min(min(C(:,v,:)));
    imax=max(max(C(:,v,:)));
end;

ds=1;

for i=1:nssim
    co=coord{i};
    nbi=size(co,1);
    fs=ds+nbi-1;
    
    
    if typTab==2
        nl=max(co(:,1));
        nc=max(co(:,2));
        sco=C(ds:fs,:);
    else 
        nl=max(max(co(:,:,1)));
        nc=max(max(co(:,:,2)));
        sco=C(ds:fs,:,:);
    end
    
    vim=zeros(nl,nc);
    
    if typTab==2
        for j=1:size(co,1)
            vim(co(j,1),co(j,2))=sco(j,v);
        end;
    else
        for j=1:size(co,1)
            for k=1:size(co,2)
                vim(co(j,k,1),co(j,k,2))=sco(j,v,k);
            end
        end;
    end;
    
    figure
    fmul=100/max(size(vim));
    imshow(imresize(vim,fmul,'nearest'),[imin imax])
    ds=fs+1;
end;

        





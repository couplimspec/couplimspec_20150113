function conditionnersp
pathd=pwd;
close all

[nom,pathr]=uigetfile({'*.tif'},'image � traiter :','*.tif');
%pathr='Y:\01_projets\SYNCHROTRON_multimodalit�s\proposal1002\traiter\2f11mc2r-ir-fluo-raman\faisceau_cote\raman\sv.spi.ltf.norm2';
cd(pathr)
% fond noir raman
%nom='2f11mc2rfc2.sv.spi.ltf.norm.sum.tif';
imr=imread(nom);

% correction de fond
tailfond=35;
imr=imtophat(imr,strel('square',tailfond));

figure('name','raman dep corr fond','windowstyle','docked')
imshow(imr);

% filtrage adapt� paroi (david)
imr=imdirfilter(imr,'immedian','max',5,16,1);

figure('name','raman dep corr fond filtrageparoi','windowstyle','docked')
imshow(imr);

ok =0;
while ~ok
    s=inputdlg('seuil min','seuil min',1,{'120'});
% normalisation des varaitions d'intensit� sur les parois
%seuilmin=120;
    seuilmin=str2num(char(s));
    imrf=imhmin(imr,seuilmin);

    fil=fspecial('average',15);
    tmp=imfilter(imrf,fil);

    tmp=uint8(double(imr)./double(tmp)*255);

    figure('name','raman dep corr fond filtrageparoi normalisee','windowstyle','docked')
    imshow(tmp);
    
    ok=ouinon('etes vous satisfait ?');
end;

%imr=uint8(double(imr)./double(tmp)*255);
imr=tmp;
figure
imshow(imr)
% inversion des images pour ressembler � l'image concocale
%imr=imcomplement(imr);

%figure('name','raman dep corr fond filtrageparoi normalisee','windowstyle','docked')
%imshow(imr);

%paths='Y:\01_projets\SYNCHROTRON_multimodalit�s\proposal1002\traiter\2f11mc2r-ir-fluo-raman\faisceau_cote\raman\fondclair';
paths=pathr;
cd(paths)
imwrite(imr,strrep(nom,'.tif','.recal.tif'),'tif','compression','none');

cd(pathd)

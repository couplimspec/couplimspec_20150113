function selregref


pathd=pwd;
close all

imc=imread('imRef.tif');


figure;
imshow(imc);
[ref , posref]=imcrop;


nom=uiputfile({'*.tif'},'sauvegarde de l''image sélectionnee ?','imRef.tif');

imwrite(ref,nom,'tif','compression','none');

p.d=[posref(2) posref(1) posref(4) posref(3)];
p.i=nom;
p.v=char('ldep','cdep','lsize','csize');

ecrire(p,strrep(nom,'.tif','.pos.txt'));

cd(pathd)
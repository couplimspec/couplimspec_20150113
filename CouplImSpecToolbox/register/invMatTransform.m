function [T,nom,rep]=invMatTransform

if nargin==0
    [nom,rep]=uigetfile({'*.txt'},'nom du fichier param�tre','*.recal*.txt');
    cd (rep)

    [angr,scar,tr]=lireRecal(nom,'.');
    nom=strrep(nom,'.recal.txt','');
    
    listenom=decoderRecal(nom);

    nb=length(listenom);
    nom=listenom{end};
    for i=(nb-1):-1:1
        nom=strcat(nom,'.TO.',listenom{i});
    end;
    
end;

Tr=matTransform(angr,scar,tr);
%T1=[t1.d(2)*cosd(t1.d(1)) (-1)*t1.d(2)*sind(t1.d(1)) t1.d(3);   t1.d(2)*sind(t1.d(1)) t1.d(2)*cosd(t1.d(1)) t1.d(4);0 0 1];
% if sens1
%     T1=inv(T1);
% end;
% T2=[t2.d(2)*cosd(t2.d(1)) (-1)*t2.d(2)*sind(t2.d(1)) t2.d(3);   t2.d(2)*sind(t2.d(1)) t2.d(2)*cosd(t2.d(1)) t2.d(4);0 0 1];
% if sens2
%     T2=inv(T2);
% end

T=inv(Tr);


% ligne image=colonne matrice matlab et vice versa
ecrireRecal(T,nom);


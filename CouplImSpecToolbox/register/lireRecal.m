function [ang,scale,trans]=lireRecal(nom,rep)
% focntion qui lit et d�code la matrice de recalage � partir d'un fichier de
% type *recal.txt
%
%
% input : nothing ou
%       nom : nom du fichier de recalage
%       rep : dossier contenant le fichier
%
% output : ang : angle de rotatio,, csale= facteur d'chelle
            %trans= vecteur ligne du d�placement en ligne e tcolonne
%
% pricnipe : les fichiers *recal* sont de la forme : 
%           nom	angle	factEchelle	posl	posc
%           
%
% author : MF Devaux
%           BIA PVPP
%           26 septembre 2013
% 
%%
porig=pwd;

%% entree des param�tres
if nargin ~=2 && nargin ~=0
    error('uses :[T,nom,rep]=builtMatTransform(nom,rep) or [T,nom,rep]=builtMatTransform');
end;

if nargin==0
    [nom,rep]=uigetfile({'*.txt'},'nom du fichier param�tre','*.recal*.txt');
end;

cd (rep)
t=lire(nom);

%%
ang=t.d(1);
scale=t.d(2);
tl=t.d(3);
tc=t.d(4);
trans=[tl tc];

%%
cd (porig)



% COUPLIMSPECTOOLBOX
%
% CouplImSpecToolbox aims at managing and analysing spectral images
% acquired with various modalities. 
%
%
% Requires the "DataSetObject" toobox, from EigenVectors.
%   http://www.eigenvector.com/software/dataset.htm
%
% Modules:
% analysis      - analysis of images and data
% basic         - common functions
% dso           - read and write DataSetObjects
% edit          - edition of data
% exempledata   - example data to test algorithms
% fileIO        - import and import of data files
% help          - some help files
% import        - import data
% plots         - display spectra and images
% preprocess    - preprocessing of spectra and images
% processCol    - processing of columns (?)
% register      - registration of hyperspectral images
% template      - file template for writing new functions
%

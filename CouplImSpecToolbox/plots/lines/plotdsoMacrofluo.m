  function hplot=plotdsoMacrofluo(hfig,labvar,data,xlab,ylab,titre)

 %% description
    % plot spectra in a view figure using the plot function and writting
    % xlabel and ylabel
    
%% input
    % hfig: handle of the figure
    % labvar: variable for x axis under the form of a vector of char array
    % data: data table of spectra
    % xlab, ylab: x and ylabel
    % titre : title of figure
     
    
%% output
    
    % 

%% principe
    % spectra are drawn using the plot function
    % x and y labels are written
    % 

%% use
    % hfig=figure
    % plotdsoMacrofluo(hfig,labvar,data,xlab,ylab,titre)
    
    
%% Comments
    % written to generalise plotdso
    
    
%% Author
    % MF Devaux
    % BIA PVPP - INRA  Nantes
    
%% date
    % 20 aout 2013



%% treatement
figure(hfig);
hplot=plot(data');
xlabel(xlab,'fontsize',16);
ylabel(ylab,'fontsize',16);
title(titre,'fontsize',16);
set(gca,'fontsize',12);
nbvar=size(labvar,1);
set(gca,'xtick',1:nbvar);
labvar=reshape(strrep(reshape(labvar,1,nbvar*4),'-',''),nbvar,3);
labvar(:,3)=lower(labvar(:,3));
set(gca,'xticklabel',labvar);
xlim([0 nbvar+1]);


%% matlab function tracking  

% no function tracking

%% end


    
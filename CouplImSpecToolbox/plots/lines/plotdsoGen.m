  function hplot=plotdsoGen(hfig,xvar,data,xlab,ylab,titre)

 %% description
    % plot spectra in a view figure using the plot function and writting
    % xlabel and ylabel
    
%% input
    % hfig: handle of the figure
    % xvar: variable for x axis
    % data: data table of spectra
    % xlab, ylab: x and ylabel
    % titre : title of figure
     
    
%% output
    
    % 

%% principe
    % spectra are drawn using the plot function
    % x and y labels are written
    % 

%% use
    % hfig=figure
    % plotdsoGen(hfig,xvar,data,xlab,ylab,titre)
    
    
%% Comments
    % written to generalise plotdso
    
    
%% Author
    % MF Devaux
    % BIA PVPP - INRA  Nantes
    
%% date
    % 19 aout 2013



%% treatement
figure(hfig);
hplot=plot(xvar,data');
xlabel(xlab,'fontsize',16);
ylabel(ylab,'fontsize',16);
title(titre,'fontsize',16);
set(gca,'fontsize',14)


%% matlab function tracking  

% no function tracking

%% end


    
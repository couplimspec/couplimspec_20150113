  function plotdso(ims,newfig,sauve,sfolder)

 %% description
    % plot of spectra from a data set object 
    % dso is a dataset object from eigenvector research see
    % http://www.eigenvector.com/software/dataset.htm
    
%% input
    % nothing
    %or
    %  dso : data set object or file name of dataset object
    %  newfig : flag to draw in a new figure (optional) default = 1
    %  sauve : flag for saving the figure (optional) default = 1
    % sfolder : save folder
    
    
%% output
    
    % 

%% principe
    % spectra are drawn using the plot function
    % axis are reversed depending on the method
    % x, y labels, title are extrated from dso  data
    % 

%% use
    % sauve=1
    % plotdso(dso,sauve); 
    % plotdso
    
    
%% Comments
    % written for F Allouche PhD thesis
    
    
%% Author
    % F  Allouche et MF Devaux
    % BIA PVPP - INRA  Nantes
    
%% date
    % 3 mars 2011
    % 14 mars 2011 : flag for a new figure
    % 11 mai 2012 : no label for other images

%% context variables 
orig=pwd;           % returns the current directory

%% input

if nargin ==0
     % input image
     [nom,repl]=uigetfile({'*.dso.mat'},'name of spectral image','*.dso.mat');
    
     cd(repl)
     dso=load(nom);
     ims=getfield(dso,char(fieldnames(dso)));
     clear dso;
     
     sauve=1;
     sfolder=pwd;
     newfig=1;
end;
     
if nargin >=1 
    if ischar(ims)
        nom=ims;
         % test du type de fichier
        point=strfind(nom,'.');
        point=point(length(point)-1);
        ext=nom((point+1):(length(nom)));
        if ~strcmp(ext,'dso.mat')
             error('format .dso.mat attendu')
        end;
        load(nom)
        ims=dso;
        clear dso;
    else if ~strcmp(class(ims),'dataset')
        error('give a data set as first input parameter');
        end;
    end;
end;
if nargin<=2
     sauve=1;
     sfolder=pwd;
end;
if nargin ==1
    newfig=1;
end;

if nargin ==3
     sfolder=pwd;
end;
if nargin >4
     error('use: plotdso or plotdso(dso,newfig,sauve,sfolder)');
end;

%% treatement
% in a separate figure or not
if newfig
    h=figure;
else
    h=figure(27);
    set(h,'windowstyle','docked');
end;


xlab=[ ims.title{2} ' (' ims.userdata.unit, ')'];
titre=ims.name;

switch ims.userdata.method
    case 'INFRARED'
        ylab='infrared absorbance';
        plotdsoGen(h,ims.axisscale{2},ims.data,xlab,ylab,titre);
        set(gca,'xdir','reverse')

    case 'RAMAN'
        ylab='Raman intensity';
        plotdsoGen(h,ims.axisscale{2},ims.data,xlab,ylab,titre);
        set(gca,'xdir','reverse')

    case 'FLUORESCENCE'
        ylab='fluorescence intensity';
        plotdsoGen(h,ims.axisscale{2},ims.data,xlab,ylab,titre);

    case 'CONFOCAL'
        ylab='confocal fluorescence intensity';
        [exc,em]=decode_vspecmcbl(ims.label{2});
        xlab='emission wavelengths (nm)';
        plotdsoConfocal(h,exc,em,ims.data,xlab,ylab,titre);
        
    case 'MACROFLUO'
        ylab='Macrofluo fluorescence intensity';
        xlab='Macrofluo images';
        plotdsoMacrofluo(h,ims.label{2},ims.data,xlab,ylab,titre);
        
    otherwise
        ylab=sprintf('%s intensity',ims.userdata.method'');
        plotdsoGen(h,ims.axisscale{2},ims.data',xlab,ylab,titre);
        %set(gca,'xticklabel',ims.label{2});
end;



%% save 
if sauve
    cd(sfolder)
    sname=strcat(ims.name,'.sp.png');
    saveas(gcf,sname,'png');
end;


%% matlab function tracking  

% no function tracking

%% end

cd (orig)
    
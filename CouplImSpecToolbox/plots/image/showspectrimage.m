function gim=showspectrimage(varargin)

 %% description
    % compute a grey level image from a spectral image for a given wavelength, a band between two wavelength or as
    % the sum or norm of the whole spectra
    
%% input parameters can be
    % - no parameter
    % or
    % 1 - spectral image in dso format
    % 2 - string to indicate that the folllowing parameter is the method to
    %   be employed for computing the grey level image: expected values are:
    %           - 'WAVELENGTH' or 'wavelength' for a given wavelength
    %           - 'BAND' or 'band' for the area under a band
    %           - 'SUM' or 'sum' for the sum of all intensities of the
    %           spectra
    %           - 'NORM' or 'norm' for the norm of intensities for the
    %           whole spectra 
    % 3 - in case of method 'LO': vector of one value indicating the wavelength or wavenumber
    %   - in case of method 'BAND': vector of two values indicating the
    %                               wavelengths or wavenumbers that delimitate the band
    % 4 - save folder 
    
%% output
    
    % - nothing 
    % or
    % - the grey level image as a 2D uint8 array

%% principle
    % - case 'WAVELENGTH' : the intensisty at a given wavelength is represented as a
    %               grey level image
    % - case 'BAND' : the sum of intensity between two bands is calculated
    %                   after a linear baseline correction under the band
    % - case 'SUM' :  the sum of intensities of the whole spectra is
    %                   computed
    % - case 'NORM' : the sum of squared intensities is assessed
    %
    % in all cases : grey level values are adjusted to fit the 0-255 grey
    %                   level range and converted to unsigned int 8
    %
    % if no output argument is provided : the grey level image is saved in
    % a sub-folder 'showim' using the name found in the dso format
    %

%% use
    % showspectrimage;
    
    % ims=readomnic;
    % gim=showspectrimage(ims,'BAND',[1200 800])
    
%% Comments, context
    % written for the PhD thesis of Fatma Allouche
    
    
%% Author
    % MF Devaux et F Allouche
    % BIA - PVPP INRA Nantes
    
%% date
    % 2 mars 2011
    % 10 mars 2011 : bug in data input nargin ==1

%% context variables 
orig=pwd; % returns the current directory



%% input data
if nargin == 0
    % input image
    [nom,repl]=uigetfile({'*.dso.mat'},'name of spectral image','*.dso.mat');

    cd(repl)
    dso=load(nom);
    ims=getfield(dso,char(fieldnames(dso)));
    clear dso;

    % choice of the method
    choix={'one wavelength','sum between two wavelengths', 'sum of intensities', 'norm of intensities'};
    codemethod={'WAVELENGTH','BAND','SUM','NORM'};
    [nummethod,ok] = listdlg('ListString',choix, 'SelectionMode','single','PromptString','choose method',...
        'name','method to compute grey level image','listsize',[300 160]);
    if ok 
        method=codemethod{nummethod};
    else if isempty(nummethod)  % cancel of no method chosen
            disp('method choice cancelled showspectrimage');
            return;
        end;
    end;

    % according to the method wavelengths are required :
    switch method
        case 'WAVELENGTH'
            lot=ims.axisscale{2};           % liste des longueurs d'onde de l'image spectrale
            figure(1)                       % graphes de la s�rie des spectres
            ok=0;
            while ~ok
                nbs=size(ims.data,1);
                st=round(nbs/100);
                plot(lot,ims.data(1:st:nbs,:)); 
                if strcmp(ims.title{2},'Wavenumber')
                    set(gca,'Xdir','reverse');
                end;
                xlabel([ims.title{2} ' (' ims.userdata.unit ')'], 'fontsize',16)
                ylabel('intensity', 'fontsize',16)
                [x,y]=ginput(1);
                hold on
                plot([x x],[min(ims.data(:)) max(ims.data(:))],':k')
                title(['selected ' ims.title{2} ': ' num2str(round(x)) ' ' ims.userdata.unit])
                ok=ouinon(sprintf('selected %s : %d %s  Ok ?',ims.title{2},round(x),ims.userdata.unit));
                if ~ok
                    los = inputdlg(sprintf('enter %s',ims.title{2}),sprintf('choose %s',ims.title{2}),1,{num2str(x)});
                    if isempty(los)
                        disp('no wavelength chosen');
                        return;
                    end;
                    x=str2double(los);

                    if x>min(lot) && x<max(lot)
                        ok=1;
                    else
                        ok=ouinon('valeur erron�e, voulez vous continuer ?');
                        if ~ok
                            error('choice of %s cancelled in showspectrimage',ims.title{2});
                        else
                            ok=0;
                        end;
                    end;
                end;

                hold off
            end;
            lo=x;
            clear x
            clear y

            silo=lot-repmat(lo,1,length(lot));
            indicelo=find(abs(silo)==min(abs(silo)));
            lo=lot(indicelo);



       case 'BAND'
            lot=ims.axisscale{2};           % liste des longueurs d'onde de l'image spectrale
            figure(1)                       % graphes de la s�rie des spectres
            ok=0;
            while ~ok
                nbs=size(ims.data,1);
                st=round(nbs/100);
                plot(lot,ims.data(1:st:nbs,:)); 
                if strcmp(ims.title{2},'Wavenumber')
                    set(gca,'Xdir','reverse');
                end;
                xlabel([ims.title{2} ' (' ims.userdata.unit ')'], 'fontsize',16)
                ylabel('intensity', 'fontsize',16)
                [x,y]=ginput(1);
                hold on
                plot([x x],[min(ims.data(:)) max(ims.data(:))],':k')
                title(['selected ' ims.title{2} ': ' num2str(round(x))])
                ok=ouinon(sprintf('selected %s : %d %s  Ok ?',ims.title{2},round(x),ims.userdata.unit));
                if ~ok
                    los = inputdlg(sprintf('enter %s',ims.title{2}),sprintf('choose %s',ims.title{2}),1,{num2str(x)});
                    if isempty(los)
                        disp('no wavelength chosen');
                        return;
                    end;
                    x=str2double(los);

                    if x>min(lot) && x<max(lot)
                        ok=1;
                    else
                        ok=ouinon('valeur erron�e, voulez vous continuer ?');
                        if ~ok
                            error('choice of %s cancelled in showspectrimage',ims.title{2});
                        else
                            ok=0;
                        end;
                    end;
                end;
                hold off
            end;
            lo(1)=x;
            clear x
            clear y
            ok=0;
            while ~ok
                nbs=size(ims.data,1);
                st=round(nbs/100);
                plot(lot,ims.data(1:st:nbs,:)); 
                if strcmp(ims.title{2},'Wavenumber')
                    set(gca,'Xdir','reverse');
                end;
                xlabel([ims.title{2} ' (' ims.userdata.unit ')'], 'fontsize',16)
                ylabel('intensity', 'fontsize',16)
                hold on
                plot([lo(1) lo(1)],[min(ims.data(:)) max(ims.data(:))],':k')

                [x,y]=ginput(1);
                plot([x x],[min(ims.data(:)) max(ims.data(:))],':k')
                title(['selected ' ims.title{2} ': ' num2str(round(lo(1))) ' - ' num2str(round(x))])
                ok=ouinon(sprintf('selected %s : %d %s  Ok ?',ims.title{2},round(x),ims.userdata.unit));
                if ~ok
                    los = inputdlg(sprintf('enter %s',ims.title{2}),sprintf('choose %s',ims.title{2}),1,{num2str(x)});
                    if isempty(los)
                        disp('no wavelength chosen');
                        return;
                    end;
                    x=str2double(los);

                    if x>min(lot) && x<max(lot)
                        ok=1;
                    else
                        ok=ouinon('valeur erron�e, voulez vous continuer ?');
                        if ~ok
                            error('choice of %s cancelled in showspectrimage',ims.title{2});
                        else
                            ok=0;
                        end;
                    end;
                end;
                hold off
            end;
            lo(2)=x;
            clear x
            clear y                
            silo=lot-repmat(lo(1),1,length(lot));
            indicelo(1)=find(abs(silo)==min(abs(silo)));
            lo(1)=lot(indicelo(1));

            silo=lot-repmat(lo(2),1,length(lot));
            indicelo(2)=find(abs(silo)==min(abs(silo)));
            lo(2)=lot(indicelo(2));

            clear silo

        otherwise
    end;

    % sauvegarde de l'image si appel sans param�tres. Sinon, laiss� �
    % l'appr�ciation de l'utilisateur/programmeur qui appelle la fonction
    sauve=1;
end;

if nargin >= 2
    % test si l'image en entr�e est un dataset or a file
    ims=varargin{1};
    if ischar(ims)
        nom=ims;
         % test du type de fichier
        point=strfind(nom,'.');
        point=point(length(point)-1);
        ext=nom((point+1):(length(nom)));
        if ~strcmp(ext,'dso.mat')
             error('format .dso.mat attendu')
        end;
        load(nom)
        ims=dso;
        clear dso;
    else if ~strcmp(class(ims),'dataset')
        error('give a data set as first input parameter');
        end;
    end;

    method=upper(varargin{2});
    repl=pwd;                               % reading folder
end;

if nargin == 1
  
    ims=varargin{1};
  
    % choice of the method
    choix={'one wavelength','sum between two wavelengths', 'sum of intensities', 'norm of intensities'};
    codemethod={'WAVELENGTH','BAND','SUM','NORM'};
    [nummethod,ok] = listdlg('ListString',choix, 'SelectionMode','single','PromptString','choose method',...
        'name','method to compute grey level image','listsize',[300 160]);
    if ok 
        method=codemethod{nummethod};
    else if isempty(nummethod)  % cancel of no method chosen
            disp('method choice cancelled showspectrimage');
            return;
        end;
    end;

    % according to the method wavelengths are required :
    switch method
        case 'WAVELENGTH'
            lot=ims.axisscale{2};           % liste des longueurs d'onde de l'image spectrale
            figure(1)                       % graphes de la s�rie des spectres
            ok=0;
            while ~ok
                nbs=size(ims.data,1);
                st=round(nbs/100);
                plot(lot,ims.data(1:st:nbs,:)); 
                if strcmp(ims.title{2},'Wavenumber')
                    set(gca,'Xdir','reverse');
                end;
                xlabel([ims.title{2} ' (' ims.userdata.unit ')'], 'fontsize',16)
                ylabel('intensity', 'fontsize',16)
                [x,y]=ginput(1);
                hold on
                plot([x x],[min(ims.data(:)) max(ims.data(:))],':k')
                title(['selected ' ims.title{2} ': ' num2str(round(x)) ' ' ims.userdata.unit])
                ok=ouinon(sprintf('selected %s : %d %s  Ok ?',ims.title{2},round(x),ims.userdata.unit));
                if ~ok
                    los = inputdlg(sprintf('enter %s',ims.title{2}),sprintf('choose %s',ims.title{2}),1,{num2str(x)});
                    if isempty(los)
                        disp('no wavelength chosen');
                        return;
                    end;
                    x=str2double(los);

                    if x>min(lot) && x<max(lot)
                        ok=1;
                    else
                        ok=ouinon('valeur erron�e, voulez vous continuer ?');
                        if ~ok
                            error('choice of %s cancelled in showspectrimage',ims.title{2});
                        else
                            ok=0;
                        end;
                    end;
                end;

                hold off
            end;
            lo=x;
            clear x
            clear y

            silo=lot-repmat(lo,1,length(lot));
            indicelo=find(abs(silo)==min(abs(silo)));
            lo=lot(indicelo);



       case 'BAND'
            lot=ims.axisscale{2};           % liste des longueurs d'onde de l'image spectrale
            figure(1)                       % graphes de la s�rie des spectres
            ok=0;
            while ~ok
                nbs=size(ims.data,1);
                st=round(nbs/100);
                plot(lot,ims.data(1:st:nbs,:)); 
                if strcmp(ims.title{2},'Wavenumber')
                    set(gca,'Xdir','reverse');
                end;
                xlabel([ims.title{2} ' (' ims.userdata.unit ')'], 'fontsize',16)
                ylabel('intensity', 'fontsize',16)
                [x,y]=ginput(1);
                hold on
                plot([x x],[min(ims.data(:)) max(ims.data(:))],':k')
                title(['selected ' ims.title{2} ': ' num2str(round(x))])
                ok=ouinon(sprintf('selected %s : %d %s  Ok ?',ims.title{2},round(x),ims.userdata.unit));
                if ~ok
                    los = inputdlg(sprintf('enter %s',ims.title{2}),sprintf('choose %s',ims.title{2}),1,{num2str(x)});
                    if isempty(los)
                        disp('no wavelength chosen');
                        return;
                    end;
                    x=str2double(los);

                    if x>min(lot) && x<max(lot)
                        ok=1;
                    else
                        ok=ouinon('valeur erron�e, voulez vous continuer ?');
                        if ~ok
                            error('choice of %s cancelled in showspectrimage',ims.title{2});
                        else
                            ok=0;
                        end;
                    end;
                end;
                hold off
            end;
            lo(1)=x;
            clear x
            clear y
            ok=0;
            while ~ok
                nbs=size(ims.data,1);
                st=round(nbs/100);
                plot(lot,ims.data(1:st:nbs,:)); 
                if strcmp(ims.title{2},'Wavenumber')
                    set(gca,'Xdir','reverse');
                end;
                xlabel([ims.title{2} ' (' ims.userdata.unit ')'], 'fontsize',16)
                ylabel('intensity', 'fontsize',16)
                hold on
                plot([lo(1) lo(1)],[min(ims.data(:)) max(ims.data(:))],':k')

                [x,y]=ginput(1);
                plot([x x],[min(ims.data(:)) max(ims.data(:))],':k')
                title(['selected ' ims.title{2} ': ' num2str(round(lo(1))) ' - ' num2str(round(x))])
                ok=ouinon(sprintf('selected %s : %d %s  Ok ?',ims.title{2},round(x),ims.userdata.unit));
                if ~ok
                    los = inputdlg(sprintf('enter %s',ims.title{2}),sprintf('choose %s',ims.title{2}),1,{num2str(x)});
                    if isempty(los)
                        disp('no wavelength chosen');
                        return;
                    end;
                    x=str2double(los);

                    if x>min(lot) && x<max(lot)
                        ok=1;
                    else
                        ok=ouinon('valeur erron�e, voulez vous continuer ?');
                        if ~ok
                            error('choice of %s cancelled in showspectrimage',ims.title{2});
                        else
                            ok=0;
                        end;
                    end;
                end;
                hold off
            end;
            lo(2)=x;
            clear x
            clear y                
            silo=lot-repmat(lo(1),1,length(lot));
            indicelo(1)=find(abs(silo)==min(abs(silo)));
            lo(1)=lot(indicelo(1));

            silo=lot-repmat(lo(2),1,length(lot));
            indicelo(2)=find(abs(silo)==min(abs(silo)));
            lo(2)=lot(indicelo(2));

            clear silo

        otherwise
    end;

    % sauvegarde de l'image si appel sans param�tres. Sinon, laiss� �
    % l'appr�ciation de l'utilisateur/programmeur qui appelle la fonction
    sauve=1;
    repl=pwd;  
end;


if nargin == 2
    if ~strcmp(method,'SUM') && ~strcmp(method,'NORM')
        disp('Wrong method or wrong number of parameter');
        error('usage: gim=showspectrimage(ims,''WAVELENGTH'' or ''BAND'', wavelength or wavevenumber)');
    end;
    % pas de sauvegarde de l'image si appel avec param�tres. Laiss� �
    % l'appr�ciation de l'utilisateur/programmeur qui appelle la
    % fonction
    sauve=0;
end;
        
if nargin >3        
    if ~strcmp(method,'WAVELENGTH') && ~strcmp(method,'BAND')
        disp('Wrong method or wrong number of parameter');
        error('Usage: gim=showspectrimage(ims,''SUM'' or ''NORM''');
    end;

    if strcmp(method,'WAVELENGTH')
        lo=varargin{3}; 
        if ~strcmp(class(lo),'double')
            error('Wrong wavelength or wavenumber (real value expected)');
        end;
        if length(lo)>1
            error('Wrong number of wavelength or wavenumber (1 expected)');
        end;
        lot=ims.axisscale{2};           % liste des longueurs d'onde de l'image spectrale
        if lo<min(lot)|| lo>max(lot)
            error('Wrong wavelength value: must be between %s and %s',min(lot),max(lot))
        end;
        silo=lot-repmat(lo,1,length(lot));
        indicelo=find(abs(silo)==min(abs(silo)));
        lo=lot(indicelo);
    end;

    if strcmp(method,'BAND')
        lo=varargin{3}; 
        if ~strcmp(class(lo),'double')
            error('Wrong wavelength or wavenumber (real value expected)');
        end;
        if length(lo)~=2
            error('Wrong number of wavelength or wavenumber (2 expected)');
        end;
        lot=ims.axisscale{2};           % liste des longueurs d'onde de l'image spectrale
        if lo(1)<min(lot)|| lo(1)>max(lot)
            error('Wrong wavelength value 1: must be between %s and %s',min(lot),max(lot))
        end;
        if lo(2)<min(lot)|| lo(2)>max(lot)
            error('Wrong wavelength value 2: must be between %s and %s',min(lot),max(lot))
        end;
        silo=lot-repmat(lo(1),1,length(lot));
        indicelo(1)=find(abs(silo)==min(abs(silo)));
        lo(1)=lot(indicelo(1));
        silo=lot-repmat(lo(2),1,length(lot));
        indicelo(2)=find(abs(silo)==min(abs(silo)));
        lo(2)=lot(indicelo(2));
    end;

    % pas de sauvegarde de l'image si appel avec param�tres. Laiss� �
    % l'appr�ciation de l'utilisateur/programmeur qui appelle la
    % fonction
    sauve=0;
end;

if nargin == 3
    sauve=1;
    sfolder=varargin{3};
    
end;

if nargin == 4
    sauve=1;
    sfolder=varargin{4};
end;

if nargin >4
    disp('Wrong number of parameter')
    error('Usage: gim=showspectrimage(ims,''METHOD'', wavelength or wavevenumber,sfolder');
end;

% test des param�tres de sortie
if  nargout >1
    error('wrong number of output parameter')
end;
% 
%% Compute grey level image

switch method
    case 'WAVELENGTH'
        
        % image
        gim=ims.imagedata(:,:,indicelo);
        
    case 'BAND'
        
        % correction de ligne de base des spectres pour la bande consid�r�e
        if indicelo(1)<indicelo(2)      % cas fluorescence (set longueur d'onde)
            i1=indicelo(1);
            i2=indicelo(2);
        else                            % cas IR ou Ramand avec nombre d'onde invers�s dans les graphiques
                                        % mais dans le sens croissant dans la matrice des donn�es
            i1=indicelo(2);
            i2=indicelo(1);
        end;
            
        sp=ims.data(:,i1:i2);           % s�lection de la bande d'int�r�t
        nl=size(sp,2);
        
        % calcul de la droite � soustraire
        a=(sp(:,nl)-sp(:,1))/(nl-1);
        b=sp(:,1)-a;
        
        % soustraction de la droite pour la bande consid�r�e
        spc=sp-repmat(a,1,nl).*repmat(1:nl,size(sp,1),1)-repmat(b,1,nl);
        
        % calcul de l'aire sous la bande
        gim=sum(spc,2);
        gim=reshape(gim,ims.imagesize(1),ims.imagesize(2));
        
    case 'SUM'
        gim=sum(ims.imagedata,3);
        
    case 'NORM'
        gim=sum(ims.imagedata.*ims.imagedata,3);
end;


% adjust values to 0-255 grey levels
mini=min(gim(:));
maxi=max(gim(:));

a=255/(maxi-mini);
b=-a*mini;

gim=uint8(gim*a+b);

% show grey level image
if nargin==1
    figure(27)
    imagesc(ims.userdata.dX,ims.userdata.dY,gim);
    colormap(jet)
    title(ims.name,'Fontsize',12);
    set(gca,'dataAspectRatio',[1 1 1])
else
    h=figure(27);
    set(h,'windowstyle','docked');
    imagesc(ims.userdata.dX,ims.userdata.dY,gim);
    colormap(jet)
    title(ims.name,'Fontsize',12);
    set(gca,'dataAspectRatio',[1 1 1])
end
%% save 
if sauve
    switch method
        case 'WAVELENGTH'
            sname=strcat(ims.name,'.w',num2str(round(lo)),'.tif');
        case 'BAND'
            sname=strcat(ims.name,'.w',num2str(round(lo(1))),'-',num2str(round(lo(2))),'.tif');
        case 'SUM'
            sname=strcat(ims.name,'.sum.tif');
        case 'NORM'
            sname=strcat(ims.name,'.norm.tif');
    end;
    if exist('sfolder','var')
        if ~exist(sfolder,'dir')
            mkdir(sfolder)
        end;
    else if ~exist(strcat(ims.name,'.showim'),'dir')
        mkdir(strcat(ims.name,'.showim'))
        end;
        sfolder=strcat(ims.name,'.showim');
    end;
    
    cd(sfolder)
    imwrite(gim,sname,'tif','compression','none');
end;


%% matlab function tracking  only if grey level image is saved on disk

% i=strfind(fname,'.');
% gname=fname(1:i(1)-1);
% 
nom=ims.name;
if sauve
    fid=fopen(strrep(sname,'.tif','.track.txt'),'w');

    if fid==0
         errordlg('enable to open track file');
     end;
 
    fprintf(fid,'\r\n%s\t',datestr(now,0));
    fprintf(fid,'Compute grey level image from spectral image \r\n');
    fprintf(fid,'__________________________________________________________________________\r\n');

    fprintf(fid,'\r\nInput spectral image name: %s\r\n',nom);
    fprintf(fid,'data folder: %s\r\n\r\n',repl);
    
    switch method
        case 'WAVELENGTH'
            fprintf(fid,'image at %s: %d %s\r\n\r\n',lower(ims.title{2}),round(lo), ims.userdata.unit);
            
        case 'BAND'
            fprintf(fid,'sum of intensities between %d and %d %s \r\n',round(lo(1)), round(lo(2)),ims.userdata.unit);
            fprintf(fid,'after removing the linear baseline between the two %ss\r\n\r\n',lower(ims.title{2}));
            
        case 'SUM'
            fprintf(fid,'image of the sum of intensities\r\n\r\n');        
            
        case 'NORM'
            fprintf(fid,'image of the spectra norms (sum of squared intensities)\r\n\r\n');    
    end;
    
    
    fprintf(fid,'\r\nsaved file name : %s \r\n',sname);
    fprintf(fid,'\r\nsave folder : %s \r\n',sfolder);


    % save of function used
    fprintf(fid,'__________________________________________________________________________\r\n');
    info=which (mfilename);
    os=computer;        % return the type of computer used : windows, mac...
    switch os(1)
        case 'P'                        % for windows
            ind=strfind(info,'\');                          
        case 'M'                        % for Mac
            ind=strfind(info,'/');
        otherwise
            ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
    end;

    repprog=info(1:(ind(length(ind))-1));
    fprintf(fid,'function name: %s ',mfilename);
    res=dir(info);
    fprintf(fid,'on %s \r\n',res.date);
    fprintf(fid,'function folder: %s \r\n',repprog);
    
    
    fclose(fid);
end;


%% end

cd (orig)
    
function [imsc] =corriger_bruit_fft(varargin)
% correction du bruit dans des spectres de fluorescence
% tous les spectres d'un m�me fichier sont trait�s
% fonction ecrite pour les spectres acquis en fluorescence sue
% la ligne DISCO du synchrotron soleil
% fonction d�velopp�e dans le cadre du projet cartographie par imagerie
% hyperspectrale dans le moyen infrarouge, en fluorescence (sources
% synchrotron) et Raman. 
%
% param�tres d'entr�e : 
%           soit pas de param�tres
%           soit 
%               1 - nom du fichier
%                   ou dataset object en m�moire matlab
%               2 - windowsize : taille du lissage de la transform�e de fourier
%               3 - sfolder : r�pertoire de sauvegarde
%
% param�tres de sortie : spltf : corrected dataset
%
% principe : tous les spectres du fichier de d�part sont trait�s
%            la m�thode utilis�e est un filtrage des fr�quence de la
%            transform�e de Fourier selon la m�thode d�crite pour les
%            images dans Gonzales et al., 2009 : Digital Image Processing
%            using Matlab
%            la transform�e de Fourier des spectres est calcul�e
%            shift de la transform�e pour se retrouver avec les valeurs de basse fr�quence au centre du spectre
%            constitution d'un filtre gaussien pour filtrer les fr�quence 
%                   le filtre gaussien garde les plus basse fr�quence et
%                   d�croit progressivement les plus hautes fr�quences. la
%                   largeur du gaussien d�finit un "seuile" de coupure des
%                   fr�quence
%            shift du r�sultat pour remettre les fr�quences comme apr�s une transform�e
%            transform�e de Fourier inverse
%            r�cup�ration de la partie r�elle comme spectre liss�
%                   
%           
%
% Usage : 
%       spc = corriger_bruit_fft
% ou
%       spc = corriger_bruit_fft(nomfic)
% ou 
% ou
%       windowsize=25;
%       spc = corriger_bruit_fft(nomfic,windowsize,threshold)
%
%
% Auteur : MArie-Fran�oise Devaux a Fatma Allouche
%           INRA - BIA - PVPP
%
% Date : 12 mars 2010
%        22 juin 2010       pour l'ajout du format raw en lecture et �criture
%        02 juillet 2010 : taille du filtre gaussien = 15
%        10 mars 2011 : pour la prise en compte du format dso et le
%        param�trage de la taille de la fen�tre 
%        4 octobre 2011 : demande pour la largeur  du filtre
%
%
%   

%% variables utiles / param�tres par d�faut
porig=pwd;

%largeur = 21; % du pic gaussien � mi hauteur
%largeur = 15;
%largeur=11;
%largeur=21;

%% entr�e des donne�s
if nargin >=1
    if ischar(varargin{1})
        nom=varargin{1};
        % test du type de fichier
        point=strfind(nom,'.');
        point=point(length(point)-1);
        ext=nom((point+1):(length(nom)));
        if ~strcmp(ext,'dso.mat')
             error('format .dso.mat attendu')
        end;
        load(nom)
        ims=dso;
        clear dso;
        repl=pwd;
    else if strcmp(class(varargin{1}),'dataset')
            ims=varragin;
            nom=ims.name;
        else
            error('argument incorrect : usage corriger_bruit_fft ou spc = corriger_bruit_fft(nomfic,windowsize,sfolder)');
        end;
    end;
end;
if nargin >3
    error('argument incorrect : usage corriger_bruit_fft ou spc = corriger_bruit_fft(nomfic,windowsize,sfolder)');
end;

if nargin==0
    [nom,repl]=uigetfile({'*.mat'},'nom du fichier des spectres � traiter : ','*.dso.mat');
    cd(repl)
    point=strfind(nom,'.');
    point=point(length(point)-1);
    ext=nom((point+1):(length(nom)));
    load(nom)
    ims=dso;
    clear dso;
end;

if nargin >=2 
    largeur=varargin{2};
    if largeur <0
        error('invalid value for window size')
    end;
    reps=pwd;
end;

if nargin ==3
    reps=varargin{3};
end;

% mode interactif pour le choix des param�tres
if nargin == 0
    verbose =1;
else
    verbose =0;
end;
   
if verbose
    % nom de sauvegarde du fichier r�sultat
    if exist('nom','var')
        noms=strrep(nom,ext,'ltf.dso.mat');
    else
        noms='*.ltf.dso.mat';
    end
    [noms,reps]=uiputfile({'*.mat'},'nom de sauvegarde du fichier r�sultat',noms);
else
    noms=strcat(ims.name,'.ltf.dso.mat');
end;


%% correction des spectres
if verbose
    close all
    % affichage des spectres de d�part
    plotdso(ims,1,0)
end;

% matrix of spectra
sp=ims.data;

% resulting spectr
spltf=zeros(size(sp));

    
% nombre de variables dans le tableau
nbsp=size(sp,1);

ok=0;

while ~ok
    % boucle sur les spectres
    for i=1:nbsp
        if round(i/50)-i/50==0
            disp(i);
        end;
        spt=sp(i,:);

        % trouver les valeurs non nulles : dans un tableau, il peut y avoir
        % des zeros que d�but ou � la fin li� � la diff�rence dans les
        % longueurs d'onde lors de l'acquisition
        indice=spt~=0;
        if sum(indice)>=1                  % cas d'un signal null : pas de fluorescence
            spt=spt(indice);
            nblot=length(spt);

            % transform�e de Fourier avec remplissage � 0 pour compl�ter
            tf=fft(spt,2*nblot);                % le r�sultat est complexe
            % shift de la transform�e de Fourier poru se retrouver avec les
            % basses fr�quences au centre du spectre
            tfs=fftshift(tf);

            % filtre gaussien : plage couverte = 2x le nombre de variable pour
            % �viter les transitions de bord � bord d'images
            fg= fspecial('gaussian', [1 nblot*2], largeur); 
            % le pic gaudssien est mis � 1 pour ne pas changer les valeurs
            % d'intensit� des spectres
            fg=fg./max(fg);                         

            % filtrage gaussien de la transform�e de Fourier
            tfsf=tfs.*fg;

            % shift du r�sultat pour remettre les basses fr�quences sur les
            % cot�s
            tfsfs=ifftshift(tfsf);

            % transform�e de Fourier inverse
            sptltf=ifft(tfsfs);

            % r�cup�ration d ela partie r�elle
            sptltf=real(sptltf);

            spltf(i,indice)=sptltf(1:nblot);
        else
            spltf(i,:)=spt;
        end;
    end;

    imsc=ims;
    imsc.data=spltf;
    imsc.name=strcat(ims.name,'.ltf');

    if verbose
        plotdso(imsc,1,0)
        title(' : spectres corrig�s des spikes')
        ok=ouinon('�tes-vous satisfait de la correction ? ');
        if ~ok
            prompt = {'taille de la fen�tre (nombre impair >0 et <200) :'};
            dlg_title = 'param�tres du lissage des spectres par transform�e de Fourier';
            num_lines = 1;
            def = {num2str(largeur)};
            options.Resize='on';
            suite=0;
            while ~suite
                reponse = inputdlg(prompt,dlg_title,num_lines,def,options);
                ll=str2num(reponse{1}); %#ok<ST2NM>
                if ll <0 || ll > 200
                    errordlg('largeur incorrecte')
                else
                    largeur=ll;
                    suite=1;
                end;
            end;
        end;
    else
        ok=1;
    end;
end;
    
    

    
   
%% sauvegarde des r�sultats
cd(reps)
savedso(imsc,noms,reps)

%% trace de l'ex�cution du programme

fic=fopen(strcat(strrep(noms,'.dso.mat',''),'.track.txt'),'w');
if fic==0
    errordlg('probl�me d''�criture du fichier %s',strcat(strrep(noms,'.dso.mat',''),'.track.txt'));
end;

fprintf(fic,'%s\r\n',datestr(now,0));
fprintf(fic,'__________________________________________________________________________\r\n');
fprintf(fic,'\t\t lissage des spectres par filtre gaussien de la transform�e de Fourier\r\n\r\n');
fprintf(fic,'R�pertoire de lecture du fichier des spectres :\r\n\t- %s\r\n',repl);
fprintf(fic,'\r\nFichier : %s\r\n',nom);
fprintf(fic,'\r\n\r\n');

% param�tres de correction 
fprintf(fic,'Param�tres de lissage par filtre gaussien de la transform�e de Fourier : \r\n\r\n');
fprintf(fic,'\tfonctions matlab pour calculer la transform�e de Fourier : fft et ifft\r\n\');
fprintf(fic,'\tfonctions matlab pour calculer le filtre gaussien : fspecial  + option ''gaussian''\r\n\');
fprintf(fic,'\tlargeur du filtre gaussien : %d\r\n',largeur);
fprintf(fic,'\tr�cup�ration de la partie r�elle du spectre apr�s lissage''\r\n\');

fprintf(fic,'R�pertoire de sauvegarde : \r\n\t- %s\r\n',reps);
fprintf(fic,'\r\nFichier : %s \r\n',noms);

% sauvegarde du programme utilis�
fprintf(fic,'__________________________________________________________________________\r\n');
info=which(mfilename);
repprog=fileparts(info);
fprintf(fic,'Programme : %s ',mfilename);
res=dir(info);
fprintf(fic,'du %s\r\n',res.date);
fprintf(fic,'\r\nR�pertoire du programme : %s \r\n',repprog);

fclose(fic);


%% fin
cd(porig)

    
            
            
            
                
        
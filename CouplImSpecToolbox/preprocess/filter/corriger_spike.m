function [ims] =corriger_spike(varargin)

%% description

    % correction des spikes dans des spectres
    
%% input 

    % soit pas de parametres
    % soit nom du fichier
    % soit dataset object en memoire matlab
    
    % windowsize : taille du tophat
    % threshold : seuil d'intensite
    % sfolder : repertoire de sauvegarde
    
%% output

    % ims : corrected dataset object

%% principe

   % principe : tous les spectres du fichier de depart sont traites.
   % la methode utilisee est le tophat
   % les parametres du chapeau haut de forme (tophat) sont fixes de la facon suivante : 
   % valeur par defaut programmee :     21 points de spectres
   %                                    40 en seuil d'intensite 
   % visualisation du resultats a partir de la collection de spectres
   % validation par l'operateur du resultat : 
   %   ok
   %   ou  nouvelle valeur de largeur et de seuil du tophat
   % le dataset object est sauve en format .dso.mat, le nom par defaut
   % est celui du fichier de depart auquel on ajoute l'extension.spi. 

%% use
 
    %   ims = corriger_spike
    %
    %       windowsize=25;
    %       threshold=60;
    %       ims = corriger_spike(nomfichier,windowsize,threshold)

%% comments

    % fonction ecrite pour les spectres acquis en fluorescence sur
    % la ligne DISCO du synchrotron soleil
    % fonction developpee dans le cadre du projet cartographie par imagerie
    % hyperspectrale dans le moyen infrarouge, en fluorescence (sources
    % synchrotron) et Raman. 

%% Author
    % MArie-Francoise Devaux, Fatma Allouche
    % INRA - BIA - PVPP

%% Date 
    % 12 mars 2010
    % 22 juin 2010 pour l'ajout du format raw en lecture et ecriture
    % 3 mars 2011 pour la prise en compte du format dso et le
    % parametrage de la taille de la fenetre et du seuil de 'tophat'   

%% context variables

porig=pwd;          % returns the current directory

%% default values

largeur = 21;           % window size
seuil = 40;             % seuil d'intensite

%% start

if nargin >4
    error('argument incorrect : usage corriger spike ou spc = corriger_spike(nomfic,windowsize,threshold,sfolder)');
end;

%% input
if nargin >=1
    if ischar(varargin{1})
        nom=varargin{1};
        % test du type de fichier
        point=strfind(nom,'.');
        point=point(length(point)-1);
        ext=nom((point+1):(length(nom)));
        if ~strcmp(ext,'dso.mat')
             error('format .dso.mat attendu')
        end;
        load(nom)
        ims=dso;
        clear dso;
        repl=pwd;
    else if strcmp(class(varargin{1}),'dataset')
            ims=varragin;
            nom=ims.name;
        else
            error('argument incorrect : usage corriger spike ou spc = corriger_spike(nomfic,windowsize,threshold)');
        end;
    end;
end;


if nargin==0
    [nom,repl]=uigetfile({'*.mat'},'nom du fichier des spectres a traiter : ','*.dso.mat');
    cd(repl)
    point=strfind(nom,'.');
    point=point(length(point)-1);
    ext=nom((point+1):(length(nom)));
    load(nom)
    ims=dso;
    clear dso;
end;

if nargin >=3 
    largeur=varargin{2};
    if largeur <0
        error('invalid value for window size')
    end;
    seuil= varargin{3};
    if seuil<0
        error('invalid value for intensity threshold')
    end;
    reps=pwd;
end;

if nargin ==4
    reps=varargin{4};
end;

% mode interactif pour le choix des parametres
if nargin == 0
    verbose =1;
else
    verbose =0;
end;
   
if verbose
    % nom de sauvegarde du fichier resultat
    if exist('nom','var')
        noms=strrep(nom,ext,'spi.dso.mat');
    else
        noms='*.spi.dso.mat';
    end
    [noms,reps]=uiputfile({'*.mat'},'nom de sauvegarde du fichier r?sultat',noms);
else
    noms=strcat(ims.name,'.spi.dso.mat');
end;

%% treatment

    % top hat 
if verbose
    close all
    % affichage des spectres de depart
    plotdso(ims,0)
end;

% matrix of spectra
sp=ims.data;

ok=0;

while ~ok
    % partie traitement des spikes
    se=strel('line',largeur,0);
    spth=imtophat(sp,se);
    spike=zeros(size(spth));
    spike(spth>seuil)=spth(spth>seuil);
    spc=sp-spike;
    
    imsc=ims;
    imsc.data=spc;
    imsc.name=strcat(ims.name,'.spi');
    
    if verbose
        plotdso(imsc,0)
        title(' : spectres corrig?s des spikes')
        ok=input('?tes-vous satisfait de la correction (0 = non 1 = oui) ? ');
        if ~ok
            prompt = {'taille de la fen?tre (nombre impair >0 et <200) :','seuil d''intensit? (>0) :'};
            dlg_title = 'param?tres du chapeau haut de forme';
            num_lines = 1;
            def = {num2str(largeur),num2str(seuil)};
            options.Resize='on';
            suite=0;
            while ~suite
                reponse = inputdlg(prompt,dlg_title,num_lines,def,options);
                ll=str2num(reponse{1}); %#ok<ST2NM>
                ss=str2num(reponse{2}); %#ok<ST2NM>
                if ll <0 || ll > 200
                    errordlg('largeur incorrecte')
                    suitel=0;
                else
                    largeur=ll;
                    suitel=1;
                end;
                if ss <0
                    errordlg('seuil incorrect')
                    suites=0;
                else
                    suites=1;
                    seuil=ss;
                end;
                if suitel && suites
                    suite=1;
                else 
                    suite=0;
                end;
            end;
        end;
    else
        ok=1;
    end;

end;

    
    
%% save

cd(reps)
savedso(imsc,noms,reps)
 
    

%% matlab function tracking

fic=fopen(strcat(strrep(noms,'.dso.mat',''),'.track.txt'),'w');
if fic==0
    errordlg('probl?me d''?criture du fichier %s',strcat(strrep(noms,'.dso.mat',''),'.track.txt'));
end;

fprintf(fic,'%s\r\n',datestr(now,0));
fprintf(fic,'__________________________________________________________________________\r\n');
fprintf(fic,'\t\t correction des "spikes"\r\n\r\n');
fprintf(fic,'R?pertoire de lecture du fichier des spectres :\r\n\t- %s\r\n',repl);
fprintf(fic,'\r\nFichier : %s\r\n',nom);
fprintf(fic,'\r\n\r\n');

% param?tres de correction 
fprintf(fic,'Param?tres de d?tection des spikes (spike : peak ?troit et intense dans le spectre) : \r\n\r\n');
fprintf(fic,'\tfonction matlab pour d?tecter les spikes : imtophat\r\n\');
fprintf(fic,'\tlargeur de d?tection : %d\r\n',largeur);
fprintf(fic,'\tseuil de d?tection : %d\r\n\r\n',seuil);

fprintf(fic,'R?pertoire de sauvegarde : \r\n\t- %s\r\n',reps);
fprintf(fic,'\r\nFichier : %s \r\n',noms);

% sauvegarde du programme utilis?
fprintf(fic,'__________________________________________________________________________\r\n');
info=which(mfilename);
repprog=fileparts(info);
fprintf(fic,'Programme : %s ',mfilename);
res=dir(info);
fprintf(fic,'du %s\r\n',res.date);
fprintf(fic,'\r\nR?pertoire du programme : %s \r\n',repprog);

fclose(fic);


%% end
cd(porig)

    
            
            
            
                
        
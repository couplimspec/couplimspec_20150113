function unitvector_norm (varargin)


[fname,pathname]=uigetfile({'*.dso.mat'},'select dataset file');
[path, name, ext] = fileparts(fname);
cd(pathname);
load(fname);% load dso to workspace
        % name and folder to save results
[sname,sfolder] = uiputfile({'*.dso.mat'},'result file name',strrep(fname,'.dso.mat',strcat('.norm1','.dso.mat')));

spr=dso;
sp=dso.data;
nn=sum((sp.*sp),2);

spr.data=sp./repmat(nn,1,size(sp,2));

cd(sfolder);
savedso(spr,sname,sfolder)

end
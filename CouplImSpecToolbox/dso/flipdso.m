
function flipdso

[fname,rfolder]=uigetfile({'*.dso.mat';},'name of first dso file');
cd (rfolder)
liste=dir(['*' '.dso.mat']);
nbf=length(liste);


for i=1:nbf
    cd(rfolder)
    disp(liste(i).name);
    load(liste(i).name); 
    imflip=flipdim(dso.imagedata,2);
    dataflip=reshape(imflip,dso.imagesize(1)*dso.imagesize(2),dso.size(2));
    dso.data=dataflip;
    dso.userdata.visim=fliplr(dso.userdata.visim);
    cd ..
    cd ..
    sfolder= 'flip';
    mkdir (sfolder)
    save(strcat(dso.name,'.flip','.dso.mat'),'dso');
end
end
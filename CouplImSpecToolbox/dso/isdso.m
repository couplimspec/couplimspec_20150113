  function [test,message]=isdso(dso)

 %% description
    % test si la structure pass�e est une structure de type dso
    %       % dso is a dataset object from eigenvector research see http://www.eigenvector.com/software/dataset.htm

    
    
%% input

    %  dso : datasetobject
    
%% output
    
    % test =1 si la structure est de type dso
    % test= 0 sinon
    % message : message d'erreur si test=0;

%% principe
    % test du type de la structure
    % attention : c'est aussi une structure de la boite � outils
    % statistique
    % 

%% use
    % test=isdso(don);
    
%% Comments
    % 
    
    
%% Author
    % MF devaux
    % BIA-PVPP
    % INRA Nantes
    
%% date
    % 21 octobre 2011


%% start

if nargin >1 || nargin ==0
    error('use: test = isdso(dso)');
end


%% treatement

message = '';
if ~strcmp(class(dso),'dataset')
    message=sprintf('la structure n''est pas de type ''dataset''');
    test=0;
else 
    test=1;
end;


    

%% end

    